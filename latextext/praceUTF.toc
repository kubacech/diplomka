\select@language {czech}
\contentsline {section}{\numberline {1}\IeC {\'U}vod}{5}
\contentsline {section}{\numberline {2}O Petriho s\IeC {\'\i }t\IeC {\'\i }ch}{6}
\contentsline {subsection}{\numberline {2.1}Sezn\IeC {\'a}men\IeC {\'\i } s Petriho s\IeC {\'\i }t\IeC {\v e}mi}{6}
\contentsline {subsection}{\numberline {2.2}Historie Petriho s\IeC {\'\i }t\IeC {\'\i }}{6}
\contentsline {subsection}{\numberline {2.3}Popis a stavov\IeC {\'a} anal\IeC {\'y}za P/T Petriho s\IeC {\'\i }t\IeC {\v e}}{7}
\contentsline {subsection}{\numberline {2.4}Hierarchick\IeC {\'e} Petriho s\IeC {\'\i }t\IeC {\v e}}{12}
\contentsline {subsection}{\numberline {2.5}\IeC {\v C}asovan\IeC {\'e} s\IeC {\'\i }t\IeC {\v e}}{12}
\contentsline {subsection}{\numberline {2.6}Objektov\IeC {\v e} orientovan\IeC {\'e} s\IeC {\'\i }t\IeC {\v e}}{13}
\contentsline {section}{\numberline {3}Vyu\IeC {\v z}\IeC {\'\i }van\IeC {\'e} technologie}{14}
\contentsline {subsection}{\numberline {3.1}N\IeC {\'a}stroj pro sazbu \textlatin {\LaTeX }}{14}
\contentsline {subsection}{\numberline {3.2}Java}{14}
\contentsline {subsection}{\numberline {3.3}Eclipse}{15}
\contentsline {subsection}{\numberline {3.4}Graphical Modeling Framework}{15}
\contentsline {subsection}{\numberline {3.5}Groovy}{16}
\contentsline {subsection}{\numberline {3.6}Verzovac\IeC {\'\i } syst\IeC {\'e}m}{17}
\contentsline {section}{\numberline {4}Popis v\IeC {\'y}choz\IeC {\'\i }ho stavu a anal\IeC {\'y}za}{18}
\contentsline {subsection}{\numberline {4.1}Prvn\IeC {\'\i } spu\IeC {\v s}t\IeC {\v e}n\IeC {\'\i }}{18}
\contentsline {subsection}{\numberline {4.2}O\IeC {\v s}et\IeC {\v r}en\IeC {\'\i } cyklick\IeC {\'y}ch z\IeC {\'a}vislostn\IeC {\'\i }}{19}
\contentsline {subsection}{\numberline {4.3}Dal\IeC {\v s}\IeC {\'\i } pravidla validace}{21}
\contentsline {subsection}{\numberline {4.4}\IeC {\'U}prava menu}{22}
\contentsline {subsection}{\numberline {4.5}Programov\IeC {\'a}n\IeC {\'\i } simulace}{22}
\contentsline {subsection}{\numberline {4.6}Vytvo\IeC {\v r}en\IeC {\'\i } requestu}{24}
\contentsline {subsection}{\numberline {4.7}Vykreslov\IeC {\'a}n\IeC {\'\i } v\IeC {\'y}sledk\IeC {\r u}}{25}
\contentsline {subsection}{\numberline {4.8}Oprava chyb s requestem}{26}
\contentsline {section}{\numberline {5}P\IeC {\v r}epracov\IeC {\'a}n\IeC {\'\i } modelu}{28}
\contentsline {subsection}{\numberline {5.1}Probl\IeC {\'e}m s modelem}{28}
\contentsline {subsection}{\numberline {5.2}Zm\IeC {\v e}na modelu}{29}
\contentsline {subsection}{\numberline {5.3}P\IeC {\v r}id\IeC {\'a}n\IeC {\'\i } u\IeC {\v z}ivatelsk\IeC {\'y}ch atribut\IeC {\r u} objektu}{32}
\contentsline {subsection}{\numberline {5.4}Vytv\IeC {\'a}\IeC {\v r}en\IeC {\'\i } objekt\IeC {\r u}}{34}
\contentsline {section}{\numberline {6}Dokon\IeC {\v c}en\IeC {\'\i } simulace}{36}
\contentsline {subsection}{\numberline {6.1}\IeC {\'U}prava simulace pro nov\IeC {\'y} model}{36}
\contentsline {subsection}{\numberline {6.2}\IeC {\v R}e\IeC {\v s}en\IeC {\'\i } n\IeC {\'a}hodn\IeC {\'e} volby parametr\IeC {\r u}}{37}
\contentsline {subsection}{\numberline {6.3}Implementace skriptovac\IeC {\'\i }ho enginu}{39}
\contentsline {subsection}{\numberline {6.4}Dopln\IeC {\v e}n\IeC {\'\i } chyb\IeC {\v e}j\IeC {\'\i }c\IeC {\'\i }ch \IeC {\v c}\IeC {\'a}st\IeC {\'\i }}{41}
\contentsline {subsection}{\numberline {6.5}P\IeC {\v r}id\IeC {\'a}n\IeC {\'\i } dal\IeC {\v s}\IeC {\'\i }ch akc\IeC {\'\i }}{42}
\contentsline {section}{\numberline {7}Dokon\IeC {\v c}en\IeC {\'\i }}{47}
\contentsline {subsection}{\numberline {7.1}Roz\IeC {\v s}\IeC {\'\i }\IeC {\v r}en\IeC {\'\i } modelu a simulace o \IeC {\v c}as}{47}
\contentsline {subsection}{\numberline {7.2}Zachycen\IeC {\'\i } z\IeC {\'a}znamu b\IeC {\v e}hu}{49}
\contentsline {subsection}{\numberline {7.3}Dopln\IeC {\v e}n\IeC {\'\i } guardu p\IeC {\v r}echodu}{49}
\contentsline {subsection}{\numberline {7.4}Dopln\IeC {\v e}n\IeC {\'\i } graf\IeC {\r u} a statistiky}{50}
\contentsline {subsection}{\numberline {7.5}Posledn\IeC {\'\i } drobn\IeC {\'e} \IeC {\'u}pravy}{52}
\contentsline {subsection}{\numberline {7.6}Co chyb\IeC {\'\i } dod\IeC {\v e}lat}{52}
\contentsline {section}{\numberline {8}Z\IeC {\'a}v\IeC {\v e}r}{54}
\contentsline {section}{\numberline {9}Reference}{55}
\contentsline {section}{\numberline {10}Seznam p\IeC {\v r}\IeC {\'\i }loh}{57}
\contentsline {section}{P\v {r}\'{\i }lohy}{57}
\contentsline {section}{\numberline {A}Vypisy d\IeC {\r u}le\IeC {\v z}it\IeC {\'y}ch \IeC {\v c}\IeC {\'a}st\IeC {\'\i } zdrojov\IeC {\'e}ho k\IeC {\'o}du}{58}
\contentsline {section}{\numberline {B}Z\IeC {\'a}klady nastaven\IeC {\'\i } skriptu p\IeC {\v r}echodu}{63}
