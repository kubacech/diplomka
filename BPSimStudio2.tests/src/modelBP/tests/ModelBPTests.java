/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>modelBP</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelBPTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new ModelBPTests("modelBP Tests");
		suite.addTestSuite(TransitionTest.class);
		suite.addTestSuite(ObjectTest.class);
		suite.addTestSuite(ActiveTest.class);
		suite.addTestSuite(PassiveTest.class);
		suite.addTestSuite(GroupTest.class);
		suite.addTestSuite(ScenarioTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelBPTests(String name) {
		super(name);
	}

} //ModelBPTests
