/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import modelBP.GroupName;
import modelBP.ModelBPFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Group Name</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class GroupNameTest extends TestCase {

	/**
	 * The fixture for this Group Name test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupName fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(GroupNameTest.class);
	}

	/**
	 * Constructs a new Group Name test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupNameTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Group Name test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(GroupName fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Group Name test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupName getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ModelBPFactory.eINSTANCE.createGroupName());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //GroupNameTest
