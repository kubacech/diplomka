/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.tests;

import junit.textui.TestRunner;

import modelBP.Active;
import modelBP.ModelBPFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Active</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ActiveTest extends GroupTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ActiveTest.class);
	}

	/**
	 * Constructs a new Active test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Active test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Active getFixture() {
		return (Active)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ModelBPFactory.eINSTANCE.createActive());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ActiveTest
