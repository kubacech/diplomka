/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.tests;

import junit.textui.TestRunner;

import modelBP.ModelBPFactory;
import modelBP.Passive;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Passive</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class PassiveTest extends GroupTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PassiveTest.class);
	}

	/**
	 * Constructs a new Passive test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PassiveTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Passive test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Passive getFixture() {
		return (Passive)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ModelBPFactory.eINSTANCE.createPassive());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //PassiveTest
