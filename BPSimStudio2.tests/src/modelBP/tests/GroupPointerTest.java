/**
 */
package modelBP.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import modelBP.GroupPointer;
import modelBP.ModelBPFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Group Pointer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class GroupPointerTest extends TestCase {

	/**
	 * The fixture for this Group Pointer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupPointer fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(GroupPointerTest.class);
	}

	/**
	 * Constructs a new Group Pointer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupPointerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Group Pointer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(GroupPointer fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Group Pointer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroupPointer getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ModelBPFactory.eINSTANCE.createGroupPointer());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //GroupPointerTest
