/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Group</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelBP.Group#getDescription <em>Description</em>}</li>
 *   <li>{@link modelBP.Group#getContained <em>Contained</em>}</li>
 *   <li>{@link modelBP.Group#getNamed <em>Named</em>}</li>
 *   <li>{@link modelBP.Group#getHasObject <em>Has Object</em>}</li>
 *   <li>{@link modelBP.Group#getCount <em>Count</em>}</li>
 *   <li>{@link modelBP.Group#getCountOut <em>Count Out</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelBP.ModelBPPackage#getGroup()
 * @model
 * @generated
 */
public interface Group extends EObject {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see modelBP.ModelBPPackage#getGroup_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link modelBP.Group#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Contained</b></em>' container reference.
	 * It is bidirectional and its opposite is '
	 * {@link modelBP.Place#getHasObject <em>Has Object</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained</em>' container reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Contained</em>' container reference.
	 * @see #setContained(Place)
	 * @see modelBP.ModelBPPackage#getGroup_Contained()
	 * @see modelBP.Place#getHasObject
	 * @model opposite="hasObject" required="true" transient="false"
	 * @generated
	 */
	Place getContained();

	/**
	 * Sets the value of the '{@link modelBP.Group#getContained <em>Contained</em>}' container reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Contained</em>' container reference.
	 * @see #getContained()
	 * @generated
	 */
	void setContained(Place value);

	/**
	 * Returns the value of the '<em><b>Count</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count</em>' attribute.
	 * @see #setCount(int)
	 * @see modelBP.ModelBPPackage#getGroup_Count()
	 * @model default="0" transient="true"
	 * @generated
	 */
	int getCount();

	
	/**
	 * Sets the value of the '{@link modelBP.Group#getCount <em>Count</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count</em>' attribute.
	 * @see #getCount()
	 * @generated
	 */
	void setCount(int value);

	/**
	 * Returns the value of the '<em><b>Count Out</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count Out</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count Out</em>' attribute.
	 * @see #isSetCountOut()
	 * @see #unsetCountOut()
	 * @see #setCountOut(String)
	 * @see modelBP.ModelBPPackage#getGroup_CountOut()
	 * @model default="" unsettable="true" transient="true"
	 * @generated
	 */
	String getCountOut();

	/**
	 * Sets the value of the '{@link modelBP.Group#getCountOut <em>Count Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count Out</em>' attribute.
	 * @see #isSetCountOut()
	 * @see #unsetCountOut()
	 * @see #getCountOut()
	 * @generated
	 */
	void setCountOut(String value);

	/**
	 * Unsets the value of the '{@link modelBP.Group#getCountOut <em>Count Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCountOut()
	 * @see #getCountOut()
	 * @see #setCountOut(String)
	 * @generated
	 */
	void unsetCountOut();

	/**
	 * Returns whether the value of the '{@link modelBP.Group#getCountOut <em>Count Out</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Count Out</em>' attribute is set.
	 * @see #unsetCountOut()
	 * @see #getCountOut()
	 * @see #setCountOut(String)
	 * @generated
	 */
	boolean isSetCountOut();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" many="false"
	 * @generated NOT
	 */
	List<Object> getHasEnabledObject();

	/**
	 * Returns the value of the '<em><b>Named</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Named</em>' reference.
	 * @see #setNamed(GroupName)
	 * @see modelBP.ModelBPPackage#getGroup_Named()
	 * @model required="true"
	 * @generated
	 */
	GroupName getNamed();

	/**
	 * Sets the value of the '{@link modelBP.Group#getNamed <em>Named</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Named</em>' reference.
	 * @see #getNamed()
	 * @generated
	 */
	void setNamed(GroupName value);

	/**
	 * Returns the value of the '<em><b>Has Object</b></em>' containment reference list.
	 * The list contents are of type {@link modelBP.Object}.
	 * It is bidirectional and its opposite is '{@link modelBP.Object#getContained <em>Contained</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Object</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Object</em>' containment reference list.
	 * @see modelBP.ModelBPPackage#getGroup_HasObject()
	 * @see modelBP.Object#getContained
	 * @model opposite="contained" containment="true"
	 * @generated
	 */
	EList<modelBP.Object> getHasObject();

} // Group
