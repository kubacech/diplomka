/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see modelBP.ModelBPFactory
 * @model kind="package"
 * @generated
 */
public interface ModelBPPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "modelBP";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://modelBP";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "modelBP";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ModelBPPackage eINSTANCE = modelBP.impl.ModelBPPackageImpl.init();

	/**
	 * The meta object id for the '{@link modelBP.impl.ElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.ElementImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getElement()
	 * @generated
	 */
	int ELEMENT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Linked Out</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__LINKED_OUT = 1;

	/**
	 * The feature id for the '<em><b>Assigned</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__ASSIGNED = 2;

	/**
	 * The feature id for the '<em><b>Duplicated</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__DUPLICATED = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Linked In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__LINKED_IN = 5;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link modelBP.impl.ProcessImpl <em>Process</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.ProcessImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getProcess()
	 * @generated
	 */
	int PROCESS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__NAME = ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Linked Out</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__LINKED_OUT = ELEMENT__LINKED_OUT;

	/**
	 * The feature id for the '<em><b>Assigned</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__ASSIGNED = ELEMENT__ASSIGNED;

	/**
	 * The feature id for the '<em><b>Duplicated</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__DUPLICATED = ELEMENT__DUPLICATED;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__DESCRIPTION = ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Linked In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__LINKED_IN = ELEMENT__LINKED_IN;

	/**
	 * The feature id for the '<em><b>Has Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__HAS_ELEMENT = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Input</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__INPUT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Output</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__OUTPUT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Has Group Name</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__HAS_GROUP_NAME = ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Global Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS__GLOBAL_TIME = ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Process</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link modelBP.impl.PlaceImpl <em>Place</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.PlaceImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getPlace()
	 * @generated
	 */
	int PLACE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__NAME = ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Linked Out</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__LINKED_OUT = ELEMENT__LINKED_OUT;

	/**
	 * The feature id for the '<em><b>Assigned</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__ASSIGNED = ELEMENT__ASSIGNED;

	/**
	 * The feature id for the '<em><b>Duplicated</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__DUPLICATED = ELEMENT__DUPLICATED;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__DESCRIPTION = ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Linked In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__LINKED_IN = ELEMENT__LINKED_IN;

	/**
	 * The feature id for the '<em><b>Has Object</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__HAS_OBJECT = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Capacity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__CAPACITY = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Place</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link modelBP.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.TransitionImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Linked Out</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__LINKED_OUT = ELEMENT__LINKED_OUT;

	/**
	 * The feature id for the '<em><b>Assigned</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ASSIGNED = ELEMENT__ASSIGNED;

	/**
	 * The feature id for the '<em><b>Duplicated</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__DUPLICATED = ELEMENT__DUPLICATED;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__DESCRIPTION = ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Linked In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__LINKED_IN = ELEMENT__LINKED_IN;

	/**
	 * The feature id for the '<em><b>Script</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SCRIPT = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Time Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TIME_MIN = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Time Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TIME_MAX = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Count Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__COUNT_MIN = ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Count Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__COUNT_MAX = ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Has Scenarios</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__HAS_SCENARIOS = ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link modelBP.impl.EdgeImpl <em>Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.EdgeImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getEdge()
	 * @generated
	 */
	int EDGE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__NAME = ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Linked Out</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__LINKED_OUT = ELEMENT__LINKED_OUT;

	/**
	 * The feature id for the '<em><b>Assigned</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__ASSIGNED = ELEMENT__ASSIGNED;

	/**
	 * The feature id for the '<em><b>Duplicated</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__DUPLICATED = ELEMENT__DUPLICATED;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__DESCRIPTION = ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Linked In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__LINKED_IN = ELEMENT__LINKED_IN;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__SOURCE = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__TARGET = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__MULTIPLICITY = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Scenarios</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__SCENARIOS = ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link modelBP.impl.ObjectImpl <em>Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.ObjectImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getObject()
	 * @generated
	 */
	int OBJECT = 5;

	/**
	 * The feature id for the '<em><b>Duplicated</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT__DUPLICATED = 0;

	/**
	 * The feature id for the '<em><b>Contained</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT__CONTAINED = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT__PROPERTIES = 3;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT__TIME = 4;

	/**
	 * The number of structural features of the '<em>Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link modelBP.impl.GroupImpl <em>Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.GroupImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getGroup()
	 * @generated
	 */
	int GROUP = 8;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Contained</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__CONTAINED = 1;

	/**
	 * The feature id for the '<em><b>Named</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__NAMED = 2;

	/**
	 * The feature id for the '<em><b>Has Object</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__HAS_OBJECT = 3;

	/**
	 * The feature id for the '<em><b>Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__COUNT = 4;

	/**
	 * The feature id for the '<em><b>Count Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP__COUNT_OUT = 5;

	/**
	 * The number of structural features of the '<em>Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link modelBP.impl.ActiveImpl <em>Active</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.ActiveImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getActive()
	 * @generated
	 */
	int ACTIVE = 6;

	/**
	 * The meta object id for the '{@link modelBP.impl.PassiveImpl <em>Passive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.PassiveImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getPassive()
	 * @generated
	 */
	int PASSIVE = 7;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE__DESCRIPTION = GROUP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Contained</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE__CONTAINED = GROUP__CONTAINED;

	/**
	 * The feature id for the '<em><b>Named</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE__NAMED = GROUP__NAMED;

	/**
	 * The feature id for the '<em><b>Has Object</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE__HAS_OBJECT = GROUP__HAS_OBJECT;

	/**
	 * The feature id for the '<em><b>Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE__COUNT = GROUP__COUNT;

	/**
	 * The feature id for the '<em><b>Count Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE__COUNT_OUT = GROUP__COUNT_OUT;

	/**
	 * The number of structural features of the '<em>Active</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_FEATURE_COUNT = GROUP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PASSIVE__DESCRIPTION = GROUP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Contained</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PASSIVE__CONTAINED = GROUP__CONTAINED;

	/**
	 * The feature id for the '<em><b>Named</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PASSIVE__NAMED = GROUP__NAMED;

	/**
	 * The feature id for the '<em><b>Has Object</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PASSIVE__HAS_OBJECT = GROUP__HAS_OBJECT;

	/**
	 * The feature id for the '<em><b>Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PASSIVE__COUNT = GROUP__COUNT;

	/**
	 * The feature id for the '<em><b>Count Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PASSIVE__COUNT_OUT = GROUP__COUNT_OUT;

	/**
	 * The number of structural features of the '<em>Passive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PASSIVE_FEATURE_COUNT = GROUP_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link modelBP.impl.GroupNameImpl <em>Group Name</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.GroupNameImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getGroupName()
	 * @generated
	 */
	int GROUP_NAME = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_NAME__NAME = 0;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_NAME__ACTIVE = 1;

	/**
	 * The number of structural features of the '<em>Group Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_NAME_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link modelBP.impl.ScenarioImpl <em>Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.ScenarioImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getScenario()
	 * @generated
	 */
	int SCENARIO = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__NAME = 0;

	/**
	 * The feature id for the '<em><b>Percent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__PERCENT = 1;

	/**
	 * The feature id for the '<em><b>Time Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__TIME_MIN = 2;

	/**
	 * The feature id for the '<em><b>Time Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__TIME_MAX = 3;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__COST = 4;

	/**
	 * The number of structural features of the '<em>Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_FEATURE_COUNT = 5;


	/**
	 * The meta object id for the '{@link modelBP.impl.GroupPointerImpl <em>Group Pointer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelBP.impl.GroupPointerImpl
	 * @see modelBP.impl.ModelBPPackageImpl#getGroupPointer()
	 * @generated
	 */
	int GROUP_POINTER = 11;

	/**
	 * The feature id for the '<em><b>Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_POINTER__COUNT = 0;

	/**
	 * The feature id for the '<em><b>Named</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_POINTER__NAMED = 1;

	/**
	 * The number of structural features of the '<em>Group Pointer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_POINTER_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link modelBP.Process <em>Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process</em>'.
	 * @see modelBP.Process
	 * @generated
	 */
	EClass getProcess();

	/**
	 * Returns the meta object for the containment reference list '{@link modelBP.Process#getHasElement <em>Has Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Element</em>'.
	 * @see modelBP.Process#getHasElement()
	 * @see #getProcess()
	 * @generated
	 */
	EReference getProcess_HasElement();

	/**
	 * Returns the meta object for the reference list '{@link modelBP.Process#getInput <em>Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Input</em>'.
	 * @see modelBP.Process#getInput()
	 * @see #getProcess()
	 * @generated
	 */
	EReference getProcess_Input();

	/**
	 * Returns the meta object for the reference list '{@link modelBP.Process#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Output</em>'.
	 * @see modelBP.Process#getOutput()
	 * @see #getProcess()
	 * @generated
	 */
	EReference getProcess_Output();

	/**
	 * Returns the meta object for the containment reference list '{@link modelBP.Process#getHasGroupName <em>Has Group Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Group Name</em>'.
	 * @see modelBP.Process#getHasGroupName()
	 * @see #getProcess()
	 * @generated
	 */
	EReference getProcess_HasGroupName();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Process#getGlobalTime <em>Global Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Global Time</em>'.
	 * @see modelBP.Process#getGlobalTime()
	 * @see #getProcess()
	 * @generated
	 */
	EAttribute getProcess_GlobalTime();

	/**
	 * Returns the meta object for class '{@link modelBP.Place <em>Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Place</em>'.
	 * @see modelBP.Place
	 * @generated
	 */
	EClass getPlace();

	/**
	 * Returns the meta object for the containment reference list '{@link modelBP.Place#getHasObject <em>Has Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Object</em>'.
	 * @see modelBP.Place#getHasObject()
	 * @see #getPlace()
	 * @generated
	 */
	EReference getPlace_HasObject();

	/**
	 * Returns the meta object for the containment reference list '{@link modelBP.Place#getCapacity <em>Capacity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Capacity</em>'.
	 * @see modelBP.Place#getCapacity()
	 * @see #getPlace()
	 * @generated
	 */
	EReference getPlace_Capacity();

	/**
	 * Returns the meta object for class '{@link modelBP.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see modelBP.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Transition#getScript <em>Script</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Script</em>'.
	 * @see modelBP.Transition#getScript()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Script();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Transition#getTimeMin <em>Time Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Min</em>'.
	 * @see modelBP.Transition#getTimeMin()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_TimeMin();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Transition#getTimeMax <em>Time Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Max</em>'.
	 * @see modelBP.Transition#getTimeMax()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_TimeMax();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Transition#getCountMin <em>Count Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Count Min</em>'.
	 * @see modelBP.Transition#getCountMin()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_CountMin();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Transition#getCountMax <em>Count Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Count Max</em>'.
	 * @see modelBP.Transition#getCountMax()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_CountMax();

	/**
	 * Returns the meta object for the containment reference list '{@link modelBP.Transition#getHasScenarios <em>Has Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Scenarios</em>'.
	 * @see modelBP.Transition#getHasScenarios()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_HasScenarios();

	/**
	 * Returns the meta object for class '{@link modelBP.Edge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edge</em>'.
	 * @see modelBP.Edge
	 * @generated
	 */
	EClass getEdge();

	/**
	 * Returns the meta object for the reference '{@link modelBP.Edge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see modelBP.Edge#getSource()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_Source();

	/**
	 * Returns the meta object for the reference '{@link modelBP.Edge#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see modelBP.Edge#getTarget()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_Target();

	/**
	 * Returns the meta object for the containment reference list '{@link modelBP.Edge#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Multiplicity</em>'.
	 * @see modelBP.Edge#getMultiplicity()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_Multiplicity();

	/**
	 * Returns the meta object for the reference list '{@link modelBP.Edge#getScenarios <em>Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Scenarios</em>'.
	 * @see modelBP.Edge#getScenarios()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_Scenarios();

	/**
	 * Returns the meta object for class '{@link modelBP.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see modelBP.Element
	 * @generated
	 */
	EClass getElement();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Element#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelBP.Element#getName()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Name();

	/**
	 * Returns the meta object for the reference list '{@link modelBP.Element#getLinkedOut <em>Linked Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Linked Out</em>'.
	 * @see modelBP.Element#getLinkedOut()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_LinkedOut();

	/**
	 * Returns the meta object for the container reference '{@link modelBP.Element#getAssigned <em>Assigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Assigned</em>'.
	 * @see modelBP.Element#getAssigned()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_Assigned();

	/**
	 * Returns the meta object for the reference list '{@link modelBP.Element#getDuplicated <em>Duplicated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Duplicated</em>'.
	 * @see modelBP.Element#getDuplicated()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_Duplicated();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Element#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see modelBP.Element#getDescription()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Description();

	/**
	 * Returns the meta object for the reference list '{@link modelBP.Element#getLinkedIn <em>Linked In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Linked In</em>'.
	 * @see modelBP.Element#getLinkedIn()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_LinkedIn();

	/**
	 * Returns the meta object for class '{@link modelBP.Object <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object</em>'.
	 * @see modelBP.Object
	 * @generated
	 */
	EClass getObject();

	/**
	 * Returns the meta object for the reference list '{@link modelBP.Object#getDuplicated <em>Duplicated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Duplicated</em>'.
	 * @see modelBP.Object#getDuplicated()
	 * @see #getObject()
	 * @generated
	 */
	EReference getObject_Duplicated();

	/**
	 * Returns the meta object for the container reference '{@link modelBP.Object#getContained <em>Contained</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Contained</em>'.
	 * @see modelBP.Object#getContained()
	 * @see #getObject()
	 * @generated
	 */
	EReference getObject_Contained();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Object#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see modelBP.Object#getDescription()
	 * @see #getObject()
	 * @generated
	 */
	EAttribute getObject_Description();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Object#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Properties</em>'.
	 * @see modelBP.Object#getProperties()
	 * @see #getObject()
	 * @generated
	 */
	EAttribute getObject_Properties();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Object#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see modelBP.Object#getTime()
	 * @see #getObject()
	 * @generated
	 */
	EAttribute getObject_Time();

	/**
	 * Returns the meta object for class '{@link modelBP.Active <em>Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active</em>'.
	 * @see modelBP.Active
	 * @generated
	 */
	EClass getActive();

	/**
	 * Returns the meta object for class '{@link modelBP.Passive <em>Passive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Passive</em>'.
	 * @see modelBP.Passive
	 * @generated
	 */
	EClass getPassive();

	/**
	 * Returns the meta object for class '{@link modelBP.Group <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group</em>'.
	 * @see modelBP.Group
	 * @generated
	 */
	EClass getGroup();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Group#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see modelBP.Group#getDescription()
	 * @see #getGroup()
	 * @generated
	 */
	EAttribute getGroup_Description();

	/**
	 * Returns the meta object for the container reference '{@link modelBP.Group#getContained <em>Contained</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Contained</em>'.
	 * @see modelBP.Group#getContained()
	 * @see #getGroup()
	 * @generated
	 */
	EReference getGroup_Contained();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Group#getCount <em>Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Count</em>'.
	 * @see modelBP.Group#getCount()
	 * @see #getGroup()
	 * @generated
	 */
	EAttribute getGroup_Count();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Group#getCountOut <em>Count Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Count Out</em>'.
	 * @see modelBP.Group#getCountOut()
	 * @see #getGroup()
	 * @generated
	 */
	EAttribute getGroup_CountOut();

	/**
	 * Returns the meta object for the reference '{@link modelBP.Group#getNamed <em>Named</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Named</em>'.
	 * @see modelBP.Group#getNamed()
	 * @see #getGroup()
	 * @generated
	 */
	EReference getGroup_Named();

	/**
	 * Returns the meta object for the containment reference list '{@link modelBP.Group#getHasObject <em>Has Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Object</em>'.
	 * @see modelBP.Group#getHasObject()
	 * @see #getGroup()
	 * @generated
	 */
	EReference getGroup_HasObject();

	/**
	 * Returns the meta object for class '{@link modelBP.GroupName <em>Group Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group Name</em>'.
	 * @see modelBP.GroupName
	 * @generated
	 */
	EClass getGroupName();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.GroupName#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelBP.GroupName#getName()
	 * @see #getGroupName()
	 * @generated
	 */
	EAttribute getGroupName_Name();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.GroupName#isActive <em>Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Active</em>'.
	 * @see modelBP.GroupName#isActive()
	 * @see #getGroupName()
	 * @generated
	 */
	EAttribute getGroupName_Active();

	/**
	 * Returns the meta object for class '{@link modelBP.Scenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scenario</em>'.
	 * @see modelBP.Scenario
	 * @generated
	 */
	EClass getScenario();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Scenario#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelBP.Scenario#getName()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_Name();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Scenario#getPercent <em>Percent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Percent</em>'.
	 * @see modelBP.Scenario#getPercent()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_Percent();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Scenario#getTimeMin <em>Time Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Min</em>'.
	 * @see modelBP.Scenario#getTimeMin()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_TimeMin();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Scenario#getTimeMax <em>Time Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Max</em>'.
	 * @see modelBP.Scenario#getTimeMax()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_TimeMax();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.Scenario#getCost <em>Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cost</em>'.
	 * @see modelBP.Scenario#getCost()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_Cost();

	/**
	 * Returns the meta object for class '{@link modelBP.GroupPointer <em>Group Pointer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group Pointer</em>'.
	 * @see modelBP.GroupPointer
	 * @generated
	 */
	EClass getGroupPointer();

	/**
	 * Returns the meta object for the attribute '{@link modelBP.GroupPointer#getCount <em>Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Count</em>'.
	 * @see modelBP.GroupPointer#getCount()
	 * @see #getGroupPointer()
	 * @generated
	 */
	EAttribute getGroupPointer_Count();

	/**
	 * Returns the meta object for the reference '{@link modelBP.GroupPointer#getNamed <em>Named</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Named</em>'.
	 * @see modelBP.GroupPointer#getNamed()
	 * @see #getGroupPointer()
	 * @generated
	 */
	EReference getGroupPointer_Named();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ModelBPFactory getModelBPFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link modelBP.impl.ProcessImpl <em>Process</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.ProcessImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getProcess()
		 * @generated
		 */
		EClass PROCESS = eINSTANCE.getProcess();

		/**
		 * The meta object literal for the '<em><b>Has Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS__HAS_ELEMENT = eINSTANCE.getProcess_HasElement();

		/**
		 * The meta object literal for the '<em><b>Input</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS__INPUT = eINSTANCE.getProcess_Input();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS__OUTPUT = eINSTANCE.getProcess_Output();

		/**
		 * The meta object literal for the '<em><b>Has Group Name</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESS__HAS_GROUP_NAME = eINSTANCE.getProcess_HasGroupName();

		/**
		 * The meta object literal for the '<em><b>Global Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESS__GLOBAL_TIME = eINSTANCE.getProcess_GlobalTime();

		/**
		 * The meta object literal for the '{@link modelBP.impl.PlaceImpl <em>Place</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.PlaceImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getPlace()
		 * @generated
		 */
		EClass PLACE = eINSTANCE.getPlace();

		/**
		 * The meta object literal for the '<em><b>Has Object</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLACE__HAS_OBJECT = eINSTANCE.getPlace_HasObject();

		/**
		 * The meta object literal for the '<em><b>Capacity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLACE__CAPACITY = eINSTANCE.getPlace_Capacity();

		/**
		 * The meta object literal for the '{@link modelBP.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.TransitionImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Script</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__SCRIPT = eINSTANCE.getTransition_Script();

		/**
		 * The meta object literal for the '<em><b>Time Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__TIME_MIN = eINSTANCE.getTransition_TimeMin();

		/**
		 * The meta object literal for the '<em><b>Time Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__TIME_MAX = eINSTANCE.getTransition_TimeMax();

		/**
		 * The meta object literal for the '<em><b>Count Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__COUNT_MIN = eINSTANCE.getTransition_CountMin();

		/**
		 * The meta object literal for the '<em><b>Count Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__COUNT_MAX = eINSTANCE.getTransition_CountMax();

		/**
		 * The meta object literal for the '<em><b>Has Scenarios</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__HAS_SCENARIOS = eINSTANCE.getTransition_HasScenarios();

		/**
		 * The meta object literal for the '{@link modelBP.impl.EdgeImpl <em>Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.EdgeImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getEdge()
		 * @generated
		 */
		EClass EDGE = eINSTANCE.getEdge();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__SOURCE = eINSTANCE.getEdge_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__TARGET = eINSTANCE.getEdge_Target();

		/**
		 * The meta object literal for the '<em><b>Multiplicity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__MULTIPLICITY = eINSTANCE.getEdge_Multiplicity();

		/**
		 * The meta object literal for the '<em><b>Scenarios</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__SCENARIOS = eINSTANCE.getEdge_Scenarios();

		/**
		 * The meta object literal for the '{@link modelBP.impl.ElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.ElementImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getElement()
		 * @generated
		 */
		EClass ELEMENT = eINSTANCE.getElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__NAME = eINSTANCE.getElement_Name();

		/**
		 * The meta object literal for the '<em><b>Linked Out</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__LINKED_OUT = eINSTANCE.getElement_LinkedOut();

		/**
		 * The meta object literal for the '<em><b>Assigned</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__ASSIGNED = eINSTANCE.getElement_Assigned();

		/**
		 * The meta object literal for the '<em><b>Duplicated</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__DUPLICATED = eINSTANCE.getElement_Duplicated();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__DESCRIPTION = eINSTANCE.getElement_Description();

		/**
		 * The meta object literal for the '<em><b>Linked In</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__LINKED_IN = eINSTANCE.getElement_LinkedIn();

		/**
		 * The meta object literal for the '{@link modelBP.impl.ObjectImpl <em>Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.ObjectImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getObject()
		 * @generated
		 */
		EClass OBJECT = eINSTANCE.getObject();

		/**
		 * The meta object literal for the '<em><b>Duplicated</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT__DUPLICATED = eINSTANCE.getObject_Duplicated();

		/**
		 * The meta object literal for the '<em><b>Contained</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT__CONTAINED = eINSTANCE.getObject_Contained();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT__DESCRIPTION = eINSTANCE.getObject_Description();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT__PROPERTIES = eINSTANCE.getObject_Properties();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT__TIME = eINSTANCE.getObject_Time();

		/**
		 * The meta object literal for the '{@link modelBP.impl.ActiveImpl <em>Active</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.ActiveImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getActive()
		 * @generated
		 */
		EClass ACTIVE = eINSTANCE.getActive();

		/**
		 * The meta object literal for the '{@link modelBP.impl.PassiveImpl <em>Passive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.PassiveImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getPassive()
		 * @generated
		 */
		EClass PASSIVE = eINSTANCE.getPassive();

		/**
		 * The meta object literal for the '{@link modelBP.impl.GroupImpl <em>Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.GroupImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getGroup()
		 * @generated
		 */
		EClass GROUP = eINSTANCE.getGroup();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP__DESCRIPTION = eINSTANCE.getGroup_Description();

		/**
		 * The meta object literal for the '<em><b>Contained</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP__CONTAINED = eINSTANCE.getGroup_Contained();

		/**
		 * The meta object literal for the '<em><b>Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP__COUNT = eINSTANCE.getGroup_Count();

		/**
		 * The meta object literal for the '<em><b>Count Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP__COUNT_OUT = eINSTANCE.getGroup_CountOut();

		/**
		 * The meta object literal for the '<em><b>Named</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP__NAMED = eINSTANCE.getGroup_Named();

		/**
		 * The meta object literal for the '<em><b>Has Object</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP__HAS_OBJECT = eINSTANCE.getGroup_HasObject();

		/**
		 * The meta object literal for the '{@link modelBP.impl.GroupNameImpl <em>Group Name</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.GroupNameImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getGroupName()
		 * @generated
		 */
		EClass GROUP_NAME = eINSTANCE.getGroupName();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_NAME__NAME = eINSTANCE.getGroupName_Name();

		/**
		 * The meta object literal for the '<em><b>Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_NAME__ACTIVE = eINSTANCE.getGroupName_Active();

		/**
		 * The meta object literal for the '{@link modelBP.impl.ScenarioImpl <em>Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.ScenarioImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getScenario()
		 * @generated
		 */
		EClass SCENARIO = eINSTANCE.getScenario();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__NAME = eINSTANCE.getScenario_Name();

		/**
		 * The meta object literal for the '<em><b>Percent</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__PERCENT = eINSTANCE.getScenario_Percent();

		/**
		 * The meta object literal for the '<em><b>Time Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__TIME_MIN = eINSTANCE.getScenario_TimeMin();

		/**
		 * The meta object literal for the '<em><b>Time Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__TIME_MAX = eINSTANCE.getScenario_TimeMax();

		/**
		 * The meta object literal for the '<em><b>Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__COST = eINSTANCE.getScenario_Cost();

		/**
		 * The meta object literal for the '{@link modelBP.impl.GroupPointerImpl <em>Group Pointer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelBP.impl.GroupPointerImpl
		 * @see modelBP.impl.ModelBPPackageImpl#getGroupPointer()
		 * @generated
		 */
		EClass GROUP_POINTER = eINSTANCE.getGroupPointer();

		/**
		 * The meta object literal for the '<em><b>Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUP_POINTER__COUNT = eINSTANCE.getGroupPointer_Count();

		/**
		 * The meta object literal for the '<em><b>Named</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP_POINTER__NAMED = eINSTANCE.getGroupPointer_Named();

	}

} //ModelBPPackage
