/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.impl;

import java.util.Collection;
import java.util.Random;

import modelBP.ModelBPPackage;
import modelBP.Scenario;
import modelBP.Transition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Transition</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelBP.impl.TransitionImpl#getScript <em>Script</em>}</li>
 *   <li>{@link modelBP.impl.TransitionImpl#getTimeMin <em>Time Min</em>}</li>
 *   <li>{@link modelBP.impl.TransitionImpl#getTimeMax <em>Time Max</em>}</li>
 *   <li>{@link modelBP.impl.TransitionImpl#getCountMin <em>Count Min</em>}</li>
 *   <li>{@link modelBP.impl.TransitionImpl#getCountMax <em>Count Max</em>}</li>
 *   <li>{@link modelBP.impl.TransitionImpl#getHasScenarios <em>Has Scenarios</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TransitionImpl extends ElementImpl implements Transition {
	/**
	 * The default value of the '{@link #getScript() <em>Script</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getScript()
	 * @generated
	 * @ordered
	 */
	protected static final String SCRIPT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getScript() <em>Script</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getScript()
	 * @generated
	 * @ordered
	 */
	protected String script = SCRIPT_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeMin() <em>Time Min</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTimeMin()
	 * @generated
	 * @ordered
	 */
	protected static final int TIME_MIN_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTimeMin() <em>Time Min</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTimeMin()
	 * @generated
	 * @ordered
	 */
	protected int timeMin = TIME_MIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeMax() <em>Time Max</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTimeMax()
	 * @generated
	 * @ordered
	 */
	protected static final int TIME_MAX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTimeMax() <em>Time Max</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTimeMax()
	 * @generated
	 * @ordered
	 */
	protected int timeMax = TIME_MAX_EDEFAULT;

	/**
	 * The default value of the '{@link #getCountMin() <em>Count Min</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCountMin()
	 * @generated
	 * @ordered
	 */
	protected static final int COUNT_MIN_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCountMin() <em>Count Min</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCountMin()
	 * @generated
	 * @ordered
	 */
	protected int countMin = COUNT_MIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getCountMax() <em>Count Max</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCountMax()
	 * @generated
	 * @ordered
	 */
	protected static final int COUNT_MAX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCountMax() <em>Count Max</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCountMax()
	 * @generated
	 * @ordered
	 */
	protected int countMax = COUNT_MAX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHasScenarios() <em>Has Scenarios</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getHasScenarios()
	 * @generated
	 * @ordered
	 */
	protected EList<Scenario> hasScenarios;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelBPPackage.Literals.TRANSITION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public String getScript() {
		if (script == null)
			script = new String();
		return script;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setScript(String newScript) {
		String oldScript = script;
		script = newScript;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.TRANSITION__SCRIPT, oldScript, script));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getTimeMin() {
		return timeMin;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeMin(int newTimeMin) {
		int oldTimeMin = timeMin;
		timeMin = newTimeMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.TRANSITION__TIME_MIN, oldTimeMin, timeMin));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getTimeMax() {
		return timeMax;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeMax(int newTimeMax) {
		int oldTimeMax = timeMax;
		timeMax = newTimeMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.TRANSITION__TIME_MAX, oldTimeMax, timeMax));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getCountMin() {
		return countMin;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCountMin(int newCountMin) {
		int oldCountMin = countMin;
		countMin = newCountMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.TRANSITION__COUNT_MIN, oldCountMin, countMin));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getCountMax() {
		return countMax;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCountMax(int newCountMax) {
		int oldCountMax = countMax;
		countMax = newCountMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.TRANSITION__COUNT_MAX, oldCountMax, countMax));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Scenario> getHasScenarios() {
		if (hasScenarios == null) {
			hasScenarios = new EObjectContainmentEList<Scenario>(Scenario.class, this, ModelBPPackage.TRANSITION__HAS_SCENARIOS);
		}
		return hasScenarios;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getTime() {
		if (timeMin > timeMax)
			return timeMin;
		return new Random().nextInt(timeMax - timeMin + 1) + timeMin;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getCount() {
		if (countMin > countMax)
			return countMin;
		return new Random().nextInt(countMax - countMin + 1) + countMin;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelBPPackage.TRANSITION__HAS_SCENARIOS:
				return ((InternalEList<?>)getHasScenarios()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelBPPackage.TRANSITION__SCRIPT:
				return getScript();
			case ModelBPPackage.TRANSITION__TIME_MIN:
				return getTimeMin();
			case ModelBPPackage.TRANSITION__TIME_MAX:
				return getTimeMax();
			case ModelBPPackage.TRANSITION__COUNT_MIN:
				return getCountMin();
			case ModelBPPackage.TRANSITION__COUNT_MAX:
				return getCountMax();
			case ModelBPPackage.TRANSITION__HAS_SCENARIOS:
				return getHasScenarios();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelBPPackage.TRANSITION__SCRIPT:
				setScript((String)newValue);
				return;
			case ModelBPPackage.TRANSITION__TIME_MIN:
				setTimeMin((Integer)newValue);
				return;
			case ModelBPPackage.TRANSITION__TIME_MAX:
				setTimeMax((Integer)newValue);
				return;
			case ModelBPPackage.TRANSITION__COUNT_MIN:
				setCountMin((Integer)newValue);
				return;
			case ModelBPPackage.TRANSITION__COUNT_MAX:
				setCountMax((Integer)newValue);
				return;
			case ModelBPPackage.TRANSITION__HAS_SCENARIOS:
				getHasScenarios().clear();
				getHasScenarios().addAll((Collection<? extends Scenario>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelBPPackage.TRANSITION__SCRIPT:
				setScript(SCRIPT_EDEFAULT);
				return;
			case ModelBPPackage.TRANSITION__TIME_MIN:
				setTimeMin(TIME_MIN_EDEFAULT);
				return;
			case ModelBPPackage.TRANSITION__TIME_MAX:
				setTimeMax(TIME_MAX_EDEFAULT);
				return;
			case ModelBPPackage.TRANSITION__COUNT_MIN:
				setCountMin(COUNT_MIN_EDEFAULT);
				return;
			case ModelBPPackage.TRANSITION__COUNT_MAX:
				setCountMax(COUNT_MAX_EDEFAULT);
				return;
			case ModelBPPackage.TRANSITION__HAS_SCENARIOS:
				getHasScenarios().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelBPPackage.TRANSITION__SCRIPT:
				return SCRIPT_EDEFAULT == null ? script != null : !SCRIPT_EDEFAULT.equals(script);
			case ModelBPPackage.TRANSITION__TIME_MIN:
				return timeMin != TIME_MIN_EDEFAULT;
			case ModelBPPackage.TRANSITION__TIME_MAX:
				return timeMax != TIME_MAX_EDEFAULT;
			case ModelBPPackage.TRANSITION__COUNT_MIN:
				return countMin != COUNT_MIN_EDEFAULT;
			case ModelBPPackage.TRANSITION__COUNT_MAX:
				return countMax != COUNT_MAX_EDEFAULT;
			case ModelBPPackage.TRANSITION__HAS_SCENARIOS:
				return hasScenarios != null && !hasScenarios.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (script: ");
		result.append(script);
		result.append(", timeMin: ");
		result.append(timeMin);
		result.append(", timeMax: ");
		result.append(timeMax);
		result.append(", countMin: ");
		result.append(countMin);
		result.append(", countMax: ");
		result.append(countMax);
		result.append(')');
		return result.toString();
	}

} // TransitionImpl
