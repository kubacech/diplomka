/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.impl;

import java.util.Random;

import modelBP.ModelBPPackage;
import modelBP.Scenario;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Scenario</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelBP.impl.ScenarioImpl#getName <em>Name</em>}</li>
 *   <li>{@link modelBP.impl.ScenarioImpl#getPercent <em>Percent</em>}</li>
 *   <li>{@link modelBP.impl.ScenarioImpl#getTimeMin <em>Time Min</em>}</li>
 *   <li>{@link modelBP.impl.ScenarioImpl#getTimeMax <em>Time Max</em>}</li>
 *   <li>{@link modelBP.impl.ScenarioImpl#getCost <em>Cost</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ScenarioImpl extends EObjectImpl implements Scenario {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPercent() <em>Percent</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPercent()
	 * @generated
	 * @ordered
	 */
	protected static final float PERCENT_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getPercent() <em>Percent</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPercent()
	 * @generated
	 * @ordered
	 */
	protected float percent = PERCENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeMin() <em>Time Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeMin()
	 * @generated
	 * @ordered
	 */
	protected static final int TIME_MIN_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTimeMin() <em>Time Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeMin()
	 * @generated
	 * @ordered
	 */
	protected int timeMin = TIME_MIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeMax() <em>Time Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeMax()
	 * @generated
	 * @ordered
	 */
	protected static final int TIME_MAX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTimeMax() <em>Time Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeMax()
	 * @generated
	 * @ordered
	 */
	protected int timeMax = TIME_MAX_EDEFAULT;

	/**
	 * The default value of the '{@link #getCost() <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCost()
	 * @generated
	 * @ordered
	 */
	protected static final int COST_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCost() <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCost()
	 * @generated
	 * @ordered
	 */
	protected int cost = COST_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ScenarioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelBPPackage.Literals.SCENARIO;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.SCENARIO__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public float getPercent() {
		return percent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPercent(float newPercent) {
		float oldPercent = percent;
		percent = newPercent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.SCENARIO__PERCENT, oldPercent, percent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTimeMin() {
		return timeMin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeMin(int newTimeMin) {
		int oldTimeMin = timeMin;
		timeMin = newTimeMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.SCENARIO__TIME_MIN, oldTimeMin, timeMin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTimeMax() {
		return timeMax;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeMax(int newTimeMax) {
		int oldTimeMax = timeMax;
		timeMax = newTimeMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.SCENARIO__TIME_MAX, oldTimeMax, timeMax));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setMin(int newMin) {
		int oldMin = timeMin;
		timeMin = newMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ModelBPPackage.SCENARIO__TIME_MIN, oldMin, timeMin));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setMax(int newMax) {
		if (newMax < timeMin)
			newMax = timeMin;
		int oldMax = timeMax;
		timeMax = newMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ModelBPPackage.SCENARIO__TIME_MAX, oldMax, timeMax));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCost(int newCost) {
		int oldCost = cost;
		cost = newCost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.SCENARIO__COST, oldCost, cost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getTime() {
		if(timeMin>timeMax)
			return timeMin;
		return new Random().nextInt(timeMax - timeMin + 1) + timeMin;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelBPPackage.SCENARIO__NAME:
				return getName();
			case ModelBPPackage.SCENARIO__PERCENT:
				return getPercent();
			case ModelBPPackage.SCENARIO__TIME_MIN:
				return getTimeMin();
			case ModelBPPackage.SCENARIO__TIME_MAX:
				return getTimeMax();
			case ModelBPPackage.SCENARIO__COST:
				return getCost();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelBPPackage.SCENARIO__NAME:
				setName((String)newValue);
				return;
			case ModelBPPackage.SCENARIO__PERCENT:
				setPercent((Float)newValue);
				return;
			case ModelBPPackage.SCENARIO__TIME_MIN:
				setTimeMin((Integer)newValue);
				return;
			case ModelBPPackage.SCENARIO__TIME_MAX:
				setTimeMax((Integer)newValue);
				return;
			case ModelBPPackage.SCENARIO__COST:
				setCost((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelBPPackage.SCENARIO__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ModelBPPackage.SCENARIO__PERCENT:
				setPercent(PERCENT_EDEFAULT);
				return;
			case ModelBPPackage.SCENARIO__TIME_MIN:
				setTimeMin(TIME_MIN_EDEFAULT);
				return;
			case ModelBPPackage.SCENARIO__TIME_MAX:
				setTimeMax(TIME_MAX_EDEFAULT);
				return;
			case ModelBPPackage.SCENARIO__COST:
				setCost(COST_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelBPPackage.SCENARIO__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ModelBPPackage.SCENARIO__PERCENT:
				return percent != PERCENT_EDEFAULT;
			case ModelBPPackage.SCENARIO__TIME_MIN:
				return timeMin != TIME_MIN_EDEFAULT;
			case ModelBPPackage.SCENARIO__TIME_MAX:
				return timeMax != TIME_MAX_EDEFAULT;
			case ModelBPPackage.SCENARIO__COST:
				return cost != COST_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", percent: ");
		result.append(percent);
		result.append(", timeMin: ");
		result.append(timeMin);
		result.append(", timeMax: ");
		result.append(timeMax);
		result.append(", cost: ");
		result.append(cost);
		result.append(')');
		return result.toString();
	}

} // ScenarioImpl
