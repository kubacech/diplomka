/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.impl;

import java.util.Collection;

import modelBP.Group;
import modelBP.GroupPointer;
import modelBP.ModelBPPackage;
import modelBP.Place;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelBP.impl.PlaceImpl#getHasObject <em>Has Object</em>}</li>
 *   <li>{@link modelBP.impl.PlaceImpl#getCapacity <em>Capacity</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PlaceImpl extends ElementImpl implements Place {
	/**
	 * The cached value of the '{@link #getHasObject() <em>Has Object</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasObject()
	 * @generated
	 * @ordered
	 */
	protected EList<Group> hasObject;

	/**
	 * The cached value of the '{@link #getCapacity() <em>Capacity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapacity()
	 * @generated
	 * @ordered
	 */
	protected EList<GroupPointer> capacity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelBPPackage.Literals.PLACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Group> getHasObject() {
		if (hasObject == null) {
			hasObject = new EObjectContainmentWithInverseEList<Group>(Group.class, this, ModelBPPackage.PLACE__HAS_OBJECT, ModelBPPackage.GROUP__CONTAINED);
		}
		return hasObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GroupPointer> getCapacity() {
		if (capacity == null) {
			capacity = new EObjectContainmentEList<GroupPointer>(GroupPointer.class, this, ModelBPPackage.PLACE__CAPACITY);
		}
		return capacity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelBPPackage.PLACE__HAS_OBJECT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getHasObject()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelBPPackage.PLACE__HAS_OBJECT:
				return ((InternalEList<?>)getHasObject()).basicRemove(otherEnd, msgs);
			case ModelBPPackage.PLACE__CAPACITY:
				return ((InternalEList<?>)getCapacity()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelBPPackage.PLACE__HAS_OBJECT:
				return getHasObject();
			case ModelBPPackage.PLACE__CAPACITY:
				return getCapacity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelBPPackage.PLACE__HAS_OBJECT:
				getHasObject().clear();
				getHasObject().addAll((Collection<? extends Group>)newValue);
				return;
			case ModelBPPackage.PLACE__CAPACITY:
				getCapacity().clear();
				getCapacity().addAll((Collection<? extends GroupPointer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelBPPackage.PLACE__HAS_OBJECT:
				getHasObject().clear();
				return;
			case ModelBPPackage.PLACE__CAPACITY:
				getCapacity().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelBPPackage.PLACE__HAS_OBJECT:
				return hasObject != null && !hasObject.isEmpty();
			case ModelBPPackage.PLACE__CAPACITY:
				return capacity != null && !capacity.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PlaceImpl
