/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.impl;

import modelBP.Active;
import modelBP.Edge;
import modelBP.Element;
import modelBP.Group;
import modelBP.GroupName;
import modelBP.GroupPointer;
import modelBP.ModelBPFactory;
import modelBP.ModelBPPackage;
import modelBP.Passive;
import modelBP.Place;
import modelBP.Scenario;
import modelBP.Transition;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelBPFactoryImpl extends EFactoryImpl implements ModelBPFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ModelBPFactory init() {
		try {
			ModelBPFactory theModelBPFactory = (ModelBPFactory)EPackage.Registry.INSTANCE.getEFactory(ModelBPPackage.eNS_URI);
			if (theModelBPFactory != null) {
				return theModelBPFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ModelBPFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelBPFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ModelBPPackage.PROCESS: return createProcess();
			case ModelBPPackage.PLACE: return createPlace();
			case ModelBPPackage.TRANSITION: return createTransition();
			case ModelBPPackage.EDGE: return createEdge();
			case ModelBPPackage.ELEMENT: return createElement();
			case ModelBPPackage.OBJECT: return createObject();
			case ModelBPPackage.ACTIVE: return createActive();
			case ModelBPPackage.PASSIVE: return createPassive();
			case ModelBPPackage.GROUP: return createGroup();
			case ModelBPPackage.GROUP_NAME: return createGroupName();
			case ModelBPPackage.SCENARIO: return createScenario();
			case ModelBPPackage.GROUP_POINTER: return createGroupPointer();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public modelBP.Process createProcess() {
		ProcessImpl process = new ProcessImpl();
		return process;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Place createPlace() {
		PlaceImpl place = new PlaceImpl();
		return place;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Edge createEdge() {
		EdgeImpl edge = new EdgeImpl();
		return edge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Element createElement() {
		ElementImpl element = new ElementImpl();
		return element;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public modelBP.Object createObject() {
		ObjectImpl object = new ObjectImpl();
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Active createActive() {
		ActiveImpl active = new ActiveImpl();
		return active;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Passive createPassive() {
		PassiveImpl passive = new PassiveImpl();
		return passive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group createGroup() {
		GroupImpl group = new GroupImpl();
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupName createGroupName() {
		GroupNameImpl groupName = new GroupNameImpl();
		return groupName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scenario createScenario() {
		ScenarioImpl scenario = new ScenarioImpl();
		return scenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupPointer createGroupPointer() {
		GroupPointerImpl groupPointer = new GroupPointerImpl();
		return groupPointer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelBPPackage getModelBPPackage() {
		return (ModelBPPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ModelBPPackage getPackage() {
		return ModelBPPackage.eINSTANCE;
	}

} //ModelBPFactoryImpl
