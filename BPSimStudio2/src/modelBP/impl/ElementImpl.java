/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.impl;

import java.util.Collection;

import modelBP.Edge;
import modelBP.Element;
import modelBP.ModelBPPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelBP.impl.ElementImpl#getName <em>Name</em>}</li>
 *   <li>{@link modelBP.impl.ElementImpl#getLinkedOut <em>Linked Out</em>}</li>
 *   <li>{@link modelBP.impl.ElementImpl#getAssigned <em>Assigned</em>}</li>
 *   <li>{@link modelBP.impl.ElementImpl#getDuplicated <em>Duplicated</em>}</li>
 *   <li>{@link modelBP.impl.ElementImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link modelBP.impl.ElementImpl#getLinkedIn <em>Linked In</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ElementImpl extends EObjectImpl implements Element {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLinkedOut() <em>Linked Out</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkedOut()
	 * @generated
	 * @ordered
	 */
	protected EList<Edge> linkedOut;

	/**
	 * The cached value of the '{@link #getDuplicated() <em>Duplicated</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuplicated()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> duplicated;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLinkedIn() <em>Linked In</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkedIn()
	 * @generated
	 * @ordered
	 */
	protected EList<Edge> linkedIn;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelBPPackage.Literals.ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.ELEMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Edge> getLinkedOut() {
		if (linkedOut == null) {
			linkedOut = new EObjectWithInverseResolvingEList<Edge>(Edge.class, this, ModelBPPackage.ELEMENT__LINKED_OUT, ModelBPPackage.EDGE__SOURCE);
		}
		return linkedOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public modelBP.Process getAssigned() {
		if (eContainerFeatureID() != ModelBPPackage.ELEMENT__ASSIGNED) return null;
		return (modelBP.Process)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssigned(modelBP.Process newAssigned, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAssigned, ModelBPPackage.ELEMENT__ASSIGNED, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssigned(modelBP.Process newAssigned) {
		if (newAssigned != eInternalContainer() || (eContainerFeatureID() != ModelBPPackage.ELEMENT__ASSIGNED && newAssigned != null)) {
			if (EcoreUtil.isAncestor(this, newAssigned))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAssigned != null)
				msgs = ((InternalEObject)newAssigned).eInverseAdd(this, ModelBPPackage.PROCESS__HAS_ELEMENT, modelBP.Process.class, msgs);
			msgs = basicSetAssigned(newAssigned, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.ELEMENT__ASSIGNED, newAssigned, newAssigned));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Element> getDuplicated() {
		if (duplicated == null) {
			duplicated = new EObjectResolvingEList<Element>(Element.class, this, ModelBPPackage.ELEMENT__DUPLICATED);
		}
		return duplicated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getDescription() {
		if (description == null)
			description = new String();
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.ELEMENT__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Edge> getLinkedIn() {
		if (linkedIn == null) {
			linkedIn = new EObjectWithInverseResolvingEList<Edge>(Edge.class, this, ModelBPPackage.ELEMENT__LINKED_IN, ModelBPPackage.EDGE__TARGET);
		}
		return linkedIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelBPPackage.ELEMENT__LINKED_OUT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLinkedOut()).basicAdd(otherEnd, msgs);
			case ModelBPPackage.ELEMENT__ASSIGNED:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAssigned((modelBP.Process)otherEnd, msgs);
			case ModelBPPackage.ELEMENT__LINKED_IN:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLinkedIn()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelBPPackage.ELEMENT__LINKED_OUT:
				return ((InternalEList<?>)getLinkedOut()).basicRemove(otherEnd, msgs);
			case ModelBPPackage.ELEMENT__ASSIGNED:
				return basicSetAssigned(null, msgs);
			case ModelBPPackage.ELEMENT__LINKED_IN:
				return ((InternalEList<?>)getLinkedIn()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case ModelBPPackage.ELEMENT__ASSIGNED:
				return eInternalContainer().eInverseRemove(this, ModelBPPackage.PROCESS__HAS_ELEMENT, modelBP.Process.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelBPPackage.ELEMENT__NAME:
				return getName();
			case ModelBPPackage.ELEMENT__LINKED_OUT:
				return getLinkedOut();
			case ModelBPPackage.ELEMENT__ASSIGNED:
				return getAssigned();
			case ModelBPPackage.ELEMENT__DUPLICATED:
				return getDuplicated();
			case ModelBPPackage.ELEMENT__DESCRIPTION:
				return getDescription();
			case ModelBPPackage.ELEMENT__LINKED_IN:
				return getLinkedIn();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelBPPackage.ELEMENT__NAME:
				setName((String)newValue);
				return;
			case ModelBPPackage.ELEMENT__LINKED_OUT:
				getLinkedOut().clear();
				getLinkedOut().addAll((Collection<? extends Edge>)newValue);
				return;
			case ModelBPPackage.ELEMENT__ASSIGNED:
				setAssigned((modelBP.Process)newValue);
				return;
			case ModelBPPackage.ELEMENT__DUPLICATED:
				getDuplicated().clear();
				getDuplicated().addAll((Collection<? extends Element>)newValue);
				return;
			case ModelBPPackage.ELEMENT__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ModelBPPackage.ELEMENT__LINKED_IN:
				getLinkedIn().clear();
				getLinkedIn().addAll((Collection<? extends Edge>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelBPPackage.ELEMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ModelBPPackage.ELEMENT__LINKED_OUT:
				getLinkedOut().clear();
				return;
			case ModelBPPackage.ELEMENT__ASSIGNED:
				setAssigned((modelBP.Process)null);
				return;
			case ModelBPPackage.ELEMENT__DUPLICATED:
				getDuplicated().clear();
				return;
			case ModelBPPackage.ELEMENT__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ModelBPPackage.ELEMENT__LINKED_IN:
				getLinkedIn().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelBPPackage.ELEMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ModelBPPackage.ELEMENT__LINKED_OUT:
				return linkedOut != null && !linkedOut.isEmpty();
			case ModelBPPackage.ELEMENT__ASSIGNED:
				return getAssigned() != null;
			case ModelBPPackage.ELEMENT__DUPLICATED:
				return duplicated != null && !duplicated.isEmpty();
			case ModelBPPackage.ELEMENT__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case ModelBPPackage.ELEMENT__LINKED_IN:
				return linkedIn != null && !linkedIn.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(')');
		return result.toString();
	}

} //ElementImpl
