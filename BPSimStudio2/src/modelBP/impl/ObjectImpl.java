/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.impl;

import java.util.Collection;
import java.util.HashMap;

import modelBP.Group;
import modelBP.ModelBPPackage;
import modelBP.util.ModelMapParser;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Object</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelBP.impl.ObjectImpl#getDuplicated <em>Duplicated</em>}</li>
 *   <li>{@link modelBP.impl.ObjectImpl#getContained <em>Contained</em>}</li>
 *   <li>{@link modelBP.impl.ObjectImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link modelBP.impl.ObjectImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link modelBP.impl.ObjectImpl#getTime <em>Time</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ObjectImpl extends EObjectImpl implements modelBP.Object {
	/**
	 * The cached value of the '{@link #getDuplicated() <em>Duplicated</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDuplicated()
	 * @generated
	 * @ordered
	 */
	protected EList<modelBP.Object> duplicated;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getProperties() <em>Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected static final String PROPERTIES_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getProperties()
	 * @generated NOT
	 * @ordered
	 */
	protected HashMap<String, java.lang.Object> properties;

	/**
	 * The default value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected static final int TIME_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected int time = TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelBPPackage.Literals.OBJECT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<modelBP.Object> getDuplicated() {
		if (duplicated == null) {
			duplicated = new EObjectResolvingEList<modelBP.Object>(modelBP.Object.class, this, ModelBPPackage.OBJECT__DUPLICATED);
		}
		return duplicated;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Group getContained() {
		if (eContainerFeatureID() != ModelBPPackage.OBJECT__CONTAINED) return null;
		return (Group)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContained(Group newContained,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContained, ModelBPPackage.OBJECT__CONTAINED, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setContained(Group newContained) {
		if (newContained != eInternalContainer() || (eContainerFeatureID() != ModelBPPackage.OBJECT__CONTAINED && newContained != null)) {
			if (EcoreUtil.isAncestor(this, newContained))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContained != null)
				msgs = ((InternalEObject)newContained).eInverseAdd(this, ModelBPPackage.GROUP__HAS_OBJECT, Group.class, msgs);
			msgs = basicSetContained(newContained, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.OBJECT__CONTAINED, newContained, newContained));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.OBJECT__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public String getProperties() {
		if (properties == null)
			properties = new HashMap<String, java.lang.Object>();
		return ModelMapParser.toString(properties);
	}

	public HashMap<String, java.lang.Object> getProp() {
		if (properties == null)
			properties = new HashMap<String, java.lang.Object>();
		return properties;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setProperties(String newProperties) {
		String oldProperties = ModelMapParser.toString(properties);
		properties = ModelMapParser.toMap(newProperties);
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ModelBPPackage.OBJECT__PROPERTIES, oldProperties,
					ModelMapParser.toString(properties)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTime() {
		return time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTime(int newTime) {
		int oldTime = time;
		time = newTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.OBJECT__TIME, oldTime, time));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getDesc() {
		return description;
	}

	public void setProp(HashMap<String, java.lang.Object> newProperties) {
		String oldProperties = ModelMapParser.toString(properties);
		properties = newProperties;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ModelBPPackage.OBJECT__PROPERTIES, oldProperties,
					ModelMapParser.toString(properties)));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelBPPackage.OBJECT__CONTAINED:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContained((Group)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelBPPackage.OBJECT__CONTAINED:
				return basicSetContained(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(
			NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case ModelBPPackage.OBJECT__CONTAINED:
				return eInternalContainer().eInverseRemove(this, ModelBPPackage.GROUP__HAS_OBJECT, Group.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelBPPackage.OBJECT__DUPLICATED:
				return getDuplicated();
			case ModelBPPackage.OBJECT__CONTAINED:
				return getContained();
			case ModelBPPackage.OBJECT__DESCRIPTION:
				return getDescription();
			case ModelBPPackage.OBJECT__PROPERTIES:
				return getProperties();
			case ModelBPPackage.OBJECT__TIME:
				return getTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelBPPackage.OBJECT__DUPLICATED:
				getDuplicated().clear();
				getDuplicated().addAll((Collection<? extends modelBP.Object>)newValue);
				return;
			case ModelBPPackage.OBJECT__CONTAINED:
				setContained((Group)newValue);
				return;
			case ModelBPPackage.OBJECT__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ModelBPPackage.OBJECT__PROPERTIES:
				setProperties((String)newValue);
				return;
			case ModelBPPackage.OBJECT__TIME:
				setTime((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelBPPackage.OBJECT__DUPLICATED:
				getDuplicated().clear();
				return;
			case ModelBPPackage.OBJECT__CONTAINED:
				setContained((Group)null);
				return;
			case ModelBPPackage.OBJECT__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ModelBPPackage.OBJECT__PROPERTIES:
				setProperties(PROPERTIES_EDEFAULT);
				return;
			case ModelBPPackage.OBJECT__TIME:
				setTime(TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelBPPackage.OBJECT__DUPLICATED:
				return duplicated != null && !duplicated.isEmpty();
			case ModelBPPackage.OBJECT__CONTAINED:
				return getContained() != null;
			case ModelBPPackage.OBJECT__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case ModelBPPackage.OBJECT__PROPERTIES:
				return PROPERTIES_EDEFAULT == null ? properties != null : !PROPERTIES_EDEFAULT.equals(properties);
			case ModelBPPackage.OBJECT__TIME:
				return time != TIME_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", properties: ");
		result.append(properties);
		result.append(", time: ");
		result.append(time);
		result.append(')');
		return result.toString();
	}

} // ObjectImpl
