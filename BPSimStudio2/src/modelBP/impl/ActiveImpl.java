/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.impl;

import modelBP.Active;
import modelBP.ModelBPPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ActiveImpl extends GroupImpl implements Active {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelBPPackage.Literals.ACTIVE;
	}

} //ActiveImpl
