/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import modelBP.Group;
import modelBP.GroupName;
import modelBP.ModelBPFactory;
import modelBP.ModelBPPackage;
import modelBP.Place;
import modelBP.Process;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Group</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link modelBP.impl.GroupImpl#getDescription <em>Description</em>}</li>
 * <li>{@link modelBP.impl.GroupImpl#getContained <em>Contained</em>}</li>
 * <li>{@link modelBP.impl.GroupImpl#getNamed <em>Named</em>}</li>
 * <li>{@link modelBP.impl.GroupImpl#getHasObject <em>Has Object</em>}</li>
 * <li>{@link modelBP.impl.GroupImpl#getCount <em>Count</em>}</li>
 * <li>{@link modelBP.impl.GroupImpl#getCountOut <em>Count Out</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class GroupImpl extends EObjectImpl implements Group {
	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNamed() <em>Named</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getNamed()
	 * @generated
	 * @ordered
	 */
	protected GroupName named;

	/**
	 * The cached value of the '{@link #getHasObject() <em>Has Object</em>}'
	 * containment reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHasObject()
	 * @generated
	 * @ordered
	 */
	protected EList<modelBP.Object> hasObject;

	/**
	 * The default value of the '{@link #getCount() <em>Count</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCount()
	 * @generated
	 * @ordered
	 */
	protected static final int COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCount() <em>Count</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCount()
	 * @generated
	 * @ordered
	 */
	protected int count = COUNT_EDEFAULT;

	/**
	 * The default value of the '{@link #getCountOut() <em>Count Out</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCountOut()
	 * @generated
	 * @ordered
	 */
	protected static final String COUNT_OUT_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getCountOut() <em>Count Out</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCountOut()
	 * @generated
	 * @ordered
	 */
	protected String countOut = COUNT_OUT_EDEFAULT;

	/**
	 * This is true if the Count Out attribute has been set. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean countOutESet;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected GroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelBPPackage.Literals.GROUP;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ModelBPPackage.GROUP__DESCRIPTION, oldDescription,
					description));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Place getContained() {
		if (eContainerFeatureID() != ModelBPPackage.GROUP__CONTAINED)
			return null;
		return (Place) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetContained(Place newContained,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newContained,
				ModelBPPackage.GROUP__CONTAINED, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setContained(Place newContained) {
		if (newContained != eInternalContainer()
				|| (eContainerFeatureID() != ModelBPPackage.GROUP__CONTAINED && newContained != null)) {
			if (EcoreUtil.isAncestor(this, newContained))
				throw new IllegalArgumentException(
						"Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContained != null)
				msgs = ((InternalEObject) newContained).eInverseAdd(this,
						ModelBPPackage.PLACE__HAS_OBJECT, Place.class, msgs);
			msgs = basicSetContained(newContained, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ModelBPPackage.GROUP__CONTAINED, newContained, newContained));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public int getCount() {
		// return count;
		return getHasObject().size();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setCount(int newCount) {
		int oldCount = count;
		count = newCount;

		for (int i = getHasObject().size(); i < count; i++) {
			modelBP.Object obj = ModelBPFactory.eINSTANCE.createObject();
			obj.setContained(this);
			obj.setTime(0);
			obj.setDescription(i+". vygenerovane ve skupine s popisem "+description);
			getHasObject().add(obj);
		}

		if (newCount < getHasObject().size()) {
			List<modelBP.Object> nove = new ArrayList<modelBP.Object>();
			for (int i = 0; i < newCount; i++) {
				nove.add(getHasObject().get(i));
			}
			getHasObject().clear();
			getHasObject().addAll(nove);
		}

		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET,
					ModelBPPackage.GROUP__COUNT, oldCount, count));

			eNotify(new ENotificationImpl(this, Notification.SET,
					ModelBPPackage.GROUP__COUNT_OUT, countOut, getCountOut()));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public String getCountOut() {
		// int enabled = 0;
		// for (modelBP.Object o : getHasObject()) {
		// if (o.getTime() <= getContained().getAssigned().getGlobalTime())
		// enabled++;
		// }
		// return enabled + " (" + getHasObject().size() + ")";
		return getHasEnabledObject().size() + " (" + getHasObject().size()
				+ ")";
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setCountOut(String newCountOut) {
		String oldCountOut = countOut;
		countOut = newCountOut;
		boolean oldCountOutESet = countOutESet;
		countOutESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ModelBPPackage.GROUP__COUNT_OUT, oldCountOut, countOut,
					!oldCountOutESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void unsetCountOut() {
		String oldCountOut = countOut;
		boolean oldCountOutESet = countOutESet;
		countOut = COUNT_OUT_EDEFAULT;
		countOutESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ModelBPPackage.GROUP__COUNT_OUT, oldCountOut,
					COUNT_OUT_EDEFAULT, oldCountOutESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean isSetCountOut() {
		return countOutESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public List<modelBP.Object> getHasEnabledObject() {
		List<modelBP.Object> enabled = new ArrayList<modelBP.Object>();
		Process root = getContained().getAssigned();
		//ziskani korenoveho procesu v pripade ze je misto soucasti podprocesu
		while (root.getAssigned() != null) {
			root = root.getAssigned();
		}
		for (modelBP.Object o : getHasObject()) {
			if (o.getTime() <= root.getGlobalTime())
				enabled.add(o);
		}
		return enabled;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GroupName getNamed() {
		if (named != null && named.eIsProxy()) {
			InternalEObject oldNamed = (InternalEObject) named;
			named = (GroupName) eResolveProxy(oldNamed);
			if (named != oldNamed) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ModelBPPackage.GROUP__NAMED, oldNamed, named));
			}
		}
		return named;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public GroupName basicGetNamed() {
		return named;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setNamed(GroupName newNamed) {
		GroupName oldNamed = named;
		named = newNamed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ModelBPPackage.GROUP__NAMED, oldNamed, named));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<modelBP.Object> getHasObject() {
		if (hasObject == null) {
			hasObject = new EObjectContainmentWithInverseEList<modelBP.Object>(
					modelBP.Object.class, this,
					ModelBPPackage.GROUP__HAS_OBJECT,
					ModelBPPackage.OBJECT__CONTAINED);
		}
		return hasObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ModelBPPackage.GROUP__CONTAINED:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetContained((Place) otherEnd, msgs);
		case ModelBPPackage.GROUP__HAS_OBJECT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getHasObject())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ModelBPPackage.GROUP__CONTAINED:
			return basicSetContained(null, msgs);
		case ModelBPPackage.GROUP__HAS_OBJECT:
			return ((InternalEList<?>) getHasObject()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(
			NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case ModelBPPackage.GROUP__CONTAINED:
			return eInternalContainer().eInverseRemove(this,
					ModelBPPackage.PLACE__HAS_OBJECT, Place.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ModelBPPackage.GROUP__DESCRIPTION:
			return getDescription();
		case ModelBPPackage.GROUP__CONTAINED:
			return getContained();
		case ModelBPPackage.GROUP__NAMED:
			if (resolve)
				return getNamed();
			return basicGetNamed();
		case ModelBPPackage.GROUP__HAS_OBJECT:
			return getHasObject();
		case ModelBPPackage.GROUP__COUNT:
			return getCount();
		case ModelBPPackage.GROUP__COUNT_OUT:
			return getCountOut();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ModelBPPackage.GROUP__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case ModelBPPackage.GROUP__CONTAINED:
			setContained((Place) newValue);
			return;
		case ModelBPPackage.GROUP__NAMED:
			setNamed((GroupName) newValue);
			return;
		case ModelBPPackage.GROUP__HAS_OBJECT:
			getHasObject().clear();
			getHasObject().addAll(
					(Collection<? extends modelBP.Object>) newValue);
			return;
		case ModelBPPackage.GROUP__COUNT:
			setCount((Integer) newValue);
			return;
		case ModelBPPackage.GROUP__COUNT_OUT:
			setCountOut((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ModelBPPackage.GROUP__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case ModelBPPackage.GROUP__CONTAINED:
			setContained((Place) null);
			return;
		case ModelBPPackage.GROUP__NAMED:
			setNamed((GroupName) null);
			return;
		case ModelBPPackage.GROUP__HAS_OBJECT:
			getHasObject().clear();
			return;
		case ModelBPPackage.GROUP__COUNT:
			setCount(COUNT_EDEFAULT);
			return;
		case ModelBPPackage.GROUP__COUNT_OUT:
			unsetCountOut();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ModelBPPackage.GROUP__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null
					: !DESCRIPTION_EDEFAULT.equals(description);
		case ModelBPPackage.GROUP__CONTAINED:
			return getContained() != null;
		case ModelBPPackage.GROUP__NAMED:
			return named != null;
		case ModelBPPackage.GROUP__HAS_OBJECT:
			return hasObject != null && !hasObject.isEmpty();
		case ModelBPPackage.GROUP__COUNT:
			return count != COUNT_EDEFAULT;
		case ModelBPPackage.GROUP__COUNT_OUT:
			return isSetCountOut();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", count: ");
		result.append(count);
		result.append(", countOut: ");
		if (countOutESet)
			result.append(countOut);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} // GroupImpl
