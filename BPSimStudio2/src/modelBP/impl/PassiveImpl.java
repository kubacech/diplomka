/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.impl;

import modelBP.ModelBPPackage;
import modelBP.Passive;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Passive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class PassiveImpl extends GroupImpl implements Passive {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PassiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelBPPackage.Literals.PASSIVE;
	}

} //PassiveImpl
