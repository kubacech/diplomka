/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP.impl;

import java.util.Collection;

import modelBP.Element;
import modelBP.GroupName;
import modelBP.ModelBPPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelBP.impl.ProcessImpl#getHasElement <em>Has Element</em>}</li>
 *   <li>{@link modelBP.impl.ProcessImpl#getInput <em>Input</em>}</li>
 *   <li>{@link modelBP.impl.ProcessImpl#getOutput <em>Output</em>}</li>
 *   <li>{@link modelBP.impl.ProcessImpl#getHasGroupName <em>Has Group Name</em>}</li>
 *   <li>{@link modelBP.impl.ProcessImpl#getGlobalTime <em>Global Time</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProcessImpl extends ElementImpl implements modelBP.Process {
	/**
	 * The cached value of the '{@link #getHasElement() <em>Has Element</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasElement()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> hasElement;

	/**
	 * The cached value of the '{@link #getInput() <em>Input</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> input;

	/**
	 * The cached value of the '{@link #getOutput() <em>Output</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> output;

	/**
	 * The cached value of the '{@link #getHasGroupName() <em>Has Group Name</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasGroupName()
	 * @generated
	 * @ordered
	 */
	protected EList<GroupName> hasGroupName;

	/**
	 * The default value of the '{@link #getGlobalTime() <em>Global Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGlobalTime()
	 * @generated
	 * @ordered
	 */
	protected static final int GLOBAL_TIME_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getGlobalTime() <em>Global Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGlobalTime()
	 * @generated
	 * @ordered
	 */
	protected int globalTime = GLOBAL_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelBPPackage.Literals.PROCESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Element> getHasElement() {
		if (hasElement == null) {
			hasElement = new EObjectContainmentWithInverseEList<Element>(Element.class, this, ModelBPPackage.PROCESS__HAS_ELEMENT, ModelBPPackage.ELEMENT__ASSIGNED);
		}
		return hasElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Element> getInput() {
		if (input == null) {
			input = new EObjectResolvingEList<Element>(Element.class, this, ModelBPPackage.PROCESS__INPUT);
		}
		return input;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Element> getOutput() {
		if (output == null) {
			output = new EObjectResolvingEList<Element>(Element.class, this, ModelBPPackage.PROCESS__OUTPUT);
		}
		return output;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GroupName> getHasGroupName() {
		if (hasGroupName == null) {
			hasGroupName = new EObjectContainmentEList<GroupName>(GroupName.class, this, ModelBPPackage.PROCESS__HAS_GROUP_NAME);
		}
		return hasGroupName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getGlobalTime() {
		return globalTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGlobalTime(int newGlobalTime) {
		int oldGlobalTime = globalTime;
		globalTime = newGlobalTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelBPPackage.PROCESS__GLOBAL_TIME, oldGlobalTime, globalTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelBPPackage.PROCESS__HAS_ELEMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getHasElement()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelBPPackage.PROCESS__HAS_ELEMENT:
				return ((InternalEList<?>)getHasElement()).basicRemove(otherEnd, msgs);
			case ModelBPPackage.PROCESS__HAS_GROUP_NAME:
				return ((InternalEList<?>)getHasGroupName()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelBPPackage.PROCESS__HAS_ELEMENT:
				return getHasElement();
			case ModelBPPackage.PROCESS__INPUT:
				return getInput();
			case ModelBPPackage.PROCESS__OUTPUT:
				return getOutput();
			case ModelBPPackage.PROCESS__HAS_GROUP_NAME:
				return getHasGroupName();
			case ModelBPPackage.PROCESS__GLOBAL_TIME:
				return getGlobalTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelBPPackage.PROCESS__HAS_ELEMENT:
				getHasElement().clear();
				getHasElement().addAll((Collection<? extends Element>)newValue);
				return;
			case ModelBPPackage.PROCESS__INPUT:
				getInput().clear();
				getInput().addAll((Collection<? extends Element>)newValue);
				return;
			case ModelBPPackage.PROCESS__OUTPUT:
				getOutput().clear();
				getOutput().addAll((Collection<? extends Element>)newValue);
				return;
			case ModelBPPackage.PROCESS__HAS_GROUP_NAME:
				getHasGroupName().clear();
				getHasGroupName().addAll((Collection<? extends GroupName>)newValue);
				return;
			case ModelBPPackage.PROCESS__GLOBAL_TIME:
				setGlobalTime((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelBPPackage.PROCESS__HAS_ELEMENT:
				getHasElement().clear();
				return;
			case ModelBPPackage.PROCESS__INPUT:
				getInput().clear();
				return;
			case ModelBPPackage.PROCESS__OUTPUT:
				getOutput().clear();
				return;
			case ModelBPPackage.PROCESS__HAS_GROUP_NAME:
				getHasGroupName().clear();
				return;
			case ModelBPPackage.PROCESS__GLOBAL_TIME:
				setGlobalTime(GLOBAL_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelBPPackage.PROCESS__HAS_ELEMENT:
				return hasElement != null && !hasElement.isEmpty();
			case ModelBPPackage.PROCESS__INPUT:
				return input != null && !input.isEmpty();
			case ModelBPPackage.PROCESS__OUTPUT:
				return output != null && !output.isEmpty();
			case ModelBPPackage.PROCESS__HAS_GROUP_NAME:
				return hasGroupName != null && !hasGroupName.isEmpty();
			case ModelBPPackage.PROCESS__GLOBAL_TIME:
				return globalTime != GLOBAL_TIME_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (globalTime: ");
		result.append(globalTime);
		result.append(')');
		return result.toString();
	}

} //ProcessImpl
