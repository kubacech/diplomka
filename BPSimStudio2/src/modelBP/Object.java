/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;

import java.util.HashMap;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Object</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelBP.Object#getDuplicated <em>Duplicated</em>}</li>
 *   <li>{@link modelBP.Object#getContained <em>Contained</em>}</li>
 *   <li>{@link modelBP.Object#getDescription <em>Description</em>}</li>
 *   <li>{@link modelBP.Object#getProperties <em>Properties</em>}</li>
 *   <li>{@link modelBP.Object#getTime <em>Time</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelBP.ModelBPPackage#getObject()
 * @model
 * @generated
 */
public interface Object extends EObject {
	/**
	 * Returns the value of the '<em><b>Duplicated</b></em>' reference list.
	 * The list contents are of type {@link modelBP.Object}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duplicated</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duplicated</em>' reference list.
	 * @see modelBP.ModelBPPackage#getObject_Duplicated()
	 * @model
	 * @generated
	 */
	EList<Object> getDuplicated();

	/**
	 * Returns the value of the '<em><b>Contained</b></em>' container reference.
	 * It is bidirectional and its opposite is '
	 * {@link modelBP.Group#getHasObject <em>Has Object</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained</em>' container reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Contained</em>' container reference.
	 * @see #setContained(Group)
	 * @see modelBP.ModelBPPackage#getObject_Contained()
	 * @see modelBP.Group#getHasObject
	 * @model opposite="hasObject" required="true" transient="false"
	 * @generated
	 */
	Group getContained();

	/**
	 * Sets the value of the '{@link modelBP.Object#getContained <em>Contained</em>}' container reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Contained</em>' container reference.
	 * @see #getContained()
	 * @generated
	 */
	void setContained(Group value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see modelBP.ModelBPPackage#getObject_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link modelBP.Object#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' attribute.
	 * @see #setProperties(String)
	 * @see modelBP.ModelBPPackage#getObject_Properties()
	 * @model default=""
	 * @generated
	 */
	String getProperties();

	/**
	 * Sets the value of the '{@link modelBP.Object#getProperties <em>Properties</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Properties</em>' attribute.
	 * @see #getProperties()
	 * @generated
	 */
	void setProperties(String value);


	/**
	 * Returns the value of the '<em><b>Time</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time</em>' attribute.
	 * @see #setTime(int)
	 * @see modelBP.ModelBPPackage#getObject_Time()
	 * @model default="0"
	 * @generated
	 */
	int getTime();

	/**
	 * Sets the value of the '{@link modelBP.Object#getTime <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time</em>' attribute.
	 * @see #getTime()
	 * @generated
	 */
	void setTime(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getDesc();

	HashMap<String, java.lang.Object> getProp();
	
	void setProp(HashMap<String, java.lang.Object> newProperties);
} // Object
