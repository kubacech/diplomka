/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelBP.Place#getHasObject <em>Has Object</em>}</li>
 *   <li>{@link modelBP.Place#getCapacity <em>Capacity</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelBP.ModelBPPackage#getPlace()
 * @model
 * @generated
 */
public interface Place extends Element {
	/**
	 * Returns the value of the '<em><b>Has Object</b></em>' containment reference list.
	 * The list contents are of type {@link modelBP.Group}.
	 * It is bidirectional and its opposite is '{@link modelBP.Group#getContained <em>Contained</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Object</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Object</em>' containment reference list.
	 * @see modelBP.ModelBPPackage#getPlace_HasObject()
	 * @see modelBP.Group#getContained
	 * @model opposite="contained" containment="true"
	 * @generated
	 */
	EList<Group> getHasObject();

	/**
	 * Returns the value of the '<em><b>Capacity</b></em>' containment reference list.
	 * The list contents are of type {@link modelBP.GroupPointer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capacity</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capacity</em>' containment reference list.
	 * @see modelBP.ModelBPPackage#getPlace_Capacity()
	 * @model containment="true"
	 * @generated
	 */
	EList<GroupPointer> getCapacity();

} // Place
