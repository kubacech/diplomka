/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see modelBP.ModelBPPackage#getActive()
 * @model
 * @generated
 */
public interface Active extends Group {
} // Active
