/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelBP.Transition#getScript <em>Script</em>}</li>
 *   <li>{@link modelBP.Transition#getTimeMin <em>Time Min</em>}</li>
 *   <li>{@link modelBP.Transition#getTimeMax <em>Time Max</em>}</li>
 *   <li>{@link modelBP.Transition#getCountMin <em>Count Min</em>}</li>
 *   <li>{@link modelBP.Transition#getCountMax <em>Count Max</em>}</li>
 *   <li>{@link modelBP.Transition#getHasScenarios <em>Has Scenarios</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelBP.ModelBPPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends Element {
	/**
	 * Returns the value of the '<em><b>Script</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Script</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Script</em>' attribute.
	 * @see #setScript(String)
	 * @see modelBP.ModelBPPackage#getTransition_Script()
	 * @model
	 * @generated
	 */
	String getScript();

	/**
	 * Sets the value of the '{@link modelBP.Transition#getScript <em>Script</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Script</em>' attribute.
	 * @see #getScript()
	 * @generated
	 */
	void setScript(String value);

	/**
	 * Returns the value of the '<em><b>Time Min</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Min</em>' attribute.
	 * @see #setTimeMin(int)
	 * @see modelBP.ModelBPPackage#getTransition_TimeMin()
	 * @model default="0"
	 * @generated
	 */
	int getTimeMin();

	/**
	 * Sets the value of the '{@link modelBP.Transition#getTimeMin <em>Time Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Min</em>' attribute.
	 * @see #getTimeMin()
	 * @generated
	 */
	void setTimeMin(int value);

	/**
	 * Returns the value of the '<em><b>Time Max</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Max</em>' attribute.
	 * @see #setTimeMax(int)
	 * @see modelBP.ModelBPPackage#getTransition_TimeMax()
	 * @model default="0"
	 * @generated
	 */
	int getTimeMax();

	/**
	 * Sets the value of the '{@link modelBP.Transition#getTimeMax <em>Time Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Max</em>' attribute.
	 * @see #getTimeMax()
	 * @generated
	 */
	void setTimeMax(int value);

	/**
	 * Returns the value of the '<em><b>Count Min</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count Min</em>' attribute.
	 * @see #setCountMin(int)
	 * @see modelBP.ModelBPPackage#getTransition_CountMin()
	 * @model default="0"
	 * @generated
	 */
	int getCountMin();

	/**
	 * Sets the value of the '{@link modelBP.Transition#getCountMin <em>Count Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count Min</em>' attribute.
	 * @see #getCountMin()
	 * @generated
	 */
	void setCountMin(int value);

	/**
	 * Returns the value of the '<em><b>Count Max</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count Max</em>' attribute.
	 * @see #setCountMax(int)
	 * @see modelBP.ModelBPPackage#getTransition_CountMax()
	 * @model default="0"
	 * @generated
	 */
	int getCountMax();

	/**
	 * Sets the value of the '{@link modelBP.Transition#getCountMax <em>Count Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count Max</em>' attribute.
	 * @see #getCountMax()
	 * @generated
	 */
	void setCountMax(int value);

	/**
	 * Returns the value of the '<em><b>Has Scenarios</b></em>' containment reference list.
	 * The list contents are of type {@link modelBP.Scenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Scenarios</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Scenarios</em>' containment reference list.
	 * @see modelBP.ModelBPPackage#getTransition_HasScenarios()
	 * @model containment="true"
	 * @generated
	 */
	EList<Scenario> getHasScenarios();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	int getTime();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	int getCount();

} // Transition
