/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Passive</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see modelBP.ModelBPPackage#getPassive()
 * @model
 * @generated
 */
public interface Passive extends Group {
} // Passive
