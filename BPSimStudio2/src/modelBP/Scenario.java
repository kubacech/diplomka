/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelBP.Scenario#getName <em>Name</em>}</li>
 *   <li>{@link modelBP.Scenario#getPercent <em>Percent</em>}</li>
 *   <li>{@link modelBP.Scenario#getTimeMin <em>Time Min</em>}</li>
 *   <li>{@link modelBP.Scenario#getTimeMax <em>Time Max</em>}</li>
 *   <li>{@link modelBP.Scenario#getCost <em>Cost</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelBP.ModelBPPackage#getScenario()
 * @model
 * @generated
 */
public interface Scenario extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see modelBP.ModelBPPackage#getScenario_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link modelBP.Scenario#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Percent</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Percent</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Percent</em>' attribute.
	 * @see #setPercent(float)
	 * @see modelBP.ModelBPPackage#getScenario_Percent()
	 * @model default="0"
	 * @generated
	 */
	float getPercent();

	/**
	 * Sets the value of the '{@link modelBP.Scenario#getPercent <em>Percent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Percent</em>' attribute.
	 * @see #getPercent()
	 * @generated
	 */
	void setPercent(float value);

	/**
	 * Returns the value of the '<em><b>Time Min</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Min</em>' attribute.
	 * @see #setTimeMin(int)
	 * @see modelBP.ModelBPPackage#getScenario_TimeMin()
	 * @model default="0"
	 * @generated
	 */
	int getTimeMin();

	/**
	 * Sets the value of the '{@link modelBP.Scenario#getTimeMin <em>Time Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Min</em>' attribute.
	 * @see #getTimeMin()
	 * @generated
	 */
	void setTimeMin(int value);

	/**
	 * Returns the value of the '<em><b>Time Max</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Max</em>' attribute.
	 * @see #setTimeMax(int)
	 * @see modelBP.ModelBPPackage#getScenario_TimeMax()
	 * @model default="0"
	 * @generated
	 */
	int getTimeMax();

	/**
	 * Sets the value of the '{@link modelBP.Scenario#getTimeMax <em>Time Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Max</em>' attribute.
	 * @see #getTimeMax()
	 * @generated
	 */
	void setTimeMax(int value);

	/**
	 * Returns the value of the '<em><b>Cost</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cost</em>' attribute.
	 * @see #setCost(int)
	 * @see modelBP.ModelBPPackage#getScenario_Cost()
	 * @model default="0"
	 * @generated
	 */
	int getCost();

	/**
	 * Sets the value of the '{@link modelBP.Scenario#getCost <em>Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cost</em>' attribute.
	 * @see #getCost()
	 * @generated
	 */
	void setCost(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	int getTime();

} // Scenario
