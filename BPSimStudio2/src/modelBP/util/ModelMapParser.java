package modelBP.util;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

public class ModelMapParser {

	public static String toString(HashMap<String, Object> map) {
		if (map == null)
			return "{}";
		HashMap<String, Object> local = new HashMap<String, Object>();
		for (Entry<String, Object> set : map.entrySet()) {
			if (set.getValue() == null) {
				local.put(set.getKey(), ("O"));
				continue;
			}
			if (set.getValue() instanceof String) {
				local.put(set.getKey(), ("S" + set.getValue()));
				continue;
			}
			if (set.getValue() instanceof Integer) {
				local.put(set.getKey(), ("I" + set.getValue()));
				continue;
			}
			if (set.getValue() instanceof Double) {
				local.put(set.getKey(), ("D" + set.getValue()));
				continue;
			}
			if (set.getValue() instanceof Float) {
				local.put(set.getKey(), ("F" + set.getValue()));
				continue;
			}
			if (set.getValue() instanceof BigDecimal) {
				local.put(set.getKey(), ("B" + set.getValue()));
				continue;
			}
			local.put(set.getKey(), ("O" + set.getValue()));

		}
		return local.toString();
	}

	public static HashMap<String, Object> toMap(String text) {
		HashMap<String, Object> res = new HashMap<String, Object>();
		Properties props = new Properties();
		try {
			props.load(new StringReader(text.substring(1, text.length() - 1)
					.replace(", ", "\n")));
		} catch (IOException e) {
			e.printStackTrace();
			return res;
		}
		for (Map.Entry<Object, Object> set : props.entrySet()) {
			;
			char type = ((String) set.getValue()).charAt(0);
			String val = null;
			if (((String) set.getValue()).length() > 1)
				val = ((String) set.getValue()).substring(1);
			switch (type) {
			case 'S':
				res.put((String) set.getKey(), (String) val);
				break;

			case 'I':
				res.put((String) set.getKey(), Integer.parseInt((String) val));
				break;

			case 'D':
				res.put((String) set.getKey(), Double.parseDouble((String) val));
				break;

			case 'O':
				res.put((String) set.getKey(), val);
				break;
			
			case 'F':
				res.put((String) set.getKey(), Float.parseFloat((String) val));
				break;

			case 'B':
				res.put((String) set.getKey(),
						BigDecimal.valueOf(Double.parseDouble((String) val)));
				break;

			default:
				break;
			}

		}
		return res;
	}
}
