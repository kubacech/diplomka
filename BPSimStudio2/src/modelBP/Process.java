/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelBP.Process#getHasElement <em>Has Element</em>}</li>
 *   <li>{@link modelBP.Process#getInput <em>Input</em>}</li>
 *   <li>{@link modelBP.Process#getOutput <em>Output</em>}</li>
 *   <li>{@link modelBP.Process#getHasGroupName <em>Has Group Name</em>}</li>
 *   <li>{@link modelBP.Process#getGlobalTime <em>Global Time</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelBP.ModelBPPackage#getProcess()
 * @model
 * @generated
 */
public interface Process extends Element {
	/**
	 * Returns the value of the '<em><b>Has Element</b></em>' containment reference list.
	 * The list contents are of type {@link modelBP.Element}.
	 * It is bidirectional and its opposite is '{@link modelBP.Element#getAssigned <em>Assigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Element</em>' containment reference list.
	 * @see modelBP.ModelBPPackage#getProcess_HasElement()
	 * @see modelBP.Element#getAssigned
	 * @model opposite="assigned" containment="true"
	 * @generated
	 */
	EList<Element> getHasElement();

	/**
	 * Returns the value of the '<em><b>Input</b></em>' reference list.
	 * The list contents are of type {@link modelBP.Element}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input</em>' reference list.
	 * @see modelBP.ModelBPPackage#getProcess_Input()
	 * @model
	 * @generated
	 */
	EList<Element> getInput();

	/**
	 * Returns the value of the '<em><b>Output</b></em>' reference list.
	 * The list contents are of type {@link modelBP.Element}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output</em>' reference list.
	 * @see modelBP.ModelBPPackage#getProcess_Output()
	 * @model
	 * @generated
	 */
	EList<Element> getOutput();

	/**
	 * Returns the value of the '<em><b>Has Group Name</b></em>' containment reference list.
	 * The list contents are of type {@link modelBP.GroupName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Group Name</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Group Name</em>' containment reference list.
	 * @see modelBP.ModelBPPackage#getProcess_HasGroupName()
	 * @model containment="true"
	 * @generated
	 */
	EList<GroupName> getHasGroupName();

	/**
	 * Returns the value of the '<em><b>Global Time</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Time</em>' attribute.
	 * @see #setGlobalTime(int)
	 * @see modelBP.ModelBPPackage#getProcess_GlobalTime()
	 * @model default="0" transient="true"
	 * @generated
	 */
	int getGlobalTime();

	/**
	 * Sets the value of the '{@link modelBP.Process#getGlobalTime <em>Global Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Global Time</em>' attribute.
	 * @see #getGlobalTime()
	 * @generated
	 */
	void setGlobalTime(int value);

} // Process
