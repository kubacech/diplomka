/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelBP.Edge#getSource <em>Source</em>}</li>
 *   <li>{@link modelBP.Edge#getTarget <em>Target</em>}</li>
 *   <li>{@link modelBP.Edge#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link modelBP.Edge#getScenarios <em>Scenarios</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelBP.ModelBPPackage#getEdge()
 * @model
 * @generated
 */
public interface Edge extends Element {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link modelBP.Element#getLinkedOut <em>Linked Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(Element)
	 * @see modelBP.ModelBPPackage#getEdge_Source()
	 * @see modelBP.Element#getLinkedOut
	 * @model opposite="linkedOut" required="true"
	 * @generated
	 */
	Element getSource();

	/**
	 * Sets the value of the '{@link modelBP.Edge#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Element value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link modelBP.Element#getLinkedIn <em>Linked In</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Element)
	 * @see modelBP.ModelBPPackage#getEdge_Target()
	 * @see modelBP.Element#getLinkedIn
	 * @model opposite="linkedIn" required="true"
	 * @generated
	 */
	Element getTarget();

	/**
	 * Sets the value of the '{@link modelBP.Edge#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Element value);

	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference list.
	 * The list contents are of type {@link modelBP.GroupPointer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference list.
	 * @see modelBP.ModelBPPackage#getEdge_Multiplicity()
	 * @model containment="true"
	 * @generated
	 */
	EList<GroupPointer> getMultiplicity();

	/**
	 * Returns the value of the '<em><b>Scenarios</b></em>' reference list.
	 * The list contents are of type {@link modelBP.Scenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenarios</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenarios</em>' reference list.
	 * @see modelBP.ModelBPPackage#getEdge_Scenarios()
	 * @model
	 * @generated
	 */
	EList<Scenario> getScenarios();

} // Edge
