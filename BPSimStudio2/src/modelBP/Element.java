/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package modelBP;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelBP.Element#getName <em>Name</em>}</li>
 *   <li>{@link modelBP.Element#getLinkedOut <em>Linked Out</em>}</li>
 *   <li>{@link modelBP.Element#getAssigned <em>Assigned</em>}</li>
 *   <li>{@link modelBP.Element#getDuplicated <em>Duplicated</em>}</li>
 *   <li>{@link modelBP.Element#getDescription <em>Description</em>}</li>
 *   <li>{@link modelBP.Element#getLinkedIn <em>Linked In</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelBP.ModelBPPackage#getElement()
 * @model
 * @generated
 */
public interface Element extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see modelBP.ModelBPPackage#getElement_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link modelBP.Element#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Linked Out</b></em>' reference list.
	 * The list contents are of type {@link modelBP.Edge}.
	 * It is bidirectional and its opposite is '{@link modelBP.Edge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linked Out</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linked Out</em>' reference list.
	 * @see modelBP.ModelBPPackage#getElement_LinkedOut()
	 * @see modelBP.Edge#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<Edge> getLinkedOut();

	/**
	 * Returns the value of the '<em><b>Assigned</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link modelBP.Process#getHasElement <em>Has Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assigned</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assigned</em>' container reference.
	 * @see #setAssigned(modelBP.Process)
	 * @see modelBP.ModelBPPackage#getElement_Assigned()
	 * @see modelBP.Process#getHasElement
	 * @model opposite="hasElement" transient="false"
	 * @generated
	 */
	modelBP.Process getAssigned();

	/**
	 * Sets the value of the '{@link modelBP.Element#getAssigned <em>Assigned</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assigned</em>' container reference.
	 * @see #getAssigned()
	 * @generated
	 */
	void setAssigned(modelBP.Process value);

	/**
	 * Returns the value of the '<em><b>Duplicated</b></em>' reference list.
	 * The list contents are of type {@link modelBP.Element}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duplicated</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duplicated</em>' reference list.
	 * @see modelBP.ModelBPPackage#getElement_Duplicated()
	 * @model
	 * @generated
	 */
	EList<Element> getDuplicated();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see modelBP.ModelBPPackage#getElement_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link modelBP.Element#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Linked In</b></em>' reference list.
	 * The list contents are of type {@link modelBP.Edge}.
	 * It is bidirectional and its opposite is '{@link modelBP.Edge#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linked In</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linked In</em>' reference list.
	 * @see modelBP.ModelBPPackage#getElement_LinkedIn()
	 * @see modelBP.Edge#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<Edge> getLinkedIn();

} // Element
