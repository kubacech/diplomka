/**
 */
package modelBP;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group Pointer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelBP.GroupPointer#getCount <em>Count</em>}</li>
 *   <li>{@link modelBP.GroupPointer#getNamed <em>Named</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelBP.ModelBPPackage#getGroupPointer()
 * @model
 * @generated
 */
public interface GroupPointer extends EObject {
	/**
	 * Returns the value of the '<em><b>Count</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count</em>' attribute.
	 * @see #setCount(int)
	 * @see modelBP.ModelBPPackage#getGroupPointer_Count()
	 * @model default="0"
	 * @generated
	 */
	int getCount();

	/**
	 * Sets the value of the '{@link modelBP.GroupPointer#getCount <em>Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count</em>' attribute.
	 * @see #getCount()
	 * @generated
	 */
	void setCount(int value);

	/**
	 * Returns the value of the '<em><b>Named</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named</em>' reference.
	 * @see #setNamed(GroupName)
	 * @see modelBP.ModelBPPackage#getGroupPointer_Named()
	 * @model required="true"
	 * @generated
	 */
	GroupName getNamed();

	/**
	 * Sets the value of the '{@link modelBP.GroupPointer#getNamed <em>Named</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Named</em>' reference.
	 * @see #getNamed()
	 * @generated
	 */
	void setNamed(GroupName value);

} // GroupPointer
