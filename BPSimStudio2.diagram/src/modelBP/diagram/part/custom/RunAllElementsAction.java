package modelBP.diagram.part.custom;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import bpsimstudio2.simulace.PesInstance;

/**
 * @generated
 */
public class RunAllElementsAction extends Action {

	EObject target = null;
	EStructuralFeature targetValue = null;
	Object newValue = null;
	IWorkbenchPart part;

	public RunAllElementsAction() {
		super();
	}

	public void init(IWorkbenchPart part) {
		this.part = part;
		this.setEnabled(true);
		setId("runAll");
		setText("Proveď celou mapu");
		setToolTipText("Program náhodně provádí přechody");
		ISharedImages workbenchImages = PlatformUI.getWorkbench()
				.getSharedImages();
		setHoverImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_REDO));
		setImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_REDO));
		setDisabledImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_REDO_DISABLED));
	}

	@Override
	public void run() {
		super.run();

		PesInstance.getInstance().runAll(part);

	}
}
