package modelBP.diagram.part.custom;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import bpsimstudio2.simulace.PesInstance;

/**
 * @generated
 */
public class SaveDiagramStateAction extends Action {

	EObject target = null;
	EStructuralFeature targetValue = null;
	Object newValue = null;
	IWorkbenchPart part;

	public SaveDiagramStateAction() {
		super();
	}

	public void init(IWorkbenchPart part) {
		this.part = part;
		this.setEnabled(true);
		setId("saveAll");
		setText("Uložit stav sítě");
		setToolTipText("Program obnoví celou síť do stavu po načtení");
		ISharedImages workbenchImages = PlatformUI.getWorkbench()
				.getSharedImages();
		setHoverImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_ETOOL_SAVE_EDIT));
		setImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_ETOOL_SAVE_EDIT));
		setDisabledImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_ETOOL_SAVE_EDIT_DISABLED));
	}

	@Override
	public void run() {
		super.run();
		PesInstance.getInstance().saveState(part);

	}
}
