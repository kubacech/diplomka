package modelBP.diagram.part.custom;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import bpsimstudio2.simulace.PesInstance;

/**
 * @generated
 */
public class ResetDiagramAction extends Action {

	EObject target = null;
	EStructuralFeature targetValue = null;
	Object newValue = null;
	IWorkbenchPart part;

	public ResetDiagramAction() {
		super();
	}

	public void init(IWorkbenchPart part) {
		this.part = part;
		this.setEnabled(true);
		setId("moje");
		setText("Obnovit místa");
		setToolTipText("Program obnoví místa do stavu po načtení");
		ISharedImages workbenchImages = PlatformUI.getWorkbench()
				.getSharedImages();
		setHoverImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_UNDO));
		setImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_UNDO));
		setDisabledImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_UNDO_DISABLED));
	}

	@Override
	public void run() {
		super.run();
		PesInstance.getInstance().resetNetPlaces(part);

	}
}
