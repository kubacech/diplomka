package modelBP.diagram.part.custom;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import bpsimstudio2.simulace.PesInstance;

/**
 * @generated
 */
public class ResetWholeDiagramAction extends Action {

	EObject target = null;
	EStructuralFeature targetValue = null;
	Object newValue = null;
	IWorkbenchPart part;

	public ResetWholeDiagramAction() {
		super();
	}

	public void init(IWorkbenchPart part) {
		this.part = part;
		this.setEnabled(true);
		setId("resetAll");
		setText("Obnovit celou síť");
		setToolTipText("Program obnoví síť do stavu po načtení");
		ISharedImages workbenchImages = PlatformUI.getWorkbench()
				.getSharedImages();
		setHoverImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_UNDO));
		setImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_UNDO));
		setDisabledImageDescriptor(workbenchImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_UNDO_DISABLED));
	}

	@Override
	public void run() {
		super.run();
		PesInstance.getInstance().resetWholeNet(part);

	}
}
