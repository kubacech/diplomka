package modelBP.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.tooling.runtime.update.UpdaterNodeDescriptor;

/**
 * @generated
 */
public class BPmodelNodeDescriptor extends UpdaterNodeDescriptor {

	/**
	 * @generated
	 */
	public BPmodelNodeDescriptor(EObject modelElement, int visualID) {
		super(modelElement, visualID);
	}

}
