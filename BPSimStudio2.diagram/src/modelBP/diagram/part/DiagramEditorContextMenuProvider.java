package modelBP.diagram.part;

import java.awt.Menu;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gmf.runtime.common.ui.services.action.contributionitem.ContributionItemService;
import org.eclipse.gmf.runtime.diagram.ui.actions.ActionIds;
import org.eclipse.gmf.runtime.diagram.ui.providers.DiagramContextMenuProvider;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IWorkbenchPart;

/**
 * @generated
 */
public class DiagramEditorContextMenuProvider extends
		DiagramContextMenuProvider {

	/**
	 * @generated
	 */
	private IWorkbenchPart part;

	/**
	 * @generated NOT
	 */
	private modelBP.diagram.part.DeleteElementAction deleteAction;
	private modelBP.diagram.part.custom.RunElementAction runRndAction;
	private modelBP.diagram.part.custom.RunAllElementsAction runAllAction;
	private modelBP.diagram.part.custom.RunAllElementsMoreAction runAllActionMore;
	private modelBP.diagram.part.custom.ResetDiagramAction resetAction;
	private modelBP.diagram.part.custom.ResetWholeDiagramAction resetWholeAction;
	private modelBP.diagram.part.custom.SaveDiagramStateAction saveStateAction;
	private modelBP.diagram.part.custom.ShowObjectsStatAction showObjAction;
	private modelBP.diagram.part.custom.ShowNetStatAction showNetAction;

	/**
	 * @generated NOT
	 */
	public DiagramEditorContextMenuProvider(IWorkbenchPart part,
			EditPartViewer viewer) {
		super(part, viewer);
		this.part = part;
		deleteAction = new modelBP.diagram.part.DeleteElementAction(this.part);
		deleteAction.init();
		runRndAction = new modelBP.diagram.part.custom.RunElementAction();
		runRndAction.init(this.part);
		runAllAction = new modelBP.diagram.part.custom.RunAllElementsAction();
		runAllAction.init(this.part);
		runAllActionMore = new modelBP.diagram.part.custom.RunAllElementsMoreAction();
		runAllActionMore.init(this.part);
		resetAction = new modelBP.diagram.part.custom.ResetDiagramAction();
		resetAction.init(this.part);
		resetWholeAction = new modelBP.diagram.part.custom.ResetWholeDiagramAction();
		resetWholeAction.init(this.part);
		saveStateAction = new modelBP.diagram.part.custom.SaveDiagramStateAction();
		saveStateAction.init(this.part);
		showObjAction = new modelBP.diagram.part.custom.ShowObjectsStatAction();
		showObjAction.init(this.part);
		showNetAction = new modelBP.diagram.part.custom.ShowNetStatAction();
		showNetAction.init(this.part);
	}

	/**
	 * @generated NOT
	 */
	public void dispose() {
		if (deleteAction != null) {
			deleteAction.dispose();
			deleteAction = null;
		}
		if (runRndAction != null) {
			runRndAction = null;
		}
		if (runAllAction != null) {
			runAllAction = null;
		}

		if (runAllActionMore != null) {
			runAllActionMore = null;
		}
		if (resetAction != null) {
			resetAction = null;
		}
		if (resetWholeAction != null) {
			resetWholeAction = null;
		}
		if (saveStateAction != null) {
			saveStateAction = null;
		}
		if (showObjAction != null) {
			showObjAction = null;
		}
		if (showNetAction != null) {
			showNetAction = null;
		}
		super.dispose();
	}

	/**
	 * @generated NOT
	 */
	public void buildContextMenu(final IMenuManager menu) {
		getViewer().flush();
		try {
			TransactionUtil.getEditingDomain(
					(EObject) getViewer().getContents().getModel())
					.runExclusive(new Runnable() {

						public void run() {
							MenuManager sim = new MenuManager("Simulace");
							menu.add(sim);
							MenuManager stav = new MenuManager("Stav sítě");
							menu.add(stav);
							MenuManager stat = new MenuManager("Statistika");
							menu.add(stat);
							sim.add(runRndAction);
							sim.add(runAllAction);
							sim.add(runAllActionMore);
							stav.add(resetAction);
							stav.add(resetWholeAction);
							stav.add(saveStateAction);
							stat.add(showObjAction);
							stat.add(showNetAction);
							ContributionItemService
									.getInstance()
									.contributeToPopupMenu(
											DiagramEditorContextMenuProvider.this,
											part);
							menu.remove(ActionIds.ACTION_DELETE_FROM_MODEL);
							menu.appendToGroup("editGroup", deleteAction);
							
						}
					});
		} catch (Exception e) {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Error building context menu", e);
		}
	}
}
