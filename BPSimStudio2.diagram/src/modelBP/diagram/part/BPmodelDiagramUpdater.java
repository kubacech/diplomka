package modelBP.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;

/**
 * @generated
 */
public class BPmodelDiagramUpdater {

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelNodeDescriptor> getSemanticChildren(
			View view) {
		switch (modelBP.diagram.part.BPmodelVisualIDRegistry.getVisualID(view)) {
		case modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID:
			return getProcess_1000SemanticChildren(view);
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart.VISUAL_ID:
			return getPlacePlaceCompartmentFigure_7001SemanticChildren(view);
		case modelBP.diagram.edit.parts.ProcessProcessInputCompartmentFigureEditPart.VISUAL_ID:
			return getProcessProcessInputCompartmentFigure_7002SemanticChildren(view);
		case modelBP.diagram.edit.parts.ProcessProcessOutputCompartmentFigureEditPart.VISUAL_ID:
			return getProcessProcessOutputCompartmentFigure_7003SemanticChildren(view);
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure2EditPart.VISUAL_ID:
			return getPlacePlaceCompartmentFigure_7004SemanticChildren(view);
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure3EditPart.VISUAL_ID:
			return getPlacePlaceCompartmentFigure_7005SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelNodeDescriptor> getProcess_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		modelBP.Process modelElement = (modelBP.Process) view.getElement();
		LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor>();
		for (Iterator<?> it = modelElement.getHasElement().iterator(); it
				.hasNext();) {
			modelBP.Element childElement = (modelBP.Element) it.next();
			int visualID = modelBP.diagram.part.BPmodelVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelNodeDescriptor> getPlacePlaceCompartmentFigure_7001SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		modelBP.Place modelElement = (modelBP.Place) containerView.getElement();
		LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor>();
		for (Iterator<?> it = modelElement.getHasObject().iterator(); it
				.hasNext();) {
			modelBP.Group childElement = (modelBP.Group) it.next();
			int visualID = modelBP.diagram.part.BPmodelVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelNodeDescriptor> getProcessProcessInputCompartmentFigure_7002SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		modelBP.Process modelElement = (modelBP.Process) containerView
				.getElement();
		LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor>();
		for (Iterator<?> it = modelElement.getInput().iterator(); it.hasNext();) {
			modelBP.Element childElement = (modelBP.Element) it.next();
			int visualID = modelBP.diagram.part.BPmodelVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelNodeDescriptor> getProcessProcessOutputCompartmentFigure_7003SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		modelBP.Process modelElement = (modelBP.Process) containerView
				.getElement();
		LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor>();
		for (Iterator<?> it = modelElement.getOutput().iterator(); it.hasNext();) {
			modelBP.Element childElement = (modelBP.Element) it.next();
			int visualID = modelBP.diagram.part.BPmodelVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelNodeDescriptor> getPlacePlaceCompartmentFigure_7004SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		modelBP.Place modelElement = (modelBP.Place) containerView.getElement();
		LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor>();
		for (Iterator<?> it = modelElement.getHasObject().iterator(); it
				.hasNext();) {
			modelBP.Group childElement = (modelBP.Group) it.next();
			int visualID = modelBP.diagram.part.BPmodelVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelNodeDescriptor> getPlacePlaceCompartmentFigure_7005SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		modelBP.Place modelElement = (modelBP.Place) containerView.getElement();
		LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelNodeDescriptor>();
		for (Iterator<?> it = modelElement.getHasObject().iterator(); it
				.hasNext();) {
			modelBP.Group childElement = (modelBP.Group) it.next();
			int visualID = modelBP.diagram.part.BPmodelVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID) {
				result.add(new modelBP.diagram.part.BPmodelNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getContainedLinks(
			View view) {
		switch (modelBP.diagram.part.BPmodelVisualIDRegistry.getVisualID(view)) {
		case modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID:
			return getProcess_1000ContainedLinks(view);
		case modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID:
			return getPlace_2001ContainedLinks(view);
		case modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID:
			return getProcess_2002ContainedLinks(view);
		case modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID:
			return getTransition_2003ContainedLinks(view);
		case modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID:
			return getPassive_3001ContainedLinks(view);
		case modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID:
			return getActive_3002ContainedLinks(view);
		case modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID:
			return getPlace_3003ContainedLinks(view);
		case modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID:
			return getTransition_3004ContainedLinks(view);
		case modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID:
			return getPlace_3005ContainedLinks(view);
		case modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID:
			return getTransition_3006ContainedLinks(view);
		case modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID:
			return getEdge_4001ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getIncomingLinks(
			View view) {
		switch (modelBP.diagram.part.BPmodelVisualIDRegistry.getVisualID(view)) {
		case modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID:
			return getPlace_2001IncomingLinks(view);
		case modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID:
			return getProcess_2002IncomingLinks(view);
		case modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID:
			return getTransition_2003IncomingLinks(view);
		case modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID:
			return getPassive_3001IncomingLinks(view);
		case modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID:
			return getActive_3002IncomingLinks(view);
		case modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID:
			return getPlace_3003IncomingLinks(view);
		case modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID:
			return getTransition_3004IncomingLinks(view);
		case modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID:
			return getPlace_3005IncomingLinks(view);
		case modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID:
			return getTransition_3006IncomingLinks(view);
		case modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID:
			return getEdge_4001IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getOutgoingLinks(
			View view) {
		switch (modelBP.diagram.part.BPmodelVisualIDRegistry.getVisualID(view)) {
		case modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID:
			return getPlace_2001OutgoingLinks(view);
		case modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID:
			return getProcess_2002OutgoingLinks(view);
		case modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID:
			return getTransition_2003OutgoingLinks(view);
		case modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID:
			return getPassive_3001OutgoingLinks(view);
		case modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID:
			return getActive_3002OutgoingLinks(view);
		case modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID:
			return getPlace_3003OutgoingLinks(view);
		case modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID:
			return getTransition_3004OutgoingLinks(view);
		case modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID:
			return getPlace_3005OutgoingLinks(view);
		case modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID:
			return getTransition_3006OutgoingLinks(view);
		case modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID:
			return getEdge_4001OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getProcess_1000ContainedLinks(
			View view) {
		modelBP.Process modelElement = (modelBP.Process) view.getElement();
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Edge_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPlace_2001ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getProcess_2002ContainedLinks(
			View view) {
		modelBP.Process modelElement = (modelBP.Process) view.getElement();
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Edge_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getTransition_2003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPassive_3001ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getActive_3002ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPlace_3003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getTransition_3004ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPlace_3005ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getTransition_3006ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getEdge_4001ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPlace_2001IncomingLinks(
			View view) {
		modelBP.Place modelElement = (modelBP.Place) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getProcess_2002IncomingLinks(
			View view) {
		modelBP.Process modelElement = (modelBP.Process) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getTransition_2003IncomingLinks(
			View view) {
		modelBP.Transition modelElement = (modelBP.Transition) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPassive_3001IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getActive_3002IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPlace_3003IncomingLinks(
			View view) {
		modelBP.Place modelElement = (modelBP.Place) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getTransition_3004IncomingLinks(
			View view) {
		modelBP.Transition modelElement = (modelBP.Transition) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPlace_3005IncomingLinks(
			View view) {
		modelBP.Place modelElement = (modelBP.Place) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getTransition_3006IncomingLinks(
			View view) {
		modelBP.Transition modelElement = (modelBP.Transition) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getEdge_4001IncomingLinks(
			View view) {
		modelBP.Edge modelElement = (modelBP.Edge) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Edge_4001(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPlace_2001OutgoingLinks(
			View view) {
		modelBP.Place modelElement = (modelBP.Place) view.getElement();
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getProcess_2002OutgoingLinks(
			View view) {
		modelBP.Process modelElement = (modelBP.Process) view.getElement();
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getTransition_2003OutgoingLinks(
			View view) {
		modelBP.Transition modelElement = (modelBP.Transition) view
				.getElement();
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPassive_3001OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getActive_3002OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPlace_3003OutgoingLinks(
			View view) {
		modelBP.Place modelElement = (modelBP.Place) view.getElement();
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getTransition_3004OutgoingLinks(
			View view) {
		modelBP.Transition modelElement = (modelBP.Transition) view
				.getElement();
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getPlace_3005OutgoingLinks(
			View view) {
		modelBP.Place modelElement = (modelBP.Place) view.getElement();
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getTransition_3006OutgoingLinks(
			View view) {
		modelBP.Transition modelElement = (modelBP.Transition) view
				.getElement();
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<modelBP.diagram.part.BPmodelLinkDescriptor> getEdge_4001OutgoingLinks(
			View view) {
		modelBP.Edge modelElement = (modelBP.Edge) view.getElement();
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Edge_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<modelBP.diagram.part.BPmodelLinkDescriptor> getContainedTypeModelFacetLinks_Edge_4001(
			modelBP.Process container) {
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		for (Iterator<?> links = container.getHasElement().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof modelBP.Edge) {
				continue;
			}
			modelBP.Edge link = (modelBP.Edge) linkObject;
			if (modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID != modelBP.diagram.part.BPmodelVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			modelBP.Element dst = link.getTarget();
			modelBP.Element src = link.getSource();
			result.add(new modelBP.diagram.part.BPmodelLinkDescriptor(src, dst,
					link,
					modelBP.diagram.providers.BPmodelElementTypes.Edge_4001,
					modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<modelBP.diagram.part.BPmodelLinkDescriptor> getIncomingTypeModelFacetLinks_Edge_4001(
			modelBP.Element target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != modelBP.ModelBPPackage.eINSTANCE
					.getEdge_Target()
					|| false == setting.getEObject() instanceof modelBP.Edge) {
				continue;
			}
			modelBP.Edge link = (modelBP.Edge) setting.getEObject();
			if (modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID != modelBP.diagram.part.BPmodelVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			modelBP.Element src = link.getSource();
			result.add(new modelBP.diagram.part.BPmodelLinkDescriptor(src,
					target, link,
					modelBP.diagram.providers.BPmodelElementTypes.Edge_4001,
					modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<modelBP.diagram.part.BPmodelLinkDescriptor> getOutgoingTypeModelFacetLinks_Edge_4001(
			modelBP.Element source) {
		modelBP.Process container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof modelBP.Process) {
				container = (modelBP.Process) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor> result = new LinkedList<modelBP.diagram.part.BPmodelLinkDescriptor>();
		for (Iterator<?> links = container.getHasElement().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof modelBP.Edge) {
				continue;
			}
			modelBP.Edge link = (modelBP.Edge) linkObject;
			if (modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID != modelBP.diagram.part.BPmodelVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			modelBP.Element dst = link.getTarget();
			modelBP.Element src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new modelBP.diagram.part.BPmodelLinkDescriptor(src, dst,
					link,
					modelBP.diagram.providers.BPmodelElementTypes.Edge_4001,
					modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<modelBP.diagram.part.BPmodelNodeDescriptor> getSemanticChildren(
				View view) {
			return BPmodelDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<modelBP.diagram.part.BPmodelLinkDescriptor> getContainedLinks(
				View view) {
			return BPmodelDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<modelBP.diagram.part.BPmodelLinkDescriptor> getIncomingLinks(
				View view) {
			return BPmodelDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<modelBP.diagram.part.BPmodelLinkDescriptor> getOutgoingLinks(
				View view) {
			return BPmodelDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
