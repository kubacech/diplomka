package modelBP.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String BPmodelCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String BPmodelCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String BPmodelCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String BPmodelCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String BPmodelCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String BPmodelCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String BPmodelCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String BPmodelCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String BPmodelDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String BPmodelDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String BPmodelDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String BPmodelDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String BPmodelDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String BPmodelDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String BPmodelDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String BPmodelDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String BPmodelDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String BPmodelDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String BPmodelDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String BPmodelDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String BPmodelDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String BPmodelNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String BPmodelDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String BPmodelDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String BPmodelDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String BPmodelDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String BPmodelDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String BPmodelElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Elementy1Group_title;

	/**
	 * @generated
	 */
	public static String Elementy1Group_desc;

	/**
	 * @generated
	 */
	public static String Hrany2Group_title;

	/**
	 * @generated
	 */
	public static String Objekty3Group_title;

	/**
	 * @generated
	 */
	public static String Proces1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Proces1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Misto2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Misto2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Prechod3CreationTool_title;

	/**
	 * @generated
	 */
	public static String Prechod3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Hrana1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Hrana1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Aktivni1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Aktivni1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Pasivni2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Pasivni2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String PlacePlaceCompartmentFigureEditPart_title;

	/**
	 * @generated
	 */
	public static String ProcessProcessInputCompartmentFigureEditPart_title;

	/**
	 * @generated
	 */
	public static String ProcessProcessOutputCompartmentFigureEditPart_title;

	/**
	 * @generated
	 */
	public static String PlacePlaceCompartmentFigure2EditPart_title;

	/**
	 * @generated
	 */
	public static String PlacePlaceCompartmentFigure3EditPart_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Edge_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Edge_4001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Edge_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Edge_4001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Process_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Place_3005_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Place_3005_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Transition_2003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Transition_2003_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Transition_3006_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Transition_3006_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Place_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Place_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Process_2002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Process_2002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Place_3003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Place_3003_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Transition_3004_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Transition_3004_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String BPmodelModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String BPmodelModelingAssistantProviderMessage;

	public static String ApplicationMenuName_File;

	public static String ApplicationMenuName_New;

	public static String ApplicationMenuName_Edit;

	public static String ApplicationMenuName_Window;

	public static String ApplicationMenuName_Help;

	public static String DiagramEditorActionBarAdvisor_DefaultFileEditorTitle;

	public static String DiagramEditorActionBarAdvisor_DefaultFileEditorMessage;

	public static String DiagramEditorActionBarAdvisor_AboutDialogMessage;

	public static String DiagramEditorActionBarAdvisor_AboutDialogTitle;

	public static String DiagramEditorActionBarAdvisor_DefaultEditorOpenErrorTitle;

	public static String WizardNewFileCreationPage_BrowseButton;

	public static String WizardNewFileCreationPage_FileLabel;

	public static String WizardNewFileCreationPage_SelectNewFileDialog;

	public static String WizardNewFileCreationPage_EmptyFileNameError;

	public static String WizardNewFileCreationPage_InvalidFileNameError;

	public static String DiagramEditorWorkbenchWindowAdvisor_Title;

	//TODO: put accessor fields manually	
}
