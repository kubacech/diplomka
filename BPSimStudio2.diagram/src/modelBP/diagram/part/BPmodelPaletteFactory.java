package modelBP.diagram.part;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

/**
 * @generated
 */
public class BPmodelPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createElementy1Group());
		paletteRoot.add(createHrany2Group());
		paletteRoot.add(createObjekty3Group());
	}

	/**
	 * Creates "Elementy" palette tool group
	 * @generated
	 */
	private PaletteContainer createElementy1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				modelBP.diagram.part.Messages.Elementy1Group_title);
		paletteContainer.setId("createElementy1Group"); //$NON-NLS-1$
		paletteContainer
				.setDescription(modelBP.diagram.part.Messages.Elementy1Group_desc);
		paletteContainer.add(createProces1CreationTool());
		paletteContainer.add(createMisto2CreationTool());
		paletteContainer.add(createPrechod3CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Hrany" palette tool group
	 * @generated
	 */
	private PaletteContainer createHrany2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				modelBP.diagram.part.Messages.Hrany2Group_title);
		paletteContainer.setId("createHrany2Group"); //$NON-NLS-1$
		paletteContainer.add(createHrana1CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Objekty" palette tool group
	 * @generated
	 */
	private PaletteContainer createObjekty3Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				modelBP.diagram.part.Messages.Objekty3Group_title);
		paletteContainer.setId("createObjekty3Group"); //$NON-NLS-1$
		paletteContainer.add(createAktivni1CreationTool());
		paletteContainer.add(createPasivni2CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createProces1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				modelBP.diagram.part.Messages.Proces1CreationTool_title,
				modelBP.diagram.part.Messages.Proces1CreationTool_desc,
				Collections
						.singletonList(modelBP.diagram.providers.BPmodelElementTypes.Process_2002));
		entry.setId("createProces1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(modelBP.diagram.providers.BPmodelElementTypes
				.getImageDescriptor(modelBP.diagram.providers.BPmodelElementTypes.Process_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createMisto2CreationTool() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(3);
		types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_2001);
		types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3003);
		types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3005);
		NodeToolEntry entry = new NodeToolEntry(
				modelBP.diagram.part.Messages.Misto2CreationTool_title,
				modelBP.diagram.part.Messages.Misto2CreationTool_desc, types);
		entry.setId("createMisto2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(modelBP.diagram.providers.BPmodelElementTypes
				.getImageDescriptor(modelBP.diagram.providers.BPmodelElementTypes.Place_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createPrechod3CreationTool() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(3);
		types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3004);
		types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3006);
		types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_2003);
		NodeToolEntry entry = new NodeToolEntry(
				modelBP.diagram.part.Messages.Prechod3CreationTool_title,
				modelBP.diagram.part.Messages.Prechod3CreationTool_desc, types);
		entry.setId("createPrechod3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(modelBP.diagram.providers.BPmodelElementTypes
				.getImageDescriptor(modelBP.diagram.providers.BPmodelElementTypes.Transition_3004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createHrana1CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				modelBP.diagram.part.Messages.Hrana1CreationTool_title,
				modelBP.diagram.part.Messages.Hrana1CreationTool_desc,
				Collections
						.singletonList(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001));
		entry.setId("createHrana1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(modelBP.diagram.providers.BPmodelElementTypes
				.getImageDescriptor(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAktivni1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				modelBP.diagram.part.Messages.Aktivni1CreationTool_title,
				modelBP.diagram.part.Messages.Aktivni1CreationTool_desc,
				Collections
						.singletonList(modelBP.diagram.providers.BPmodelElementTypes.Active_3002));
		entry.setId("createAktivni1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(modelBP.diagram.providers.BPmodelElementTypes
				.getImageDescriptor(modelBP.diagram.providers.BPmodelElementTypes.Active_3002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createPasivni2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				modelBP.diagram.part.Messages.Pasivni2CreationTool_title,
				modelBP.diagram.part.Messages.Pasivni2CreationTool_desc,
				Collections
						.singletonList(modelBP.diagram.providers.BPmodelElementTypes.Passive_3001));
		entry.setId("createPasivni2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(modelBP.diagram.providers.BPmodelElementTypes
				.getImageDescriptor(modelBP.diagram.providers.BPmodelElementTypes.Passive_3001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
