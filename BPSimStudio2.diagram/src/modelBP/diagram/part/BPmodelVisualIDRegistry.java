package modelBP.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class BPmodelVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "BPSimStudio2.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (modelBP.diagram.edit.parts.ProcessEditPart.MODEL_ID.equals(view
					.getType())) {
				return modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return modelBP.diagram.part.BPmodelVisualIDRegistry.getVisualID(view
				.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
						.logError(
								"Unable to parse view type as a visualID number: "
										+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (modelBP.ModelBPPackage.eINSTANCE.getProcess().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((modelBP.Process) domainElement)) {
			return modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = modelBP.diagram.part.BPmodelVisualIDRegistry
				.getModelID(containerView);
		if (!modelBP.diagram.edit.parts.ProcessEditPart.MODEL_ID
				.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (modelBP.diagram.edit.parts.ProcessEditPart.MODEL_ID
				.equals(containerModelID)) {
			containerVisualID = modelBP.diagram.part.BPmodelVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID:
			if (modelBP.ModelBPPackage.eINSTANCE.getPlace().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID;
			}
			if (modelBP.ModelBPPackage.eINSTANCE.getProcess().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID;
			}
			if (modelBP.ModelBPPackage.eINSTANCE.getTransition().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID;
			}
			break;
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart.VISUAL_ID:
			if (modelBP.ModelBPPackage.eINSTANCE.getPassive().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID;
			}
			if (modelBP.ModelBPPackage.eINSTANCE.getActive().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID;
			}
			break;
		case modelBP.diagram.edit.parts.ProcessProcessInputCompartmentFigureEditPart.VISUAL_ID:
			if (modelBP.ModelBPPackage.eINSTANCE.getTransition().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID;
			}
			if (modelBP.ModelBPPackage.eINSTANCE.getPlace().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID;
			}
			break;
		case modelBP.diagram.edit.parts.ProcessProcessOutputCompartmentFigureEditPart.VISUAL_ID:
			if (modelBP.ModelBPPackage.eINSTANCE.getPlace().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID;
			}
			if (modelBP.ModelBPPackage.eINSTANCE.getTransition().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID;
			}
			break;
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure2EditPart.VISUAL_ID:
			if (modelBP.ModelBPPackage.eINSTANCE.getPassive().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID;
			}
			if (modelBP.ModelBPPackage.eINSTANCE.getActive().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID;
			}
			break;
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure3EditPart.VISUAL_ID:
			if (modelBP.ModelBPPackage.eINSTANCE.getPassive().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID;
			}
			if (modelBP.ModelBPPackage.eINSTANCE.getActive().isSuperTypeOf(
					domainElement.eClass())) {
				return modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = modelBP.diagram.part.BPmodelVisualIDRegistry
				.getModelID(containerView);
		if (!modelBP.diagram.edit.parts.ProcessEditPart.MODEL_ID
				.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (modelBP.diagram.edit.parts.ProcessEditPart.MODEL_ID
				.equals(containerModelID)) {
			containerVisualID = modelBP.diagram.part.BPmodelVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.PlaceNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.ProcessNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.ProcessProcessInputCompartmentFigureEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.ProcessProcessOutputCompartmentFigureEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.TransitionNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.PassiveCountEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.ActiveCountEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.PlaceName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.TransitionName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.PlaceName3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.TransitionName3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.ProcessProcessInputCompartmentFigureEditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.ProcessProcessOutputCompartmentFigureEditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure2EditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure3EditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID:
			if (modelBP.diagram.edit.parts.EdgeNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (modelBP.ModelBPPackage.eINSTANCE.getEdge().isSuperTypeOf(
				domainElement.eClass())) {
			return modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(modelBP.Process element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		switch (visualID) {
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart.VISUAL_ID:
		case modelBP.diagram.edit.parts.ProcessProcessInputCompartmentFigureEditPart.VISUAL_ID:
		case modelBP.diagram.edit.parts.ProcessProcessOutputCompartmentFigureEditPart.VISUAL_ID:
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure2EditPart.VISUAL_ID:
		case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure3EditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID:
			return false;
		case modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID:
		case modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID:
		case modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID:
		case modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID:
		case modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return modelBP.diagram.part.BPmodelVisualIDRegistry
					.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return modelBP.diagram.part.BPmodelVisualIDRegistry
					.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return modelBP.diagram.part.BPmodelVisualIDRegistry
					.getNodeVisualID(containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return modelBP.diagram.part.BPmodelVisualIDRegistry
					.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return modelBP.diagram.part.BPmodelVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return modelBP.diagram.part.BPmodelVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
