package modelBP.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

/**
 * @generated
 */
public class BPmodelNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 7007;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof modelBP.diagram.navigator.BPmodelNavigatorItem) {
			modelBP.diagram.navigator.BPmodelNavigatorItem item = (modelBP.diagram.navigator.BPmodelNavigatorItem) element;
			return modelBP.diagram.part.BPmodelVisualIDRegistry
					.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
