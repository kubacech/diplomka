package modelBP.diagram.navigator;

import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

/**
 * @generated
 */
public class BPmodelNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		modelBP.diagram.part.BPmodelDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		modelBP.diagram.part.BPmodelDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof modelBP.diagram.navigator.BPmodelNavigatorItem
				&& !isOwnView(((modelBP.diagram.navigator.BPmodelNavigatorItem) element)
						.getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof modelBP.diagram.navigator.BPmodelNavigatorGroup) {
			modelBP.diagram.navigator.BPmodelNavigatorGroup group = (modelBP.diagram.navigator.BPmodelNavigatorGroup) element;
			return modelBP.diagram.part.BPmodelDiagramEditorPlugin
					.getInstance().getBundledImage(group.getIcon());
		}

		if (element instanceof modelBP.diagram.navigator.BPmodelNavigatorItem) {
			modelBP.diagram.navigator.BPmodelNavigatorItem navigatorItem = (modelBP.diagram.navigator.BPmodelNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (modelBP.diagram.part.BPmodelVisualIDRegistry.getVisualID(view)) {
		case modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://modelBP?Process", modelBP.diagram.providers.BPmodelElementTypes.Process_1000); //$NON-NLS-1$
		case modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://modelBP?Place", modelBP.diagram.providers.BPmodelElementTypes.Place_2001); //$NON-NLS-1$
		case modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://modelBP?Process", modelBP.diagram.providers.BPmodelElementTypes.Process_2002); //$NON-NLS-1$
		case modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://modelBP?Transition", modelBP.diagram.providers.BPmodelElementTypes.Transition_2003); //$NON-NLS-1$
		case modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://modelBP?Passive", modelBP.diagram.providers.BPmodelElementTypes.Passive_3001); //$NON-NLS-1$
		case modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://modelBP?Active", modelBP.diagram.providers.BPmodelElementTypes.Active_3002); //$NON-NLS-1$
		case modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://modelBP?Place", modelBP.diagram.providers.BPmodelElementTypes.Place_3003); //$NON-NLS-1$
		case modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://modelBP?Transition", modelBP.diagram.providers.BPmodelElementTypes.Transition_3004); //$NON-NLS-1$
		case modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://modelBP?Place", modelBP.diagram.providers.BPmodelElementTypes.Place_3005); //$NON-NLS-1$
		case modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://modelBP?Transition", modelBP.diagram.providers.BPmodelElementTypes.Transition_3006); //$NON-NLS-1$
		case modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://modelBP?Edge", modelBP.diagram.providers.BPmodelElementTypes.Edge_4001); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = modelBP.diagram.part.BPmodelDiagramEditorPlugin
				.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null
				&& elementType != null
				&& modelBP.diagram.providers.BPmodelElementTypes
						.isKnownElementType(elementType)) {
			image = modelBP.diagram.providers.BPmodelElementTypes
					.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof modelBP.diagram.navigator.BPmodelNavigatorGroup) {
			modelBP.diagram.navigator.BPmodelNavigatorGroup group = (modelBP.diagram.navigator.BPmodelNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof modelBP.diagram.navigator.BPmodelNavigatorItem) {
			modelBP.diagram.navigator.BPmodelNavigatorItem navigatorItem = (modelBP.diagram.navigator.BPmodelNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (modelBP.diagram.part.BPmodelVisualIDRegistry.getVisualID(view)) {
		case modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID:
			return getProcess_1000Text(view);
		case modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID:
			return getPlace_2001Text(view);
		case modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID:
			return getProcess_2002Text(view);
		case modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID:
			return getTransition_2003Text(view);
		case modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID:
			return getPassive_3001Text(view);
		case modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID:
			return getActive_3002Text(view);
		case modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID:
			return getPlace_3003Text(view);
		case modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID:
			return getTransition_3004Text(view);
		case modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID:
			return getPlace_3005Text(view);
		case modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID:
			return getTransition_3006Text(view);
		case modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID:
			return getEdge_4001Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getEdge_4001Text(View view) {
		IParser parser = modelBP.diagram.providers.BPmodelParserProvider
				.getParser(
						modelBP.diagram.providers.BPmodelElementTypes.Edge_4001,
						view.getElement() != null ? view.getElement() : view,
						modelBP.diagram.part.BPmodelVisualIDRegistry
								.getType(modelBP.diagram.edit.parts.EdgeNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getProcess_1000Text(View view) {
		modelBP.Process domainModelElement = (modelBP.Process) view
				.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin
					.getInstance()
					.logError(
							"No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPlace_3005Text(View view) {
		IParser parser = modelBP.diagram.providers.BPmodelParserProvider
				.getParser(
						modelBP.diagram.providers.BPmodelElementTypes.Place_3005,
						view.getElement() != null ? view.getElement() : view,
						modelBP.diagram.part.BPmodelVisualIDRegistry
								.getType(modelBP.diagram.edit.parts.PlaceName3EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 5006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getActive_3002Text(View view) {
		IParser parser = modelBP.diagram.providers.BPmodelParserProvider
				.getParser(
						modelBP.diagram.providers.BPmodelElementTypes.Active_3002,
						view.getElement() != null ? view.getElement() : view,
						modelBP.diagram.part.BPmodelVisualIDRegistry
								.getType(modelBP.diagram.edit.parts.ActiveCountEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTransition_2003Text(View view) {
		IParser parser = modelBP.diagram.providers.BPmodelParserProvider
				.getParser(
						modelBP.diagram.providers.BPmodelElementTypes.Transition_2003,
						view.getElement() != null ? view.getElement() : view,
						modelBP.diagram.part.BPmodelVisualIDRegistry
								.getType(modelBP.diagram.edit.parts.TransitionNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 5009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTransition_3006Text(View view) {
		IParser parser = modelBP.diagram.providers.BPmodelParserProvider
				.getParser(
						modelBP.diagram.providers.BPmodelElementTypes.Transition_3006,
						view.getElement() != null ? view.getElement() : view,
						modelBP.diagram.part.BPmodelVisualIDRegistry
								.getType(modelBP.diagram.edit.parts.TransitionName3EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 5007); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPlace_2001Text(View view) {
		IParser parser = modelBP.diagram.providers.BPmodelParserProvider
				.getParser(
						modelBP.diagram.providers.BPmodelElementTypes.Place_2001,
						view.getElement() != null ? view.getElement() : view,
						modelBP.diagram.part.BPmodelVisualIDRegistry
								.getType(modelBP.diagram.edit.parts.PlaceNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPassive_3001Text(View view) {
		IParser parser = modelBP.diagram.providers.BPmodelParserProvider
				.getParser(
						modelBP.diagram.providers.BPmodelElementTypes.Passive_3001,
						view.getElement() != null ? view.getElement() : view,
						modelBP.diagram.part.BPmodelVisualIDRegistry
								.getType(modelBP.diagram.edit.parts.PassiveCountEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getProcess_2002Text(View view) {
		IParser parser = modelBP.diagram.providers.BPmodelParserProvider
				.getParser(
						modelBP.diagram.providers.BPmodelElementTypes.Process_2002,
						view.getElement() != null ? view.getElement() : view,
						modelBP.diagram.part.BPmodelVisualIDRegistry
								.getType(modelBP.diagram.edit.parts.ProcessNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 5008); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPlace_3003Text(View view) {
		IParser parser = modelBP.diagram.providers.BPmodelParserProvider
				.getParser(
						modelBP.diagram.providers.BPmodelElementTypes.Place_3003,
						view.getElement() != null ? view.getElement() : view,
						modelBP.diagram.part.BPmodelVisualIDRegistry
								.getType(modelBP.diagram.edit.parts.PlaceName2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 5004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTransition_3004Text(View view) {
		IParser parser = modelBP.diagram.providers.BPmodelParserProvider
				.getParser(
						modelBP.diagram.providers.BPmodelElementTypes.Transition_3004,
						view.getElement() != null ? view.getElement() : view,
						modelBP.diagram.part.BPmodelVisualIDRegistry
								.getType(modelBP.diagram.edit.parts.TransitionName2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 5005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return modelBP.diagram.edit.parts.ProcessEditPart.MODEL_ID
				.equals(modelBP.diagram.part.BPmodelVisualIDRegistry
						.getModelID(view));
	}

}
