package modelBP.diagram.navigator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonContentProvider;

/**
 * @generated
 */
public class BPmodelNavigatorContentProvider implements ICommonContentProvider {

	/**
	 * @generated
	 */
	private static final Object[] EMPTY_ARRAY = new Object[0];

	/**
	 * @generated
	 */
	private Viewer myViewer;

	/**
	 * @generated
	 */
	private AdapterFactoryEditingDomain myEditingDomain;

	/**
	 * @generated
	 */
	private WorkspaceSynchronizer myWorkspaceSynchronizer;

	/**
	 * @generated
	 */
	private Runnable myViewerRefreshRunnable;

	/**
	 * @generated
	 */
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public BPmodelNavigatorContentProvider() {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		myEditingDomain = (AdapterFactoryEditingDomain) editingDomain;
		myEditingDomain.setResourceToReadOnlyMap(new HashMap() {
			public Object get(Object key) {
				if (!containsKey(key)) {
					put(key, Boolean.TRUE);
				}
				return super.get(key);
			}
		});
		myViewerRefreshRunnable = new Runnable() {
			public void run() {
				if (myViewer != null) {
					myViewer.refresh();
				}
			}
		};
		myWorkspaceSynchronizer = new WorkspaceSynchronizer(editingDomain,
				new WorkspaceSynchronizer.Delegate() {
					public void dispose() {
					}

					public boolean handleResourceChanged(final Resource resource) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}

					public boolean handleResourceDeleted(Resource resource) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}

					public boolean handleResourceMoved(Resource resource,
							final URI newURI) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}
				});
	}

	/**
	 * @generated
	 */
	public void dispose() {
		myWorkspaceSynchronizer.dispose();
		myWorkspaceSynchronizer = null;
		myViewerRefreshRunnable = null;
		myViewer = null;
		unloadAllResources();
		((TransactionalEditingDomain) myEditingDomain).dispose();
		myEditingDomain = null;
	}

	/**
	 * @generated
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		myViewer = viewer;
	}

	/**
	 * @generated
	 */
	void unloadAllResources() {
		for (Resource nextResource : myEditingDomain.getResourceSet()
				.getResources()) {
			nextResource.unload();
		}
	}

	/**
	 * @generated
	 */
	void asyncRefresh() {
		if (myViewer != null && !myViewer.getControl().isDisposed()) {
			myViewer.getControl().getDisplay()
					.asyncExec(myViewerRefreshRunnable);
		}
	}

	/**
	 * @generated
	 */
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof IFile) {
			IFile file = (IFile) parentElement;
			URI fileURI = URI.createPlatformResourceURI(file.getFullPath()
					.toString(), true);
			Resource resource = myEditingDomain.getResourceSet().getResource(
					fileURI, true);
			ArrayList<modelBP.diagram.navigator.BPmodelNavigatorItem> result = new ArrayList<modelBP.diagram.navigator.BPmodelNavigatorItem>();
			ArrayList<View> topViews = new ArrayList<View>(resource
					.getContents().size());
			for (EObject o : resource.getContents()) {
				if (o instanceof View) {
					topViews.add((View) o);
				}
			}
			result.addAll(createNavigatorItems(
					selectViewsByType(topViews,
							modelBP.diagram.edit.parts.ProcessEditPart.MODEL_ID),
					file, false));
			return result.toArray();
		}

		if (parentElement instanceof modelBP.diagram.navigator.BPmodelNavigatorGroup) {
			modelBP.diagram.navigator.BPmodelNavigatorGroup group = (modelBP.diagram.navigator.BPmodelNavigatorGroup) parentElement;
			return group.getChildren();
		}

		if (parentElement instanceof modelBP.diagram.navigator.BPmodelNavigatorItem) {
			modelBP.diagram.navigator.BPmodelNavigatorItem navigatorItem = (modelBP.diagram.navigator.BPmodelNavigatorItem) parentElement;
			if (navigatorItem.isLeaf() || !isOwnView(navigatorItem.getView())) {
				return EMPTY_ARRAY;
			}
			return getViewChildren(navigatorItem.getView(), parentElement);
		}

		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Object[] getViewChildren(View view, Object parentElement) {
		switch (modelBP.diagram.part.BPmodelVisualIDRegistry.getVisualID(view)) {

		case modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID: {
			LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem> result = new LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem>();
			Diagram sv = (Diagram) view;
			modelBP.diagram.navigator.BPmodelNavigatorGroup links = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Process_1000_links,
					"icons/linksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			if (!links.isEmpty()) {
				result.add(links);
			}
			return result.toArray();
		}

		case modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID: {
			LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem> result = new LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem>();
			Node sv = (Node) view;
			modelBP.diagram.navigator.BPmodelNavigatorGroup incominglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Place_2001_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			modelBP.diagram.navigator.BPmodelNavigatorGroup outgoinglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Place_2001_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID: {
			LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem> result = new LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem>();
			Node sv = (Node) view;
			modelBP.diagram.navigator.BPmodelNavigatorGroup incominglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Process_2002_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			modelBP.diagram.navigator.BPmodelNavigatorGroup outgoinglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Process_2002_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.ProcessProcessOutputCompartmentFigureEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.ProcessProcessInputCompartmentFigureEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.ProcessProcessInputCompartmentFigureEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.ProcessProcessOutputCompartmentFigureEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID: {
			LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem> result = new LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem>();
			Node sv = (Node) view;
			modelBP.diagram.navigator.BPmodelNavigatorGroup incominglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Transition_2003_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			modelBP.diagram.navigator.BPmodelNavigatorGroup outgoinglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Transition_2003_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID: {
			LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem> result = new LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem>();
			Node sv = (Node) view;
			modelBP.diagram.navigator.BPmodelNavigatorGroup incominglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Place_3003_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			modelBP.diagram.navigator.BPmodelNavigatorGroup outgoinglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Place_3003_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID: {
			LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem> result = new LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem>();
			Node sv = (Node) view;
			modelBP.diagram.navigator.BPmodelNavigatorGroup incominglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Transition_3004_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			modelBP.diagram.navigator.BPmodelNavigatorGroup outgoinglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Transition_3004_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID: {
			LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem> result = new LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem>();
			Node sv = (Node) view;
			modelBP.diagram.navigator.BPmodelNavigatorGroup incominglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Place_3005_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			modelBP.diagram.navigator.BPmodelNavigatorGroup outgoinglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Place_3005_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID: {
			LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem> result = new LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem>();
			Node sv = (Node) view;
			modelBP.diagram.navigator.BPmodelNavigatorGroup incominglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Transition_3006_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			modelBP.diagram.navigator.BPmodelNavigatorGroup outgoinglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Transition_3006_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID: {
			LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem> result = new LinkedList<modelBP.diagram.navigator.BPmodelAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			modelBP.diagram.navigator.BPmodelNavigatorGroup target = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Edge_4001_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			modelBP.diagram.navigator.BPmodelNavigatorGroup incominglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Edge_4001_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			modelBP.diagram.navigator.BPmodelNavigatorGroup source = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Edge_4001_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			modelBP.diagram.navigator.BPmodelNavigatorGroup outgoinglinks = new modelBP.diagram.navigator.BPmodelNavigatorGroup(
					modelBP.diagram.part.Messages.NavigatorGroupName_Edge_4001_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(
					Collections.singleton(sv),
					modelBP.diagram.part.BPmodelVisualIDRegistry
							.getType(modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}
		}
		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksSourceByType(Collection<Edge> edges,
			String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeSource = nextEdge.getSource();
			if (type.equals(nextEdgeSource.getType())
					&& isOwnView(nextEdgeSource)) {
				result.add(nextEdgeSource);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksTargetByType(Collection<Edge> edges,
			String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeTarget = nextEdge.getTarget();
			if (type.equals(nextEdgeTarget.getType())
					&& isOwnView(nextEdgeTarget)) {
				result.add(nextEdgeTarget);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private Collection<View> getOutgoingLinksByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getSourceEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private Collection<View> getIncomingLinksByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getTargetEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private Collection<View> getChildrenByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getChildren(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	private Collection<View> getDiagramLinksByType(
			Collection<Diagram> diagrams, String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (Diagram nextDiagram : diagrams) {
			result.addAll(selectViewsByType(nextDiagram.getEdges(), type));
		}
		return result;
	}

	// TODO refactor as static method
	/**
	 * @generated
	 */
	private Collection<View> selectViewsByType(Collection<View> views,
			String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (View nextView : views) {
			if (type.equals(nextView.getType()) && isOwnView(nextView)) {
				result.add(nextView);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return modelBP.diagram.edit.parts.ProcessEditPart.MODEL_ID
				.equals(modelBP.diagram.part.BPmodelVisualIDRegistry
						.getModelID(view));
	}

	/**
	 * @generated
	 */
	private Collection<modelBP.diagram.navigator.BPmodelNavigatorItem> createNavigatorItems(
			Collection<View> views, Object parent, boolean isLeafs) {
		ArrayList<modelBP.diagram.navigator.BPmodelNavigatorItem> result = new ArrayList<modelBP.diagram.navigator.BPmodelNavigatorItem>(
				views.size());
		for (View nextView : views) {
			result.add(new modelBP.diagram.navigator.BPmodelNavigatorItem(
					nextView, parent, isLeafs));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public Object getParent(Object element) {
		if (element instanceof modelBP.diagram.navigator.BPmodelAbstractNavigatorItem) {
			modelBP.diagram.navigator.BPmodelAbstractNavigatorItem abstractNavigatorItem = (modelBP.diagram.navigator.BPmodelAbstractNavigatorItem) element;
			return abstractNavigatorItem.getParent();
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean hasChildren(Object element) {
		return element instanceof IFile || getChildren(element).length > 0;
	}

}
