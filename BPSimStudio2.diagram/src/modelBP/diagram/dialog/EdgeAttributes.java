package modelBP.diagram.dialog;

import org.eclipse.emf.common.util.EList;

import modelBP.GroupPointer;
import modelBP.Scenario;
import modelBP.impl.EdgeImpl;
import modelBP.impl.PlaceImpl;

public class EdgeAttributes {

	EList<GroupPointer> multiplicity;
	EList<Scenario> scenario;

	public EdgeAttributes(EdgeImpl edge) {

		multiplicity = edge.getMultiplicity();
		scenario = edge.getScenarios();
	}

	public EdgeAttributes(EList<Scenario> _scenario,
			EList<GroupPointer> _multiplicity) {
		multiplicity = _multiplicity;
		scenario = _scenario;
	}

	public void setEdgeImpl(EdgeImpl edge, PlaceImpl place) {
		if (edge.getMultiplicity() == multiplicity
				&& edge.getScenarios() == scenario) {
			return;
		}
		// for (GroupPointer item : multiplicity) {
		// if (!place.getHasObject().contains(item)) {
		// place.getHasObject().add(item);
		// }
		// }
		edge.getMultiplicity().clear();
		edge.getMultiplicity().addAll(multiplicity);
		edge.getScenarios().clear();
		edge.getScenarios().addAll(scenario);
	}
}
