package modelBP.diagram.dialog;

import javax.swing.JDialog;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import modelBP.ModelBPFactory;
import modelBP.Scenario;

import org.eclipse.emf.common.util.EList;

import java.awt.event.ActionEvent;

public class TransitionDialog extends JDialog implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton yesButton = null;
	private JButton noButton = null;
	private JTextField nameTxt = null;
	private JTextArea descTxt = null;
	private JTextArea scriptTxt = null;
	private DefaultTableModel model = null;
	private JTextField scenTxt = null;
	private JTextField percentTxt = null;
	private JTextField minTxt = null;
	private JTextField maxTxt = null;
	private JTextField costTxt = null;
	private JButton addButton = null;
	private String result = "";
	private TransitionAttributes transition;

	public String getResult() {
		return result;
	}

	public TransitionDialog(JFrame frame, boolean modal,
			TransitionAttributes _transition) {
		super(frame, modal);

		transition = _transition;

		// hlavni panel
		JPanel myPanel = new JPanel();
		myPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		// name panel
		JPanel countPanel = new JPanel();
		countPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		countPanel.setPreferredSize(new Dimension(400, 50));
		myPanel.add(countPanel);

		JLabel countLabel = new JLabel("Název: ");
		countPanel.add(countLabel);

		nameTxt = new JTextField();
		nameTxt.setPreferredSize(new Dimension(130, 25));
		nameTxt.setText(transition.element.name);
		countPanel.add(nameTxt);

		// description panel
		JPanel descPanel = new JPanel();
		descPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		descPanel.setPreferredSize(new Dimension(400, 90));
		myPanel.add(descPanel);

		JLabel descLabel = new JLabel("Poznámky: ");
		descPanel.add(descLabel);

		descTxt = new JTextArea(4, 30);
		descTxt.setWrapStyleWord(true);
		descTxt.setLineWrap(true);
		descTxt.setText(transition.element.description);
		JScrollPane descScroll = new JScrollPane(descTxt,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		descPanel.add(descScroll);

		// script panel
		JPanel scriptPanel = new JPanel();
		scriptPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		scriptPanel.setPreferredSize(new Dimension(400, 205));
		myPanel.add(scriptPanel);

		JLabel scriptLabel = new JLabel("Scripty: ");
		scriptPanel.add(scriptLabel);

		scriptTxt = new JTextArea(11, 30);
		scriptTxt.setWrapStyleWord(true);
		scriptTxt.setLineWrap(true);
		scriptTxt.setText(transition.script);
		JScrollPane scriptScroll = new JScrollPane(scriptTxt,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scriptPanel.add(scriptScroll);

		// scenario panel
		JPanel scPanel = new JPanel();
		scPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		scPanel.setPreferredSize(new Dimension(460, 120));
		myPanel.add(scPanel);

		JLabel scenLabel = new JLabel("Scénáře: ");
		scPanel.add(scenLabel);

		model = new DefaultTableModel(getObjectMatrix(transition.scenarios),
				new Object[] { "Název", "%", "MIN", "MAX", "Cena" });
		JTable scTab = new JTable(model);
		JScrollPane scScroll = new JScrollPane(scTab);
		scPanel.add(scScroll);

		// add panel
		JPanel addPanel = new JPanel();
		addPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		addPanel.setPreferredSize(new Dimension(460, 70));
		myPanel.add(addPanel);

		scenTxt = new JTextField();
		scenTxt.setPreferredSize(new Dimension(85, 25));
		addPanel.add(scenTxt);

		percentTxt = new JTextField();
		percentTxt.setPreferredSize(new Dimension(85, 25));
		addPanel.add(percentTxt);

		minTxt = new JTextField();
		minTxt.setPreferredSize(new Dimension(85, 25));
		addPanel.add(minTxt);

		maxTxt = new JTextField();
		maxTxt.setPreferredSize(new Dimension(85, 25));
		addPanel.add(maxTxt);

		costTxt = new JTextField();
		costTxt.setPreferredSize(new Dimension(85, 25));
		addPanel.add(costTxt);

		addButton = new JButton("Add");
		addButton.setPreferredSize(new Dimension(450, 20));
		addButton.addActionListener(this);
		addPanel.add(addButton);

		// button panel
		yesButton = new JButton("OK");
		yesButton.addActionListener(this);
		myPanel.add(yesButton);
		noButton = new JButton("Cancel");
		noButton.addActionListener(this);
		myPanel.add(noButton);

		setPreferredSize(new Dimension(480, 635));
		getContentPane().add(myPanel);

		pack();
		setLocationRelativeTo(frame);
		setVisible(true);
	}

	private Object[][] getObjectMatrix(EList<Scenario> scenarios) {
		if (scenarios == null)
			return new Object[0][5];
		Object[][] result = new Object[scenarios.size()][5];
		for (int i = 0; i < scenarios.size(); i++) {
			result[i][0] = scenarios.get(i).getName();
			result[i][1] = scenarios.get(i).getPercent();
			result[i][2] = scenarios.get(i).getTimeMin();
			result[i][3] = scenarios.get(i).getTimeMax();
			result[i][4] = scenarios.get(i).getCost();
		}
		return result;
	}

	public void actionPerformed(ActionEvent e) {
		if (yesButton == e.getSource()) {
			result = yesButton.getText();
			setVisible(false);
		} else if (noButton == e.getSource()) {
			result = noButton.getText();
			setVisible(false);
		} else if (addButton == e.getSource()) {
			model.addRow(new Object[] { scenTxt.getText(),
					Integer.parseInt(percentTxt.getText()), minTxt.getText(),
					maxTxt.getText(), costTxt.getText() });

			scenTxt.setText("");
			percentTxt.setText("");
			minTxt.setText("");
			maxTxt.setText("");
			costTxt.setText("");
		}
	}

	public TransitionAttributes getTransition() {
		return new TransitionAttributes(new ElementAttributes(
				nameTxt.getText(), descTxt.getText()), scriptTxt.getText(),
				getScenarios(model));
	}

	private EList<Scenario> getScenarios(DefaultTableModel _model) {
		EList<Scenario> result = ModelBPFactory.eINSTANCE.createTransition()
				.getHasScenarios();
		for (int i = 0; i < _model.getRowCount(); i++) {

			Scenario scenario = ModelBPFactory.eINSTANCE.createScenario();
			scenario.setName(_model.getValueAt(i, 0).toString());
			
			if (_model.getValueAt(i, 1) != null) {
				try {
					scenario.setPercent(Float.parseFloat(_model.getValueAt(i, 1)
							.toString().trim().replaceAll(",", ".")));
				} catch (NumberFormatException e) {
					scenario.setPercent(0);
				}
			} else {
				scenario.setPercent(0);
			}
			
			if (_model.getValueAt(i, 2) != null) {
				try {
					scenario.setTimeMin(Integer.parseInt(_model.getValueAt(i, 2)
							.toString().trim()));
				} catch (NumberFormatException e) {
					scenario.setTimeMin(0);
				}
			} else {
				scenario.setTimeMin(0);
			}
			
			if (_model.getValueAt(i, 3) != null) {
				try {
					scenario.setTimeMax(Integer.parseInt(_model.getValueAt(i, 3)
							.toString().trim()));
				} catch (NumberFormatException e) {
					scenario.setTimeMax(0);
				}
			} else {
				scenario.setTimeMax(0);
			}

			if (_model.getValueAt(i, 4) != null) {
				try {
					scenario.setCost(Integer.parseInt(_model.getValueAt(i, 4)
							.toString().trim()));
				} catch (NumberFormatException e) {
					scenario.setCost(0);
				}
			} else {
				scenario.setCost(0);
			}
			result.add(scenario);
		}
		return result;
	}
}