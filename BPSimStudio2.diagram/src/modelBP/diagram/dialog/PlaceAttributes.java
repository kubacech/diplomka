package modelBP.diagram.dialog;

import org.eclipse.emf.common.util.EList;

import modelBP.GroupPointer;
import modelBP.impl.ElementImpl;
import modelBP.impl.PlaceImpl;

public class PlaceAttributes {
	
	ElementAttributes element;
	EList<GroupPointer> capacity;
	
	public PlaceAttributes(PlaceImpl place)
	{
		element = new ElementAttributes((ElementImpl)place);
		capacity = place.getCapacity();
	}
	
	public PlaceAttributes(ElementAttributes _element, EList<GroupPointer> _capacity)
	{
		element = _element;
		capacity = _capacity;
	}
	
	public void setPlaceImpl(PlaceImpl place)
	{
		if(new ElementAttributes((ElementImpl)place) == element &&
				place.getCapacity() == capacity)
		{
			return;
		}
		element.setElementImpl(place);
		place.getCapacity().clear();
		place.getCapacity().addAll(capacity);
	}
}
