package modelBP.diagram.dialog;

import javax.swing.JDialog;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import modelBP.GroupName;
import modelBP.GroupPointer;
import modelBP.ModelBPFactory;
import modelBP.Scenario;

import org.eclipse.emf.common.util.EList;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class EdgeDialog extends JDialog implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton yesButton = null;
	private JButton noButton = null;
	private DefaultTableModel mModel = null;
	private JButton mAddButton = null;
	private JTextField countTxt = null;
	private JComboBox<?> grCombo = null;
	private DefaultTableModel sModel = null;
	private JButton sAddButton = null;
	private String result = "";
	private EdgeAttributes edge;
	private JComboBox<?> sCombo;

	public String getResult() {
		return result;
	}

	public EdgeDialog(JFrame frame, boolean modal, EdgeAttributes _edge,
			EList<GroupPointer> groups, EList<Scenario> scenarios) {
		super(frame, modal);

		edge = _edge;

		// hlavni panel
		JPanel myPanel = new JPanel();
		myPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		// multiplicity panel
		JPanel mPanel = new JPanel();
		mPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		mPanel.setPreferredSize(new Dimension(400, 120));
		myPanel.add(mPanel);

		JLabel mLabel = new JLabel("Násobnost: ");
		mPanel.add(mLabel);

		mModel = new DefaultTableModel(getMultObjectMatrix(edge.multiplicity),
				new Object[] { "Počet", "Skupina" });
		JTable mTab = new JTable(mModel);
		JScrollPane mScroll = new JScrollPane(mTab);
		mScroll.setPreferredSize(new Dimension(390, 120));
		mPanel.add(mScroll);

		// add multiplicity panel
		JPanel mAddPanel = new JPanel();
		mAddPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		mAddPanel.setPreferredSize(new Dimension(400, 50));
		myPanel.add(mAddPanel);

		countTxt = new JTextField();
		countTxt.setPreferredSize(new Dimension(50, 25));
		mAddPanel.add(countTxt);

		grCombo = new JComboBox<Object>(mConvertToCombo(groups));
		grCombo.setPreferredSize(new Dimension(120, 25));
		mAddPanel.add(grCombo);

		mAddButton = new JButton("Add");
		mAddButton.addActionListener(this);
		mAddPanel.add(mAddButton);

		// scenario panel
		JPanel scPanel = new JPanel();
		scPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		scPanel.setPreferredSize(new Dimension(400, 120));
		myPanel.add(scPanel);

		JLabel scenLabel = new JLabel("Scénáře: ");
		scPanel.add(scenLabel);

		sModel = new DefaultTableModel(getScenObjectMatrix(edge.scenario),
				new Object[] { "Název" });
		JTable sTab = new JTable(sModel);
		JScrollPane sScroll = new JScrollPane(sTab);
		sScroll.setPreferredSize(new Dimension(390, 120));
		scPanel.add(sScroll);

		// add scenario panel
		JPanel sAddPanel = new JPanel();
		sAddPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		sAddPanel.setPreferredSize(new Dimension(400, 70));
		myPanel.add(sAddPanel);

		sCombo = new JComboBox<Object>(sConvertToCombo(scenarios));
		sCombo.setPreferredSize(new Dimension(120, 25));
		sAddPanel.add(sCombo);

		sAddButton = new JButton("Add");
		sAddButton.addActionListener(this);
		sAddPanel.add(sAddButton);

		// button panel
		yesButton = new JButton("OK");
		yesButton.addActionListener(this);
		myPanel.add(yesButton);
		noButton = new JButton("Cancel");
		noButton.addActionListener(this);
		myPanel.add(noButton);

		setPreferredSize(new Dimension(400, 500));
		getContentPane().add(myPanel);

		pack();
		setLocationRelativeTo(frame);
		setVisible(true);
	}

	private Object[][] getScenObjectMatrix(EList<Scenario> scenario) {
		if (scenario == null)
			return new Object[0][1];

		Object[][] result = new Object[scenario.size()][1];
		for (int i = 0; i < scenario.size(); i++) {
			if (scenario.get(i) == null) {
				return new Object[0][1];
			}
			result[i][0] = new KeyValue<Scenario>(scenario.get(i)
					.getName(), scenario.get(i));
		}
		return result;
	}

	private Object[][] getMultObjectMatrix(EList<GroupPointer> multiplicity) {
		if (multiplicity == null)
			return new Object[0][2];
		Object[][] result = new Object[multiplicity.size()][2];
		for (int i = 0; i < multiplicity.size(); i++) {
			result[i][0] = multiplicity.get(i).getCount();
			result[i][1] = new KeyValue<GroupName>(multiplicity.get(i)
					.getNamed().getName(), multiplicity.get(i).getNamed());
		}
		return result;
	}

	public void actionPerformed(ActionEvent e) {
		if (yesButton == e.getSource()) {
			result = yesButton.getText();
			setVisible(false);
		} else if (noButton == e.getSource()) {
			result = noButton.getText();
			setVisible(false);
		} else if (mAddButton == e.getSource()) {
			mModel.addRow(new Object[] { Integer.parseInt(countTxt.getText()),
					grCombo.getSelectedItem() });
		} else if (sAddButton == e.getSource()) {
			sModel.addRow(new Object[] { sCombo.getSelectedItem() });
		}
	}

	public EdgeAttributes getEdge() {
		return new EdgeAttributes(getScenarios(sModel),
				getMultiplicities(mModel));
	}

	private EList<Scenario> getScenarios(DefaultTableModel _model) {
		EList<Scenario> result = ModelBPFactory.eINSTANCE.createEdge()
				.getScenarios();
		for (int i = 0; i < _model.getRowCount(); i++) {
			if (_model.getValueAt(i, 0) == null)
				continue;
			Scenario scen = ModelBPFactory.eINSTANCE
					.createScenario();
			// (Scenario)((KeyValue<Scenario>) _model
			// .getValueAt(i, 0)).value;
			Scenario scenarioOld = (Scenario) ((KeyValue<?>) _model.getValueAt(
					i, 0)).value;
//			scen.setScen(scenarioOld);

			result.add(scenarioOld);
		}
		return result;
	}

	private EList<GroupPointer> getMultiplicities(DefaultTableModel _model) {
		EList<GroupPointer> result = ModelBPFactory.eINSTANCE.createPlace()
				.getCapacity();// place.capacity;
		for (int i = 0; i < _model.getRowCount(); i++) {
			if (_model.getValueAt(i, 1) == null)
				continue;
			GroupPointer group = ModelBPFactory.eINSTANCE.createGroupPointer();
			group.setCount(Integer.parseInt(_model.getValueAt(i, 0).toString()));
			group.setNamed((GroupName) ((KeyValue<?>) _model.getValueAt(i, 1)).value);
			result.add(group);
		}
		return result;
	}

	@SuppressWarnings("rawtypes")
	private Object[] mConvertToCombo(EList<GroupPointer> groups) {
		ArrayList<KeyValue> result = new ArrayList<KeyValue>();
		for (GroupPointer item : groups) {
			result.add(new KeyValue<GroupName>(item.getNamed().getName(), item
					.getNamed()));
		}
		return result.toArray();
	}

	@SuppressWarnings("rawtypes")
	private Object[] sConvertToCombo(EList<Scenario> scenarios) {
		ArrayList<KeyValue> result = new ArrayList<KeyValue>();
		for (Scenario item : scenarios) {
			result.add(new KeyValue<Scenario>(item.getName(),
					item));
		}
		return result.toArray();
	}

	public class KeyValue<V> {
		String key;
		V value;

		public KeyValue(String key, V value) {
			this.key = key;
			this.value = value;
		}

		public V getValue() {
			return value;
		}

		public String getKey() {
			return key;
		}

		@Override
		public String toString() {
			return key;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof KeyValue) {
				KeyValue<V> kv = (KeyValue<V>) obj;
				return (kv.value.equals(this.value));
			}
			return false;
		}

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 97 * hash + (this.value != null ? this.value.hashCode() : 0);
			return hash;
		}
	}
}