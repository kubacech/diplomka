package modelBP.diagram.dialog;

import modelBP.impl.ElementImpl;

public class ElementAttributes {
	
	String name, description;
	
	public ElementAttributes(ElementImpl element)
	{
		name = element.getName();
		description = element.getDescription();
	}
	
	public ElementAttributes(String _name, String _description)
	{
		name = _name;
		description = _description;
	}
	
	public void setElementImpl(ElementImpl element)
	{
		if(element.getName() == name &&
				element.getDescription() == description)
		{
			return;
		}
		element.setName(name);
		element.setDescription(description);
	}
}
