package modelBP.diagram.dialog;

import javax.swing.JDialog;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import modelBP.GroupName;
import modelBP.GroupPointer;
import modelBP.ModelBPFactory;

import org.eclipse.emf.common.util.EList;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class PlacetDialog extends JDialog implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton yesButton = null;
	private JButton noButton = null;
	private JTextField nameTxt = null;
	private JTextArea descTxt = null;
	private DefaultTableModel model = null;
	private JButton addButton = null;
	private JTextField countTxt = null;
	private JComboBox<?> grCombo = null;
	private String result = "";
	private PlaceAttributes place;

	public String getResult() {
		return result;
	}

	public PlacetDialog(JFrame frame, boolean modal, PlaceAttributes _place,
			EList<GroupName> groups) {
		super(frame, modal);

		place = _place;

		// hlavni panel
		JPanel myPanel = new JPanel();
		myPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		// name panel
		JPanel countPanel = new JPanel();
		countPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		countPanel.setPreferredSize(new Dimension(400, 50));
		myPanel.add(countPanel);

		JLabel countLabel = new JLabel("Název: ");
		countPanel.add(countLabel);

		nameTxt = new JTextField();
		nameTxt.setPreferredSize(new Dimension(130, 25));
		nameTxt.setText(place.element.name);
		countPanel.add(nameTxt);

		// description panel
		JPanel descPanel = new JPanel();
		descPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		descPanel.setPreferredSize(new Dimension(400, 120));
		myPanel.add(descPanel);

		JLabel descLabel = new JLabel("Poznámky: ");
		descPanel.add(descLabel);

		descTxt = new JTextArea(6, 20);
		descTxt.setWrapStyleWord(true);
		descTxt.setLineWrap(true);
		descTxt.setText(place.element.description);
		JScrollPane descScroll = new JScrollPane(descTxt,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		descPanel.add(descScroll);

		// capacity panel
		JPanel capPanel = new JPanel();
		capPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		capPanel.setPreferredSize(new Dimension(400, 120));
		myPanel.add(capPanel);

		model = new DefaultTableModel(getObjectMatrix(place.capacity),
				new Object[] { "Počet", "Skupina" });
		JTable capTab = new JTable(model);
		JScrollPane capScroll = new JScrollPane(capTab);
		capPanel.add(capScroll);

		// add panel
		JPanel addPanel = new JPanel();
		addPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		addPanel.setPreferredSize(new Dimension(400, 50));
		myPanel.add(addPanel);

		countTxt = new JTextField();
		countTxt.setPreferredSize(new Dimension(50, 25));
		addPanel.add(countTxt);

		grCombo = new JComboBox<Object>(convertToCombo(groups));
		addPanel.add(grCombo);

		addButton = new JButton("Add");
		addButton.addActionListener(this);
		addPanel.add(addButton);

		// button panel
		yesButton = new JButton("OK");
		yesButton.addActionListener(this);
		myPanel.add(yesButton);
		noButton = new JButton("Cancel");
		noButton.addActionListener(this);
		myPanel.add(noButton);

		setPreferredSize(new Dimension(400, 480));
		getContentPane().add(myPanel);

		pack();
		setLocationRelativeTo(frame);
		setVisible(true);
	}

	private Object[][] getObjectMatrix(EList<GroupPointer> capacity) {
		Object[][] result = new Object[capacity.size()][2];
		for (int i = 0; i < capacity.size(); i++) {
			result[i][0] = capacity.get(i).getCount();
			result[i][1] = new KeyValue(capacity.get(i).getNamed().getName(),
					capacity.get(i).getNamed());
		}
		return result;
	}

	public void actionPerformed(ActionEvent e) {
		if (yesButton == e.getSource()) {
			result = yesButton.getText();
			setVisible(false);
		} else if (noButton == e.getSource()) {
			result = noButton.getText();
			setVisible(false);
		} else if (addButton == e.getSource()) {
			model.addRow(new Object[] { Integer.parseInt(countTxt.getText()),
					grCombo.getSelectedItem() });
		}
	}

	public PlaceAttributes getPlace() {
		return new PlaceAttributes(new ElementAttributes(nameTxt.getText(),
				descTxt.getText()), getGroups(model));
	}

	private EList<GroupPointer> getGroups(DefaultTableModel _model) {
		EList<GroupPointer> result = ModelBPFactory.eINSTANCE.createPlace()
				.getCapacity();// place.capacity;
		for (int i = 0; i < _model.getRowCount(); i++) {
			GroupPointer group = ModelBPFactory.eINSTANCE.createGroupPointer();
			group.setCount(Integer.parseInt(_model.getValueAt(i, 0).toString()));
			group.setNamed((GroupName) ((KeyValue) _model.getValueAt(i, 1)).value);
			result.add(group);
		}
		return result;
	}

	private Object[] convertToCombo(EList<GroupName> groups) {
		ArrayList<KeyValue> result = new ArrayList<KeyValue>();
		for (GroupName item : groups) {
			result.add(new KeyValue(item.getName(), item));
		}
		return result.toArray();
	}

	public class KeyValue {
		String key;
		GroupName value;

		public KeyValue(String key, GroupName value) {
			this.key = key;
			this.value = value;
		}

		public GroupName getValue() {
			return value;
		}

		public String getKey() {
			return key;
		}

		@Override
		public String toString() {
			return key;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof KeyValue) {
				KeyValue kv = (KeyValue) obj;
				return (kv.value.equals(this.value));
			}
			return false;
		}

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 97 * hash + (this.value != null ? this.value.hashCode() : 0);
			return hash;
		}
	}
}