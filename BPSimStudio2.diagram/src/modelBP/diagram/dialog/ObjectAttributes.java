package modelBP.diagram.dialog;

import modelBP.GroupName;
import modelBP.impl.GroupImpl;

public class ObjectAttributes {
	
	int count;
	String description;
	GroupName group;
	
	public ObjectAttributes(GroupImpl active)
	{
		count = active.getCount();
		description = active.getDescription();
		group = active.getNamed();
	}
	
	public ObjectAttributes(int _count, String _description, GroupName _group)
	{
		count = _count;
		description = _description;
		group = _group;
	}
	
	public void setActiveImpl(GroupImpl element)
	{
		if(element.getCount() == count &&
				element.getDescription() == description &&
				element.getNamed() == group)
		{
			return;
		}
		element.setCount(count);
		element.setDescription(description);
		element.setNamed(group);
	}
}
