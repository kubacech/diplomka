package modelBP.diagram.dialog;

import javax.swing.JDialog; 

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;

public class ViewDialog extends JDialog implements ActionListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton yesButton = null;
    private JButton noButton = null;
    private JTextField nameTxt = null;
    private String result = ""; 
    public String getResult() { return result; }

    public ViewDialog(JFrame frame, boolean modal) {
        super(frame, modal);
        
        
        //hlavni panel
        JPanel myPanel = new JPanel();
        myPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        
        //name panel
        JPanel countPanel = new JPanel();
        countPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        countPanel.setPreferredSize(new Dimension(200, 50));
        myPanel.add(countPanel);
        
        JLabel countLabel = new JLabel("Zadejte název nové skupiny: ");
        countPanel.add(countLabel);
        
        nameTxt = new JTextField();
        nameTxt.setPreferredSize(new Dimension(130, 25));
        countPanel.add(nameTxt);
        
        
        //button panel
        yesButton = new JButton("OK");
        yesButton.addActionListener(this);
        myPanel.add(yesButton); 
        noButton = new JButton("Cancel");
        noButton.addActionListener(this);
        myPanel.add(noButton);
        
        setPreferredSize(new Dimension(200, 150));
        getContentPane().add(myPanel);
        
        pack();
        setLocationRelativeTo(frame);
        setVisible(true);
    }
    
	public void actionPerformed(ActionEvent e) {
        if(yesButton == e.getSource()) {
        	result = yesButton.getText();
            setVisible(false);
        }
        else if(noButton == e.getSource()) {
        	result = noButton.getText();
            setVisible(false);
        }
    }
    
    public String getName()
    {
    	return nameTxt.getText();
    }
}