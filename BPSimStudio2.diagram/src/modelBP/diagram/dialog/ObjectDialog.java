package modelBP.diagram.dialog;

import javax.swing.JDialog; 

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import modelBP.GroupName;
import modelBP.GroupPointer;
import modelBP.impl.PlaceImpl;

import org.eclipse.emf.common.util.EList;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class ObjectDialog extends JDialog implements ActionListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton yesButton = null;
    private JButton noButton = null;
    private JTextField countTxt = null;
    private JTextArea descTxt = null;
    private JComboBox<?> grCombo = null;
    private String result = ""; 
    public String getResult() { return result; }

    public ObjectDialog(JFrame frame, boolean modal, ObjectAttributes active, EList<GroupName> groups, PlaceImpl place) {
        super(frame, modal);
        
        //hlavni panel
        JPanel myPanel = new JPanel();
        myPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        
        //name panel
        JPanel countPanel = new JPanel();
        countPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        countPanel.setPreferredSize(new Dimension(400, 50));
        myPanel.add(countPanel);
        
        JLabel countLabel = new JLabel("Počet: ");
        countPanel.add(countLabel);
        
        countTxt = new JTextField();
        countTxt.setPreferredSize(new Dimension(130, 25));
        countTxt.setText(Integer.toString(active.count));
        countPanel.add(countTxt);
        
        //description panel
        JPanel descPanel = new JPanel();
        descPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        descPanel.setPreferredSize(new Dimension(400, 120));
        myPanel.add(descPanel);
        
        JLabel descLabel = new JLabel("Poznámky: ");
        descPanel.add(descLabel);
        
        descTxt = new JTextArea(6, 20);
        descTxt.setWrapStyleWord(true);
        descTxt.setLineWrap(true);
        descTxt.setText(active.description);
        JScrollPane descScroll = new JScrollPane(descTxt, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        descPanel.add(descScroll);
        
      //group panel
        JPanel grPanel = new JPanel();
        grPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        grPanel.setPreferredSize(new Dimension(400, 50));
        myPanel.add(grPanel);
        
        JLabel grLabel = new JLabel("Skupina: ");
        grPanel.add(grLabel);
        
        grCombo = new JComboBox<Object>(convertToCombo(groups, place));
        if(active.group != null)
        {
        	grCombo.setSelectedItem(new KeyValue(active.group.getName(), active.group));
        }
        grPanel.add(grCombo);
        
        yesButton = new JButton("OK");
        yesButton.addActionListener(this);
        myPanel.add(yesButton); 
        noButton = new JButton("Cancel");
        noButton.addActionListener(this);
        myPanel.add(noButton);
        
        setPreferredSize(new Dimension(400, 400));
        getContentPane().add(myPanel);
        
        pack();
        setLocationRelativeTo(frame);
        setVisible(true);
    }

	public void actionPerformed(ActionEvent e) {
        if(yesButton == e.getSource()) {
        	result = yesButton.getText();
            setVisible(false);
        }
        else if(noButton == e.getSource()) {
        	result = noButton.getText();
            setVisible(false);
        }
    }
    
    public ObjectAttributes getActive()
    {
    	return new ObjectAttributes(Integer.parseInt(countTxt.getText()), descTxt.getText(), ((KeyValue)grCombo.getSelectedItem()).getValue());
    }
    
    private Object[] convertToCombo(EList<GroupName> groups, PlaceImpl place) 
    {
    	ArrayList<KeyValue> result = new ArrayList<KeyValue>();
    	for(GroupName item : groups)
    	{
    		for(GroupPointer group : place.getCapacity())
    		{
    			if(group.getNamed() == item)
    			{
    				result.add(new KeyValue(item.getName(), item));
    				continue;
    			}
    		}
    	}
		return result.toArray();
	}
    
    public class KeyValue {
    	String key;
    	GroupName value;
     
    	public KeyValue(String key, GroupName value) {
    		this.key = key;
    		this.value = value;
    	}
     
    	public GroupName getValue() { return value; }
    	public String getKey() { return key; }
     
    	@Override
    	public String toString() { return key; }
     
    	@Override
    	public boolean equals(Object obj) {
    		if (obj instanceof KeyValue) {
    			KeyValue kv = (KeyValue) obj;
    			return (kv.value.equals(this.value));
    		}
    		return false;
    	}
     
    	@Override
    	public int hashCode() {
    		int hash = 7;
    		hash = 97 * hash + (this.value != null ? this.value.hashCode() : 0);
    		return hash;
    	}
    }
}