package modelBP.diagram.dialog;

import java.util.List;

import org.eclipse.emf.common.util.EList;

import modelBP.ModelBPFactory;
import modelBP.Scenario;
import modelBP.Scenario;
import modelBP.impl.TransitionImpl;

public class TransitionAttributes {

	ElementAttributes element;
	String script;
	EList<Scenario> scenarios;

	public TransitionAttributes(TransitionImpl transition) {
		element = new ElementAttributes(transition);
		script = transition.getScript();
		scenarios = transition.getHasScenarios();
	}

	public TransitionAttributes(ElementAttributes _element, String _script,
			EList<Scenario> _scenarios) {
		element = _element;
		script = _script;
		scenarios = _scenarios;
	}

	public void setTransitionImpl(TransitionImpl transition) {

		// for (ScenarioPointer scenarioPointer : scenarios) {
		// Scenario scenario = ModelBPFactory.eINSTANCE.createScenario();
		// scenario = scenarioPointer.getScen();
		// scenare.add(scenario);
		// }
		if (new ElementAttributes(transition) == element
				&& transition.getScript() == script) {
			return;
		}

		List<Scenario> scenare = transition.getHasScenarios();
		for (int i = 0; i < scenare.size(); i++) {
			scenare.get(i).setName(scenarios.get(i).getName());
			scenare.get(i)
					.setPercent(
							(float) (Math
									.round(scenarios.get(i).getPercent() * 100) / 100.0));
			scenare.get(i).setTimeMin(scenarios.get(i).getTimeMin());
			scenare.get(i).setTimeMax(scenarios.get(i).getTimeMax());
			scenare.get(i).setCost(scenarios.get(i).getCost());
		}
		int i = scenare.size();
		while (i < scenarios.size()) {
			scenare.add(scenarios.get(i));
		}
		float celkem = 0;
		for (Scenario scen : scenare) {
			celkem += scen.getPercent();
		}
		if (celkem != 100) {
			float konst = (float) (100.0 / celkem);

			for (Scenario scen : scenare) {
				scen.setPercent((float) (Math.round(scen.getPercent() * konst
						* 100) / 100.0));

			}
		}

		element.setElementImpl(transition);
		transition.setScript(script);
		// transition.getHasScenarios().clear();
		// transition.getHasScenarios().addAll(scenarios);
		// transition.getScenarios().clear();
		// transition.getScenarios().addAll(scenarios);
	}
}
