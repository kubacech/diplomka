package modelBP.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

/**
 * @generated
 */
public class BPmodelElementTypes {

	/**
	 * @generated
	 */
	private BPmodelElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.getItemProvidersAdapterFactory());

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType Process_1000 = getElementType("BPSimStudio2.diagram.Process_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Place_2001 = getElementType("BPSimStudio2.diagram.Place_2001"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Process_2002 = getElementType("BPSimStudio2.diagram.Process_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Transition_2003 = getElementType("BPSimStudio2.diagram.Transition_2003"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Passive_3001 = getElementType("BPSimStudio2.diagram.Passive_3001"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Active_3002 = getElementType("BPSimStudio2.diagram.Active_3002"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Place_3003 = getElementType("BPSimStudio2.diagram.Place_3003"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Transition_3004 = getElementType("BPSimStudio2.diagram.Transition_3004"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Place_3005 = getElementType("BPSimStudio2.diagram.Place_3005"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Transition_3006 = getElementType("BPSimStudio2.diagram.Transition_3006"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Edge_4001 = getElementType("BPSimStudio2.diagram.Edge_4001"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(Process_1000,
					modelBP.ModelBPPackage.eINSTANCE.getProcess());

			elements.put(Place_2001,
					modelBP.ModelBPPackage.eINSTANCE.getPlace());

			elements.put(Process_2002,
					modelBP.ModelBPPackage.eINSTANCE.getProcess());

			elements.put(Transition_2003,
					modelBP.ModelBPPackage.eINSTANCE.getTransition());

			elements.put(Passive_3001,
					modelBP.ModelBPPackage.eINSTANCE.getPassive());

			elements.put(Active_3002,
					modelBP.ModelBPPackage.eINSTANCE.getActive());

			elements.put(Place_3003,
					modelBP.ModelBPPackage.eINSTANCE.getPlace());

			elements.put(Transition_3004,
					modelBP.ModelBPPackage.eINSTANCE.getTransition());

			elements.put(Place_3005,
					modelBP.ModelBPPackage.eINSTANCE.getPlace());

			elements.put(Transition_3006,
					modelBP.ModelBPPackage.eINSTANCE.getTransition());

			elements.put(Edge_4001, modelBP.ModelBPPackage.eINSTANCE.getEdge());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(Process_1000);
			KNOWN_ELEMENT_TYPES.add(Place_2001);
			KNOWN_ELEMENT_TYPES.add(Process_2002);
			KNOWN_ELEMENT_TYPES.add(Transition_2003);
			KNOWN_ELEMENT_TYPES.add(Passive_3001);
			KNOWN_ELEMENT_TYPES.add(Active_3002);
			KNOWN_ELEMENT_TYPES.add(Place_3003);
			KNOWN_ELEMENT_TYPES.add(Transition_3004);
			KNOWN_ELEMENT_TYPES.add(Place_3005);
			KNOWN_ELEMENT_TYPES.add(Transition_3006);
			KNOWN_ELEMENT_TYPES.add(Edge_4001);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID:
			return Process_1000;
		case modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID:
			return Place_2001;
		case modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID:
			return Process_2002;
		case modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID:
			return Transition_2003;
		case modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID:
			return Passive_3001;
		case modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID:
			return Active_3002;
		case modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID:
			return Place_3003;
		case modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID:
			return Transition_3004;
		case modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID:
			return Place_3005;
		case modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID:
			return Transition_3006;
		case modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID:
			return Edge_4001;
		}
		return null;
	}

	/**
	 * @generated
	 */
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(
			elementTypeImages) {

		/**
		 * @generated
		 */
		@Override
		public boolean isKnownElementType(IElementType elementType) {
			return modelBP.diagram.providers.BPmodelElementTypes
					.isKnownElementType(elementType);
		}

		/**
		 * @generated
		 */
		@Override
		public IElementType getElementTypeForVisualId(int visualID) {
			return modelBP.diagram.providers.BPmodelElementTypes
					.getElementType(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public ENamedElement getDefiningNamedElement(
				IAdaptable elementTypeAdapter) {
			return modelBP.diagram.providers.BPmodelElementTypes
					.getElement(elementTypeAdapter);
		}
	};

}
