package modelBP.diagram.providers;

import org.eclipse.gmf.runtime.common.ui.services.icon.IIconProvider;
import org.eclipse.gmf.tooling.runtime.providers.DefaultElementTypeIconProvider;

/**
 * @generated
 */
public class BPmodelIconProvider extends DefaultElementTypeIconProvider
		implements IIconProvider {

	/**
	 * @generated
	 */
	public BPmodelIconProvider() {
		super(modelBP.diagram.providers.BPmodelElementTypes.TYPED_INSTANCE);
	}
}
