package modelBP.diagram.providers;

/**
 * @generated
 */
public class ElementInitializers {

	protected ElementInitializers() {
		// use #getInstance to access cached instance
	}

	/**
	 * @generated
	 */
	public static ElementInitializers getInstance() {
		ElementInitializers cached = modelBP.diagram.part.BPmodelDiagramEditorPlugin
				.getInstance().getElementInitializers();
		if (cached == null) {
			modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
					.setElementInitializers(cached = new ElementInitializers());
		}
		return cached;
	}
}
