package modelBP.diagram.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.modelingassistant.ModelingAssistantProvider;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

/**
 * @generated
 */
public class BPmodelModelingAssistantProvider extends ModelingAssistantProvider {

	/**
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	public List getTypesForPopupBar(IAdaptable host) {
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart instanceof modelBP.diagram.edit.parts.ProcessEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(3);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_2001);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Process_2002);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_2003);
			return types;
		}
		if (editPart instanceof modelBP.diagram.edit.parts.PlaceEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Passive_3001);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Active_3002);
			return types;
		}
		if (editPart instanceof modelBP.diagram.edit.parts.Process2EditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(4);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3004);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3005);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3003);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3006);
			return types;
		}
		if (editPart instanceof modelBP.diagram.edit.parts.Place2EditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Passive_3001);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Active_3002);
			return types;
		}
		if (editPart instanceof modelBP.diagram.edit.parts.Place3EditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Passive_3001);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Active_3002);
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	public List getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.PlaceEditPart) {
			return ((modelBP.diagram.edit.parts.PlaceEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Process2EditPart) {
			return ((modelBP.diagram.edit.parts.Process2EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.TransitionEditPart) {
			return ((modelBP.diagram.edit.parts.TransitionEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Place2EditPart) {
			return ((modelBP.diagram.edit.parts.Place2EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Transition2EditPart) {
			return ((modelBP.diagram.edit.parts.Transition2EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Place3EditPart) {
			return ((modelBP.diagram.edit.parts.Place3EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Transition3EditPart) {
			return ((modelBP.diagram.edit.parts.Transition3EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings({ "rawtypes" })
	public List getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof modelBP.diagram.edit.parts.PlaceEditPart) {
			return ((modelBP.diagram.edit.parts.PlaceEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Process2EditPart) {
			return ((modelBP.diagram.edit.parts.Process2EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.TransitionEditPart) {
			return ((modelBP.diagram.edit.parts.TransitionEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Place2EditPart) {
			return ((modelBP.diagram.edit.parts.Place2EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Transition2EditPart) {
			return ((modelBP.diagram.edit.parts.Transition2EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Place3EditPart) {
			return ((modelBP.diagram.edit.parts.Place3EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Transition3EditPart) {
			return ((modelBP.diagram.edit.parts.Transition3EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	public List getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.PlaceEditPart) {
			return ((modelBP.diagram.edit.parts.PlaceEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Process2EditPart) {
			return ((modelBP.diagram.edit.parts.Process2EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.TransitionEditPart) {
			return ((modelBP.diagram.edit.parts.TransitionEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Place2EditPart) {
			return ((modelBP.diagram.edit.parts.Place2EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Transition2EditPart) {
			return ((modelBP.diagram.edit.parts.Transition2EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Place3EditPart) {
			return ((modelBP.diagram.edit.parts.Place3EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Transition3EditPart) {
			return ((modelBP.diagram.edit.parts.Transition3EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	public List getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof modelBP.diagram.edit.parts.PlaceEditPart) {
			return ((modelBP.diagram.edit.parts.PlaceEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Process2EditPart) {
			return ((modelBP.diagram.edit.parts.Process2EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.TransitionEditPart) {
			return ((modelBP.diagram.edit.parts.TransitionEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Place2EditPart) {
			return ((modelBP.diagram.edit.parts.Place2EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Transition2EditPart) {
			return ((modelBP.diagram.edit.parts.Transition2EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Place3EditPart) {
			return ((modelBP.diagram.edit.parts.Place3EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Transition3EditPart) {
			return ((modelBP.diagram.edit.parts.Transition3EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	public List getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.PlaceEditPart) {
			return ((modelBP.diagram.edit.parts.PlaceEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Process2EditPart) {
			return ((modelBP.diagram.edit.parts.Process2EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.TransitionEditPart) {
			return ((modelBP.diagram.edit.parts.TransitionEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Place2EditPart) {
			return ((modelBP.diagram.edit.parts.Place2EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Transition2EditPart) {
			return ((modelBP.diagram.edit.parts.Transition2EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Place3EditPart) {
			return ((modelBP.diagram.edit.parts.Place3EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof modelBP.diagram.edit.parts.Transition3EditPart) {
			return ((modelBP.diagram.edit.parts.Transition3EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForSource(IAdaptable target,
			IElementType relationshipType) {
		return selectExistingElement(target,
				getTypesForSource(target, relationshipType));
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForTarget(IAdaptable source,
			IElementType relationshipType) {
		return selectExistingElement(source,
				getTypesForTarget(source, relationshipType));
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	protected EObject selectExistingElement(IAdaptable host, Collection types) {
		if (types.isEmpty()) {
			return null;
		}
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart == null) {
			return null;
		}
		Diagram diagram = (Diagram) editPart.getRoot().getContents().getModel();
		HashSet<EObject> elements = new HashSet<EObject>();
		for (Iterator<EObject> it = diagram.getElement().eAllContents(); it
				.hasNext();) {
			EObject element = it.next();
			if (isApplicableElement(element, types)) {
				elements.add(element);
			}
		}
		if (elements.isEmpty()) {
			return null;
		}
		return selectElement((EObject[]) elements.toArray(new EObject[elements
				.size()]));
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	protected boolean isApplicableElement(EObject element, Collection types) {
		IElementType type = ElementTypeRegistry.getInstance().getElementType(
				element);
		return types.contains(type);
	}

	/**
	 * @generated
	 */
	protected EObject selectElement(EObject[] elements) {
		Shell shell = Display.getCurrent().getActiveShell();
		ILabelProvider labelProvider = new AdapterFactoryLabelProvider(
				modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
						.getItemProvidersAdapterFactory());
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(
				shell, labelProvider);
		dialog.setMessage(modelBP.diagram.part.Messages.BPmodelModelingAssistantProviderMessage);
		dialog.setTitle(modelBP.diagram.part.Messages.BPmodelModelingAssistantProviderTitle);
		dialog.setMultipleSelection(false);
		dialog.setElements(elements);
		EObject selected = null;
		if (dialog.open() == Window.OK) {
			selected = (EObject) dialog.getFirstResult();
		}
		return selected;
	}
}
