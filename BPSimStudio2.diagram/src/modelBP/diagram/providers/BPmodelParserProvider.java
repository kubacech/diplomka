package modelBP.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class BPmodelParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser placeName_5003Parser;

	/**
	 * @generated
	 */
	private IParser getPlaceName_5003Parser() {
		if (placeName_5003Parser == null) {
			EAttribute[] features = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			EAttribute[] editableFeatures = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			modelBP.diagram.parsers.MessageFormatParser parser = new modelBP.diagram.parsers.MessageFormatParser(
					features, editableFeatures);
			placeName_5003Parser = parser;
		}
		return placeName_5003Parser;
	}

	/**
	 * @generated
	 */
	private IParser processName_5008Parser;

	/**
	 * @generated
	 */
	private IParser getProcessName_5008Parser() {
		if (processName_5008Parser == null) {
			EAttribute[] features = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			modelBP.diagram.parsers.MessageFormatParser parser = new modelBP.diagram.parsers.MessageFormatParser(
					features);
			processName_5008Parser = parser;
		}
		return processName_5008Parser;
	}

	/**
	 * @generated
	 */
	private IParser transitionName_5007Parser;

	/**
	 * @generated
	 */
	private IParser getTransitionName_5007Parser() {
		if (transitionName_5007Parser == null) {
			EAttribute[] features = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			modelBP.diagram.parsers.MessageFormatParser parser = new modelBP.diagram.parsers.MessageFormatParser(
					features);
			transitionName_5007Parser = parser;
		}
		return transitionName_5007Parser;
	}

	/**
	 * @generated
	 */
	private IParser edgeName_6001Parser;

	/**
	 * @generated
	 */
	private IParser getEdgeName_6001Parser() {
		if (edgeName_6001Parser == null) {
			EAttribute[] features = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			modelBP.diagram.parsers.MessageFormatParser parser = new modelBP.diagram.parsers.MessageFormatParser(
					features);
			edgeName_6001Parser = parser;
		}
		return edgeName_6001Parser;
	}

	/**
	 * @generated
	 */
	private IParser placeName_5004Parser;

	/**
	 * @generated
	 */
	private IParser getPlaceName_5004Parser() {
		if (placeName_5004Parser == null) {
			EAttribute[] features = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			EAttribute[] editableFeatures = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			modelBP.diagram.parsers.MessageFormatParser parser = new modelBP.diagram.parsers.MessageFormatParser(
					features, editableFeatures);
			placeName_5004Parser = parser;
		}
		return placeName_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser transitionName_5005Parser;

	/**
	 * @generated
	 */
	private IParser getTransitionName_5005Parser() {
		if (transitionName_5005Parser == null) {
			EAttribute[] features = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			modelBP.diagram.parsers.MessageFormatParser parser = new modelBP.diagram.parsers.MessageFormatParser(
					features);
			transitionName_5005Parser = parser;
		}
		return transitionName_5005Parser;
	}

	/**
	 * @generated
	 */
	private IParser placeName_5006Parser;

	/**
	 * @generated
	 */
	private IParser getPlaceName_5006Parser() {
		if (placeName_5006Parser == null) {
			EAttribute[] features = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			EAttribute[] editableFeatures = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			modelBP.diagram.parsers.MessageFormatParser parser = new modelBP.diagram.parsers.MessageFormatParser(
					features, editableFeatures);
			placeName_5006Parser = parser;
		}
		return placeName_5006Parser;
	}

	/**
	 * @generated
	 */
	private IParser transitionName_5009Parser;

	/**
	 * @generated
	 */
	private IParser getTransitionName_5009Parser() {
		if (transitionName_5009Parser == null) {
			EAttribute[] features = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getElement_Name() };
			modelBP.diagram.parsers.MessageFormatParser parser = new modelBP.diagram.parsers.MessageFormatParser(
					features);
			transitionName_5009Parser = parser;
		}
		return transitionName_5009Parser;
	}

	/**
	 * @generated
	 */
	private IParser passiveCountOut_5001Parser;

	/**
	 * @generated
	 */
	private IParser getPassiveCountOut_5001Parser() {
		if (passiveCountOut_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getGroup_CountOut() };
			EAttribute[] editableFeatures = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getGroup_CountOut() };
			modelBP.diagram.parsers.MessageFormatParser parser = new modelBP.diagram.parsers.MessageFormatParser(
					features, editableFeatures);
			passiveCountOut_5001Parser = parser;
		}
		return passiveCountOut_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser activeCountOut_5002Parser;

	/**
	 * @generated
	 */
	private IParser getActiveCountOut_5002Parser() {
		if (activeCountOut_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getGroup_CountOut() };
			EAttribute[] editableFeatures = new EAttribute[] { modelBP.ModelBPPackage.eINSTANCE
					.getGroup_CountOut() };
			modelBP.diagram.parsers.MessageFormatParser parser = new modelBP.diagram.parsers.MessageFormatParser(
					features, editableFeatures);
			activeCountOut_5002Parser = parser;
		}
		return activeCountOut_5002Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case modelBP.diagram.edit.parts.PlaceNameEditPart.VISUAL_ID:
			return getPlaceName_5003Parser();
		case modelBP.diagram.edit.parts.ProcessNameEditPart.VISUAL_ID:
			return getProcessName_5008Parser();
		case modelBP.diagram.edit.parts.TransitionNameEditPart.VISUAL_ID:
			return getTransitionName_5009Parser();
		case modelBP.diagram.edit.parts.PassiveCountEditPart.VISUAL_ID:
			return getPassiveCountOut_5001Parser();
		case modelBP.diagram.edit.parts.ActiveCountEditPart.VISUAL_ID:
			return getActiveCountOut_5002Parser();
		case modelBP.diagram.edit.parts.PlaceName2EditPart.VISUAL_ID:
			return getPlaceName_5004Parser();
		case modelBP.diagram.edit.parts.TransitionName2EditPart.VISUAL_ID:
			return getTransitionName_5005Parser();
		case modelBP.diagram.edit.parts.PlaceName3EditPart.VISUAL_ID:
			return getPlaceName_5006Parser();
		case modelBP.diagram.edit.parts.TransitionName3EditPart.VISUAL_ID:
			return getTransitionName_5007Parser();
		case modelBP.diagram.edit.parts.EdgeNameEditPart.VISUAL_ID:
			return getEdgeName_6001Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(modelBP.diagram.part.BPmodelVisualIDRegistry
					.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(modelBP.diagram.part.BPmodelVisualIDRegistry
					.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (modelBP.diagram.providers.BPmodelElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		@SuppressWarnings("rawtypes")
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
