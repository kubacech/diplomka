package modelBP.diagram.providers;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import modelBP.Element;
import modelBP.Process;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.IClientSelector;
import org.eclipse.gmf.runtime.emf.core.util.EMFCoreUtil;
import org.eclipse.gmf.runtime.notation.View;

import bpsimstudio2.simulace.PesInstance;

/**
 * @generated
 */
public class BPmodelValidationProvider {

	/**
	 * @generated
	 */
	private static boolean constraintsActive = false;

	/**
	 * @generated
	 */
	public static boolean shouldConstraintsBePrivate() {
		return false;
	}

	/**
	 * @generated
	 */
	public static void runWithConstraints(
			TransactionalEditingDomain editingDomain, Runnable operation) {
		final Runnable op = operation;
		Runnable task = new Runnable() {
			public void run() {
				try {
					constraintsActive = true;
					op.run();
				} finally {
					constraintsActive = false;
				}
			}
		};
		if (editingDomain != null) {
			try {
				editingDomain.runExclusive(task);
			} catch (Exception e) {
				modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
						.logError("Validation failed", e); //$NON-NLS-1$
			}
		} else {
			task.run();
		}
	}

	/**
	 * @generated
	 */
	static boolean isInDefaultEditorContext(Object object) {
		if (shouldConstraintsBePrivate() && !constraintsActive) {
			return false;
		}
		if (object instanceof View) {
			return constraintsActive
					&& modelBP.diagram.edit.parts.ProcessEditPart.MODEL_ID
							.equals(modelBP.diagram.part.BPmodelVisualIDRegistry
									.getModelID((View) object));
		}
		return true;
	}

	/**
	 * @generated
	 */
	public static class DefaultCtx implements IClientSelector {

		/**
		 * @generated
		 */
		public boolean selects(Object object) {
			return isInDefaultEditorContext(object);
		}
	}

	/**
	 * @generated
	 */
	public static class Adapter1 extends AbstractModelConstraint {

		/**
		 * @generated NOT
		 */
		public IStatus validate(IValidationContext ctx) {
			modelBP.Edge edge = (modelBP.Edge) ctx.getTarget();
			return (edge.getSource().getClass() == edge.getTarget().getClass()
					|| edge.getSource() instanceof Process || edge.getTarget() instanceof Process) ? ctx
					.createFailureStatus("Nedovolená cyklická závislost") : ctx
					.createSuccessStatus();
			// return ctx.createFailureStatus(null);
		}
	}

	/**
	 * @generated
	 */
	public static class Adapter2 extends AbstractModelConstraint {

		/**
		 * @generated NOT
		 */
		public IStatus validate(IValidationContext ctx) {
			modelBP.Place context = (modelBP.Place) ctx.getTarget();
			return (context.getName() == null || context.getName().isEmpty()) ? ctx
					.createFailureStatus("Místo musí mít přiřazené jméno")
					: ctx.createSuccessStatus();
		}
	}

	/**
	 * @generated
	 */
	public static class Adapter3 extends AbstractModelConstraint {

		/**
		 * @generated NOT
		 */
		public IStatus validate(IValidationContext ctx) {
			modelBP.Transition context = (modelBP.Transition) ctx.getTarget();
			return (context.getName() == null || context.getName().isEmpty()) ? ctx
					.createFailureStatus("Přechod musí mít přiřazené jméno")
					: ctx.createSuccessStatus();
		}
	}

	/**
	 * @generated
	 */
	public static class Adapter4 extends AbstractModelConstraint {

		/**
		 * @generated NOT
		 */
		public IStatus validate(IValidationContext ctx) {
			modelBP.Place context = (modelBP.Place) ctx.getTarget();
			modelBP.Process proc = context.getAssigned();

			ArrayList<String> jmena = new ArrayList<String>();
			for (Element ele : proc.getHasElement()) {
				if (!ele.equals(context))
					jmena.add(ele.getName());
			}

			for (String string : jmena) {
				if (context.getName() != null && string != null) {
					if (string.equals(context.getName())) {
						JOptionPane.showMessageDialog(null,
								"Názvy musejí být unikátní.\nZvolte jiný.", "",
								JOptionPane.WARNING_MESSAGE);
						return ctx
								.createFailureStatus("Jména musí být unikátní.");
					}
				}
			}

			return ctx.createSuccessStatus();
		}
	}

	/**
	 * @generated
	 */
	static String formatElement(EObject object) {
		return EMFCoreUtil.getQualifiedName(object, true);
	}

}
