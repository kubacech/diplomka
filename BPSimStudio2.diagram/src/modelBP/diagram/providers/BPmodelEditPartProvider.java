package modelBP.diagram.providers;

import org.eclipse.gmf.tooling.runtime.providers.DefaultEditPartProvider;

/**
 * @generated
 */
public class BPmodelEditPartProvider extends DefaultEditPartProvider {

	/**
	 * @generated
	 */
	public BPmodelEditPartProvider() {
		super(new modelBP.diagram.edit.parts.BPmodelEditPartFactory(),
				modelBP.diagram.part.BPmodelVisualIDRegistry.TYPED_INSTANCE,
				modelBP.diagram.edit.parts.ProcessEditPart.MODEL_ID);
	}
}
