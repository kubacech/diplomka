package modelBP.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

	/**
	 * @generated
	 */
	public DiagramRulersAndGridPreferencePage() {
		setPreferenceStore(modelBP.diagram.part.BPmodelDiagramEditorPlugin
				.getInstance().getPreferenceStore());
	}
}
