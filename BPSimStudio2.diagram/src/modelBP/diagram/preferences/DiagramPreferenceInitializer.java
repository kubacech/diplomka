package modelBP.diagram.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * @generated
 */
public class DiagramPreferenceInitializer extends AbstractPreferenceInitializer {

	/**
	 * @generated
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = getPreferenceStore();
		modelBP.diagram.preferences.DiagramGeneralPreferencePage
				.initDefaults(store);
		modelBP.diagram.preferences.DiagramAppearancePreferencePage
				.initDefaults(store);
		modelBP.diagram.preferences.DiagramConnectionsPreferencePage
				.initDefaults(store);
		modelBP.diagram.preferences.DiagramPrintingPreferencePage
				.initDefaults(store);
		modelBP.diagram.preferences.DiagramRulersAndGridPreferencePage
				.initDefaults(store);

	}

	/**
	 * @generated
	 */
	protected IPreferenceStore getPreferenceStore() {
		return modelBP.diagram.part.BPmodelDiagramEditorPlugin.getInstance()
				.getPreferenceStore();
	}
}
