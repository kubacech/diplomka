package modelBP.diagram.edit.parts;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;

import modelBP.diagram.dialog.TransitionAttributes;
import modelBP.diagram.dialog.TransitionDialog;
import modelBP.impl.TransitionImpl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.FlowLayoutEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.OpenEditPolicy;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.Style;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.FontStyleImpl;
import org.eclipse.gmf.runtime.notation.impl.NodeImpl;
import org.eclipse.swt.graphics.Color;

/**
 * @generated
 */
public class TransitionEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2003;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public TransitionEditPart(View view) {
		super(view);
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new modelBP.diagram.edit.policies.TransitionItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);

		installEditPolicy(EditPolicyRoles.OPEN_ROLE, createOpenEditPolicy());
	}

	/**
	 * @generated NOT
	 */
	private OpenEditPolicy createOpenEditPolicy() {
		OpenEditPolicy policy = new OpenEditPolicy() {
			protected Command getOpenCommand(Request request) {

				EditPart targetEditPart = getTargetEditPart(request);
				if (false == targetEditPart.getModel() instanceof View) {
					return null;
				}
				View view = (View) targetEditPart.getModel();
				Style link = view.getStyle(NotationPackage.eINSTANCE
						.getFontStyle());
				if (false == link instanceof FontStyleImpl) {
					return null;
				}

				return new ICommandProxy(new UpdatePropertyCommand(
						(FontStyleImpl) link));
			}
		};
		return policy;
	}

	/**
	 * @generated NOT
	 */
	private static class UpdatePropertyCommand extends
			AbstractTransactionalCommand {

		/**
		 * @generated NOT
		 */
		private final FontStyleImpl diagramFacet;

		/**
		 * @generated NOT
		 */
		UpdatePropertyCommand(FontStyleImpl linkStyle) {
			// editing domain is taken for original diagram, 
			// if we open diagram from another file, we should use another editing domain
			super(TransactionUtil.getEditingDomain(linkStyle),
					modelBP.diagram.part.Messages.CommandName_OpenDiagram, null);
			diagramFacet = linkStyle;
		}

		// FIXME canExecute if  !(readOnly && getDiagramToOpen == null), i.e. open works on ro diagrams only when there's associated diagram already

		/**
		 * @generated NOT
		 */
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
				IAdaptable info) throws ExecutionException {
			try {
				TransitionImpl element = getElement();
				TransitionAttributes transition = new TransitionAttributes(
						element);

				JFrame mainFrame = new JFrame("Title");
				mainFrame.setResizable(false);
				TransitionDialog dialog = new TransitionDialog(mainFrame, true,
						transition);

				if (dialog.getResult() == "OK") {
					transition = dialog.getTransition();
					transition.setTransitionImpl(element);
				}

				return CommandResult.newOKCommandResult();
			} catch (Exception ex) {
				throw new ExecutionException("Can't open diagram", ex);
			}
		}

		/**
		 * @generated NOT
		 */
		private TransitionImpl getElement() {
			return (TransitionImpl) ((NodeImpl) diagramFacet.eContainer())
					.getElement();
		}
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {

		FlowLayoutEditPolicy lep = new FlowLayoutEditPolicy() {

			protected Command createAddCommand(EditPart child, EditPart after) {
				return null;
			}

			protected Command createMoveChildCommand(EditPart child,
					EditPart after) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		return primaryShape = new TransitionFigure();
	}

	/**
	 * @generated
	 */
	public TransitionFigure getPrimaryShape() {
		return (TransitionFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof modelBP.diagram.edit.parts.TransitionNameEditPart) {
			((modelBP.diagram.edit.parts.TransitionNameEditPart) childEditPart)
					.setLabel(getPrimaryShape().getFigureTransitionNameFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof modelBP.diagram.edit.parts.TransitionNameEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(60, 60);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(modelBP.diagram.part.BPmodelVisualIDRegistry
				.getType(modelBP.diagram.edit.parts.TransitionNameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSource() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(1);
		types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSourceAndTarget(
			IGraphicalEditPart targetEditPart) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof modelBP.diagram.edit.parts.PlaceEditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Process2EditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.TransitionEditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Place2EditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Transition2EditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Place3EditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Transition3EditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForTarget(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == modelBP.diagram.providers.BPmodelElementTypes.Edge_4001) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_2001);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Process_2002);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_2003);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3003);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3004);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3005);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3006);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnTarget() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(1);
		types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForSource(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == modelBP.diagram.providers.BPmodelElementTypes.Edge_4001) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_2001);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Process_2002);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_2003);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3003);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3004);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3005);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3006);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public class TransitionFigure extends RectangleFigure {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureTransitionNameFigure;

		/**
		 * @generated
		 */
		@SuppressWarnings("deprecation")
		public TransitionFigure() {

			FlowLayout layoutThis = new FlowLayout();
			layoutThis.setStretchMinorAxis(false);
			layoutThis.setMinorAlignment(FlowLayout.ALIGN_LEFTTOP);

			layoutThis.setMajorAlignment(FlowLayout.ALIGN_LEFTTOP);
			layoutThis.setMajorSpacing(5);
			layoutThis.setMinorSpacing(5);
			layoutThis.setHorizontal(true);

			this.setLayoutManager(layoutThis);

			this.setLineWidth(2);
			this.setForegroundColor(THIS_FORE);
			this.setBackgroundColor(THIS_BACK);
			this.setPreferredSize(new Dimension(getMapMode().DPtoLP(60),
					getMapMode().DPtoLP(60)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureTransitionNameFigure = new WrappingLabel();

			fFigureTransitionNameFigure.setText("<...>");

			this.add(fFigureTransitionNameFigure);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureTransitionNameFigure() {
			return fFigureTransitionNameFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 30, 144, 255);

	/**
	 * @generated
	 */
	static final Color THIS_BACK = new Color(null, 176, 224, 230);

}
