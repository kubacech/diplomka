package modelBP.diagram.edit.parts;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;

import modelBP.GroupName;
import modelBP.diagram.dialog.PlaceAttributes;
import modelBP.diagram.dialog.PlacetDialog;
import modelBP.impl.PlaceImpl;
import modelBP.impl.ProcessImpl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.OpenEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.Style;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.FontStyleImpl;
import org.eclipse.gmf.runtime.notation.impl.NodeImpl;
import org.eclipse.gmf.tooling.runtime.edit.policies.reparent.CreationEditPolicyWithCustomReparent;
import org.eclipse.swt.graphics.Color;

/**
 * @generated
 */
public class PlaceEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2001;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public PlaceEditPart(View view) {
		super(view);
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy());
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new modelBP.diagram.edit.policies.PlaceItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);

		installEditPolicy(EditPolicyRoles.OPEN_ROLE, createOpenEditPolicy());
	}

	/**
	 * @generated NOT
	 */
	private OpenEditPolicy createOpenEditPolicy() {
		OpenEditPolicy policy = new OpenEditPolicy() {
			protected Command getOpenCommand(Request request) {

				EditPart targetEditPart = getTargetEditPart(request);
				if (false == targetEditPart.getModel() instanceof View) {
					return null;
				}
				View view = (View) targetEditPart.getModel();
				Style link = view.getStyle(NotationPackage.eINSTANCE
						.getFontStyle());
				if (false == link instanceof FontStyleImpl) {
					return null;
				}

				return new ICommandProxy(new UpdatePropertyCommand(
						(FontStyleImpl) link));
			}
		};
		return policy;
	}

	/**
	 * @generated NOT
	 */
	private static class UpdatePropertyCommand extends
			AbstractTransactionalCommand {

		/**
		 * @generated NOT
		 */
		private final FontStyleImpl diagramFacet;

		/**
		 * @generated NOT
		 */
		UpdatePropertyCommand(FontStyleImpl linkStyle) {
			// editing domain is taken for original diagram, 
			// if we open diagram from another file, we should use another editing domain
			super(TransactionUtil.getEditingDomain(linkStyle),
					modelBP.diagram.part.Messages.CommandName_OpenDiagram, null);
			diagramFacet = linkStyle;
		}

		// FIXME canExecute if  !(readOnly && getDiagramToOpen == null), i.e. open works on ro diagrams only when there's associated diagram already

		/**
		 * @generated NOT
		 */
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
				IAdaptable info) throws ExecutionException {
			try {
				PlaceImpl element = getElement();
				PlaceAttributes place = new PlaceAttributes(element);
				EList<GroupName> groups = getGroups(element);

				JFrame mainFrame = new JFrame("Title");
				mainFrame.setResizable(false);
				PlacetDialog dialog = new PlacetDialog(mainFrame, true, place,
						groups);

				if (dialog.getResult() == "OK") {
					place = dialog.getPlace();
					place.setPlaceImpl(element);
				}

				return CommandResult.newOKCommandResult();
			} catch (Exception ex) {
				throw new ExecutionException("Can't open diagram", ex);
			}
		}

		/**
		 * @generated NOT
		 */
		private PlaceImpl getElement() {
			return (PlaceImpl) ((NodeImpl) diagramFacet.eContainer())
					.getElement();
		}

		/**
		 * @generated NOT
		 */
		private EList<GroupName> getGroups(EObject element) {
			EObject object = element;
			while (object.eContainer() != null) {
				object = object.eContainer();
			}
			EList<GroupName> list = ((ProcessImpl) object).getHasGroupName();
			return list;
		}
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		return primaryShape = new PlaceFigure();
	}

	/**
	 * @generated
	 */
	public PlaceFigure getPrimaryShape() {
		return (PlaceFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof modelBP.diagram.edit.parts.PlaceNameEditPart) {
			((modelBP.diagram.edit.parts.PlaceNameEditPart) childEditPart)
					.setLabel(getPrimaryShape().getFigurePlaceNameFigure());
			return true;
		}
		if (childEditPart instanceof modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart) {
			IFigure pane = getPrimaryShape().getFigurePlaceCompartmentFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart) childEditPart)
					.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof modelBP.diagram.edit.parts.PlaceNameEditPart) {
			return true;
		}
		if (childEditPart instanceof modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart) {
			IFigure pane = getPrimaryShape().getFigurePlaceCompartmentFigure();
			pane.remove(((modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart) childEditPart)
					.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		if (editPart instanceof modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart) {
			return getPrimaryShape().getFigurePlaceCompartmentFigure();
		}
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(80, 60);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(modelBP.diagram.part.BPmodelVisualIDRegistry
				.getType(modelBP.diagram.edit.parts.PlaceNameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSource() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(1);
		types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSourceAndTarget(
			IGraphicalEditPart targetEditPart) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof modelBP.diagram.edit.parts.PlaceEditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Process2EditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.TransitionEditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Place2EditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Transition2EditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Place3EditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		if (targetEditPart instanceof modelBP.diagram.edit.parts.Transition3EditPart) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForTarget(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == modelBP.diagram.providers.BPmodelElementTypes.Edge_4001) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_2001);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Process_2002);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_2003);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3003);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3004);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3005);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3006);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnTarget() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(1);
		types.add(modelBP.diagram.providers.BPmodelElementTypes.Edge_4001);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForSource(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == modelBP.diagram.providers.BPmodelElementTypes.Edge_4001) {
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_2001);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Process_2002);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_2003);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3003);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3004);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Place_3005);
			types.add(modelBP.diagram.providers.BPmodelElementTypes.Transition_3006);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public EditPart getTargetEditPart(Request request) {
		if (request instanceof CreateViewAndElementRequest) {
			CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request)
					.getViewAndElementDescriptor()
					.getCreateElementRequestAdapter();
			IElementType type = (IElementType) adapter
					.getAdapter(IElementType.class);
			if (type == modelBP.diagram.providers.BPmodelElementTypes.Passive_3001) {
				return getChildBySemanticHint(modelBP.diagram.part.BPmodelVisualIDRegistry
						.getType(modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart.VISUAL_ID));
			}
			if (type == modelBP.diagram.providers.BPmodelElementTypes.Active_3002) {
				return getChildBySemanticHint(modelBP.diagram.part.BPmodelVisualIDRegistry
						.getType(modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart.VISUAL_ID));
			}
		}
		return super.getTargetEditPart(request);
	}

	/**
	 * @generated
	 */
	public class PlaceFigure extends RoundedRectangle {

		/**
		 * @generated
		 */
		private WrappingLabel fFigurePlaceNameFigure;

		/**
		 * @generated
		 */
		private RoundedRectangle fFigurePlaceCompartmentFigure;

		/**
		 * @generated
		 */
		public PlaceFigure() {

			BorderLayout layoutThis = new BorderLayout();
			this.setLayoutManager(layoutThis);

			this.setCornerDimensions(new Dimension(getMapMode().DPtoLP(24),
					getMapMode().DPtoLP(24)));
			this.setLineWidth(2);
			this.setForegroundColor(THIS_FORE);
			this.setBackgroundColor(THIS_BACK);
			this.setPreferredSize(new Dimension(getMapMode().DPtoLP(80),
					getMapMode().DPtoLP(60)));

			this.setBorder(new MarginBorder(getMapMode().DPtoLP(5),
					getMapMode().DPtoLP(5), getMapMode().DPtoLP(5),
					getMapMode().DPtoLP(5)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigurePlaceCompartmentFigure = new RoundedRectangle();

			fFigurePlaceCompartmentFigure.setCornerDimensions(new Dimension(
					getMapMode().DPtoLP(24), getMapMode().DPtoLP(24)));
			fFigurePlaceCompartmentFigure.setFill(false);
			fFigurePlaceCompartmentFigure.setOutline(false);
			fFigurePlaceCompartmentFigure.setLineWidth(0);

			fFigurePlaceCompartmentFigure.setBorder(new MarginBorder(
					getMapMode().DPtoLP(0), getMapMode().DPtoLP(0),
					getMapMode().DPtoLP(0), getMapMode().DPtoLP(0)));

			this.add(fFigurePlaceCompartmentFigure, BorderLayout.CENTER);

			fFigurePlaceNameFigure = new WrappingLabel();

			fFigurePlaceNameFigure.setText("<...>");

			fFigurePlaceNameFigure.setBorder(new MarginBorder(getMapMode()
					.DPtoLP(0), getMapMode().DPtoLP(0), getMapMode().DPtoLP(0),
					getMapMode().DPtoLP(0)));

			fFigurePlaceCompartmentFigure.add(fFigurePlaceNameFigure);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigurePlaceNameFigure() {
			return fFigurePlaceNameFigure;
		}

		/**
		 * @generated
		 */
		public RoundedRectangle getFigurePlaceCompartmentFigure() {
			return fFigurePlaceCompartmentFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 30, 144, 255);

	/**
	 * @generated
	 */
	static final Color THIS_BACK = new Color(null, 176, 224, 230);

}
