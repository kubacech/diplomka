package modelBP.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;

/**
 * @generated
 */
public class BPmodelEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (modelBP.diagram.part.BPmodelVisualIDRegistry
					.getVisualID(view)) {

			case modelBP.diagram.edit.parts.ProcessEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.ProcessEditPart(view);

			case modelBP.diagram.edit.parts.PlaceEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.PlaceEditPart(view);

			case modelBP.diagram.edit.parts.PlaceNameEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.PlaceNameEditPart(view);

			case modelBP.diagram.edit.parts.Process2EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.Process2EditPart(view);

			case modelBP.diagram.edit.parts.ProcessNameEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.ProcessNameEditPart(view);

			case modelBP.diagram.edit.parts.TransitionEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.TransitionEditPart(view);

			case modelBP.diagram.edit.parts.TransitionNameEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.TransitionNameEditPart(
						view);

			case modelBP.diagram.edit.parts.PassiveEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.PassiveEditPart(view);

			case modelBP.diagram.edit.parts.PassiveCountEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.PassiveCountEditPart(view);

			case modelBP.diagram.edit.parts.ActiveEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.ActiveEditPart(view);

			case modelBP.diagram.edit.parts.ActiveCountEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.ActiveCountEditPart(view);

			case modelBP.diagram.edit.parts.Place2EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.Place2EditPart(view);

			case modelBP.diagram.edit.parts.PlaceName2EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.PlaceName2EditPart(view);

			case modelBP.diagram.edit.parts.Transition2EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.Transition2EditPart(view);

			case modelBP.diagram.edit.parts.TransitionName2EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.TransitionName2EditPart(
						view);

			case modelBP.diagram.edit.parts.Place3EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.Place3EditPart(view);

			case modelBP.diagram.edit.parts.PlaceName3EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.PlaceName3EditPart(view);

			case modelBP.diagram.edit.parts.Transition3EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.Transition3EditPart(view);

			case modelBP.diagram.edit.parts.TransitionName3EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.TransitionName3EditPart(
						view);

			case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.PlacePlaceCompartmentFigureEditPart(
						view);

			case modelBP.diagram.edit.parts.ProcessProcessInputCompartmentFigureEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.ProcessProcessInputCompartmentFigureEditPart(
						view);

			case modelBP.diagram.edit.parts.ProcessProcessOutputCompartmentFigureEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.ProcessProcessOutputCompartmentFigureEditPart(
						view);

			case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure2EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure2EditPart(
						view);

			case modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure3EditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.PlacePlaceCompartmentFigure3EditPart(
						view);

			case modelBP.diagram.edit.parts.EdgeEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.EdgeEditPart(view);

			case modelBP.diagram.edit.parts.EdgeNameEditPart.VISUAL_ID:
				return new modelBP.diagram.edit.parts.EdgeNameEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}
}
