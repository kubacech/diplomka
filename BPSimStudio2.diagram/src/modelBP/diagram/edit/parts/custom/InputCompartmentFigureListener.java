package modelBP.diagram.edit.parts.custom;

import java.util.List;

import modelBP.diagram.edit.parts.Place3EditPart;
import modelBP.diagram.edit.parts.Transition2EditPart;
 


import org.eclipse.draw2d.FigureListener;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.editparts.AbstractEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ListCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
 
public class InputCompartmentFigureListener implements FigureListener {
 
	private ListCompartmentEditPart compartmentEditPart = null;
	@SuppressWarnings("unused")
	private RoundedRectangle roundedRectangle = null;
 
	public static final double MARGIN = 20; // The margin to apply before drawing our operator
	public static final double R = 50; // The base length
	public static final double REF_W = 2 * MARGIN + R * 6; // Reference width
	public static final double REF_H = 2 * MARGIN + R * 5; // Reference height
 
	public InputCompartmentFigureListener(ListCompartmentEditPart compartmentEditPart, RoundedRectangle roundedRectangle) {
		this.compartmentEditPart = compartmentEditPart;
		this.roundedRectangle = roundedRectangle;
	}
 
	@SuppressWarnings("unchecked")
	@Override
	public void figureMoved(IFigure f) {
		ResizableCompartmentFigure figure = (ResizableCompartmentFigure) f;
		if (figure.getSize().width != 0) {
			IFigure contentPane = figure.getContentPane();
			Insets is = figure.getInsets();
			// Determine the scale to apply
			double xScale = ((double) figure.getSize().width - is.left - is.right) / REF_W;
			double yScale = ((double) figure.getSize().height - is.top - is.bottom) / REF_H;
 
			// Set the constraints (bounds) for the rounded rectangle
			Rectangle constraint = new Rectangle(
					(int) ((MARGIN + R) * xScale),
					(int) ((MARGIN) * yScale), 
					(int) (R * 4 * xScale),
					(int) (R * 5 * yScale));
			//contentPane.setConstraint(roundedRectangle, constraint);
 
			// Set the constraints for the input and output nodes
			List<AbstractEditPart> childs = compartmentEditPart.getChildren();
			
			// Rozdeleni plochy
			int elementSize = 18;
			int heightCanvas = figure.getSize().height;
			int resolution = 0;
			int count = 0;
			int childCount = childs.size();
			
			if(childCount > 1)
			{
				resolution = (heightCanvas - elementSize) / (childCount - 1);
				if(resolution < 0) resolution = 0;
			}
			resolution = 20;
			
			for (AbstractEditPart child : childs) {
				if (child instanceof AbstractGraphicalEditPart) {
					AbstractGraphicalEditPart gEditPart = (AbstractGraphicalEditPart) child;
					
					if ((gEditPart instanceof Place3EditPart)||(gEditPart instanceof Transition2EditPart)) {
							int x = (int) (MARGIN * xScale);
							int y = 0;
							if(childs.get(childs.size() - 1).getSelected() == 0)
							{
								y = count * resolution + 1;
							}
							else
							{
								y = count * (elementSize + 2) + 1;
							}
							constraint = new Rectangle(x, y, elementSize, elementSize);
							contentPane.setConstraint(gEditPart.getFigure(), constraint);
							count++;
						}
					}
				}
			}
		}
	} 
