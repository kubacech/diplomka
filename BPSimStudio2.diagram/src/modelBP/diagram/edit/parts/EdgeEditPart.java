package modelBP.diagram.edit.parts;

import javax.swing.JFrame;

import modelBP.GroupPointer;
import modelBP.ModelBPFactory;
import modelBP.Scenario;
import modelBP.diagram.dialog.EdgeAttributes;
import modelBP.diagram.dialog.EdgeDialog;
import modelBP.diagram.dialog.EdgeDialog.KeyValue;
import modelBP.impl.EdgeImpl;
import modelBP.impl.PlaceImpl;
import modelBP.impl.TransitionImpl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.OpenEditPolicy;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.Style;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.FontStyleImpl;
import org.eclipse.swt.graphics.Color;

/**
 * @generated
 */
public class EdgeEditPart extends ConnectionNodeEditPart implements
		ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 4001;

	/**
	 * @generated
	 */
	public EdgeEditPart(View view) {
		super(view);
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new modelBP.diagram.edit.policies.EdgeItemSemanticEditPolicy());

		installEditPolicy(EditPolicyRoles.OPEN_ROLE, createOpenEditPolicy());
	}

	/**
	 * @generated NOT
	 */
	private OpenEditPolicy createOpenEditPolicy() {
		OpenEditPolicy policy = new OpenEditPolicy() {
			protected Command getOpenCommand(Request request) {

				EditPart targetEditPart = getTargetEditPart(request);
				if (false == targetEditPart.getModel() instanceof View) {
					return null;
				}
				View view = (View) targetEditPart.getModel();
				Style link = view.getStyle(NotationPackage.eINSTANCE
						.getFontStyle());
				if (false == link instanceof FontStyleImpl) {
					return null;
				}

				return new ICommandProxy(new UpdatePropertyCommand(
						(FontStyleImpl) link));
			}
		};
		return policy;
	}

	/**
	 * @generated NOT
	 */
	private static class UpdatePropertyCommand extends
			AbstractTransactionalCommand {

		/**
		 * @generated NOT
		 */
		private final FontStyleImpl diagramFacet;

		/**
		 * @generated NOT
		 */
		UpdatePropertyCommand(FontStyleImpl linkStyle) {
			// editing domain is taken for original diagram,
			// if we open diagram from another file, we should use another
			// editing domain
			super(TransactionUtil.getEditingDomain(linkStyle),
					modelBP.diagram.part.Messages.CommandName_OpenDiagram, null);
			diagramFacet = linkStyle;
		}

		// FIXME canExecute if !(readOnly && getDiagramToOpen == null), i.e.
		// open works on ro diagrams only when there's associated diagram
		// already

		/**
		 * @generated NOT
		 */
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
				IAdaptable info) throws ExecutionException {
			try {
				EdgeImpl element = getElement();
				EdgeAttributes edge = new EdgeAttributes(element);
				EList<GroupPointer> groups = getGroups(getPlace(element));
				EList<Scenario> scenarios = getScenarios(getTransition(element));

				JFrame mainFrame = new JFrame("Title");
				mainFrame.setResizable(false);
				EdgeDialog dialog = new EdgeDialog(mainFrame, true, edge,
						groups, scenarios);

				if (dialog.getResult() == "OK") {
					edge = dialog.getEdge();
					edge.setEdgeImpl(element, getPlace(element));
				}

				return CommandResult.newOKCommandResult();
			} catch (Exception ex) {
				throw new ExecutionException("Can't open diagram", ex);
			}
		}

		/**
		 * @generated NOT
		 */
		private EList<Scenario> getScenarios(TransitionImpl transition) {
			return transition.getHasScenarios();
		}

		/**
		 * @generated NOT
		 */
		private EList<GroupPointer> getGroups(PlaceImpl place) {
			/*
			 * if(place == null) { return
			 * ModelBPFactory.eINSTANCE.createProcess().getHasGroupName(); }
			 * EList<GroupName> list =
			 * ModelBPFactory.eINSTANCE.createProcess().getHasGroupName();
			 * for(Group item : place.getHasGroup()) {
			 * if(!list.contains(item.getNamed())) { list.add(item.getNamed());
			 * } } return list;
			 */
			return place.getCapacity();
		}

		/**
		 * @generated NOT
		 */
		private EdgeImpl getElement() {
			return (EdgeImpl) ((org.eclipse.gmf.runtime.notation.impl.EdgeImpl) diagramFacet
					.eContainer()).getElement();
		}

		/**
		 * @generated NOT
		 */
		private PlaceImpl getPlace(EdgeImpl edge) {
			PlaceImpl place = null;
			if (edge.getSource() instanceof PlaceImpl) {
				place = (PlaceImpl) edge.getSource();
			} else if (edge.getTarget() instanceof PlaceImpl) {
				place = (PlaceImpl) edge.getTarget();
			}
			return place;
		}

		/**
		 * @generated NOT
		 */
		private TransitionImpl getTransition(EdgeImpl edge) {
			TransitionImpl transition = null;
			if (edge.getSource() instanceof TransitionImpl) {
				transition = (TransitionImpl) edge.getSource();
			} else if (edge.getTarget() instanceof TransitionImpl) {
				transition = (TransitionImpl) edge.getTarget();
			}
			return transition;
		}

	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof modelBP.diagram.edit.parts.EdgeNameEditPart) {
			((modelBP.diagram.edit.parts.EdgeNameEditPart) childEditPart)
					.setLabel(getPrimaryShape().getFigureEdgeGroupFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, index);
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof modelBP.diagram.edit.parts.EdgeNameEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model so
	 * you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected Connection createConnectionFigure() {
		return new EdgeFigure();
	}

	/**
	 * @generated
	 */
	public EdgeFigure getPrimaryShape() {
		return (EdgeFigure) getFigure();
	}

	/**
	 * @generated
	 */
	public class EdgeFigure extends PolylineConnectionEx {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureEdgeGroupFigure;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureEdgeScenarioFigure;

		/**
		 * @generated
		 */
		public EdgeFigure() {
			this.setLineWidth(2);
			this.setForegroundColor(THIS_FORE);

			createContents();
			setTargetDecoration(createTargetDecoration());
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureEdgeScenarioFigure = new WrappingLabel();

			fFigureEdgeScenarioFigure.setText("");

			this.add(fFigureEdgeScenarioFigure);

			fFigureEdgeGroupFigure = new WrappingLabel();

			fFigureEdgeGroupFigure.setText("");

			this.add(fFigureEdgeGroupFigure);

		}

		/**
		 * @generated
		 */
		private RotatableDecoration createTargetDecoration() {
			PolygonDecoration df = new PolygonDecoration();
			df.setFill(true);
			df.setForegroundColor(DF_FORE);
			PointList pl = new PointList();
			pl.addPoint(getMapMode().DPtoLP(-2), getMapMode().DPtoLP(1));
			pl.addPoint(getMapMode().DPtoLP(0), getMapMode().DPtoLP(0));
			pl.addPoint(getMapMode().DPtoLP(-2), getMapMode().DPtoLP(-1));
			df.setTemplate(pl);
			df.setScale(getMapMode().DPtoLP(7), getMapMode().DPtoLP(3));
			return df;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureEdgeGroupFigure() {
			return fFigureEdgeGroupFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureEdgeScenarioFigure() {
			return fFigureEdgeScenarioFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 30, 144, 255);
	/**
	 * @generated
	 */
	static final Color DF_FORE = new Color(null, 30, 144, 255);

}
