package modelBP.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

/**
 * @generated
 */
public class EdgeReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public EdgeReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof modelBP.Edge) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof modelBP.Element && newEnd instanceof modelBP.Element)) {
			return false;
		}
		modelBP.Element target = getLink().getTarget();
		if (!(getLink().eContainer() instanceof modelBP.Process)) {
			return false;
		}
		modelBP.Process container = (modelBP.Process) getLink().eContainer();
		return modelBP.diagram.edit.policies.BPmodelBaseItemSemanticEditPolicy
				.getLinkConstraints().canExistEdge_4001(container, getLink(),
						getNewSource(), target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof modelBP.Element && newEnd instanceof modelBP.Element)) {
			return false;
		}
		modelBP.Element source = getLink().getSource();
		if (!(getLink().eContainer() instanceof modelBP.Process)) {
			return false;
		}
		modelBP.Process container = (modelBP.Process) getLink().eContainer();
		return modelBP.diagram.edit.policies.BPmodelBaseItemSemanticEditPolicy
				.getLinkConstraints().canExistEdge_4001(container, getLink(),
						source, getNewTarget());
	}

	/**
	 * @generated NOT
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			getLink().setSource(getNewSource());
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			getLink().setTarget(getNewTarget());
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setSource(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTarget(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected modelBP.Edge getLink() {
		return (modelBP.Edge) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected modelBP.Element getOldSource() {
		return (modelBP.Element) oldEnd;
	}

	/**
	 * @generated
	 */
	protected modelBP.Element getNewSource() {
		return (modelBP.Element) newEnd;
	}

	/**
	 * @generated
	 */
	protected modelBP.Element getOldTarget() {
		return (modelBP.Element) oldEnd;
	}

	/**
	 * @generated
	 */
	protected modelBP.Element getNewTarget() {
		return (modelBP.Element) newEnd;
	}
}
