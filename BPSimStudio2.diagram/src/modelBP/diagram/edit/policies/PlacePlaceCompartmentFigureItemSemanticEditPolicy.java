package modelBP.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

/**
 * @generated
 */
public class PlacePlaceCompartmentFigureItemSemanticEditPolicy extends
		modelBP.diagram.edit.policies.BPmodelBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public PlacePlaceCompartmentFigureItemSemanticEditPolicy() {
		super(modelBP.diagram.providers.BPmodelElementTypes.Place_2001);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (modelBP.diagram.providers.BPmodelElementTypes.Passive_3001 == req
				.getElementType()) {
			return getGEFWrapper(new modelBP.diagram.edit.commands.PassiveCreateCommand(
					req));
		}
		if (modelBP.diagram.providers.BPmodelElementTypes.Active_3002 == req
				.getElementType()) {
			return getGEFWrapper(new modelBP.diagram.edit.commands.ActiveCreateCommand(
					req));
		}
		return super.getCreateCommand(req);
	}

}
