package modelBP.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

/**
 * @generated
 */
public class ProcessProcessInputCompartmentFigureItemSemanticEditPolicy extends
		modelBP.diagram.edit.policies.BPmodelBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public ProcessProcessInputCompartmentFigureItemSemanticEditPolicy() {
		super(modelBP.diagram.providers.BPmodelElementTypes.Process_2002);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (modelBP.diagram.providers.BPmodelElementTypes.Transition_3004 == req
				.getElementType()) {
			return getGEFWrapper(new modelBP.diagram.edit.commands.Transition2CreateCommand(
					req));
		}
		if (modelBP.diagram.providers.BPmodelElementTypes.Place_3005 == req
				.getElementType()) {
			return getGEFWrapper(new modelBP.diagram.edit.commands.Place3CreateCommand(
					req));
		}
		return super.getCreateCommand(req);
	}

}
