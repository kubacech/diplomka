package modelBP.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

/**
 * @generated
 */
public class ProcessItemSemanticEditPolicy extends
		modelBP.diagram.edit.policies.BPmodelBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public ProcessItemSemanticEditPolicy() {
		super(modelBP.diagram.providers.BPmodelElementTypes.Process_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (modelBP.diagram.providers.BPmodelElementTypes.Place_2001 == req
				.getElementType()) {
			return getGEFWrapper(new modelBP.diagram.edit.commands.PlaceCreateCommand(
					req));
		}
		if (modelBP.diagram.providers.BPmodelElementTypes.Process_2002 == req
				.getElementType()) {
			return getGEFWrapper(new modelBP.diagram.edit.commands.ProcessCreateCommand(
					req));
		}
		if (modelBP.diagram.providers.BPmodelElementTypes.Transition_2003 == req
				.getElementType()) {
			return getGEFWrapper(new modelBP.diagram.edit.commands.TransitionCreateCommand(
					req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
