Spu�t�n� aplikace:
1. Nainstalujte Java JDK 7 a vy���
2. Ze str�nky 
	http://www.eclipse.org/downloads/ 
si st�hn�te Package Solution ozna�en� Eclipse Modeling Tools
3. Rozbalte a spus�te sta�en� prost�ed�.
4. P�i spu�t�n� budete vyzv�n� ke zvolen� v�choz� slo�ky. Za tu zvolte slo�ku pr�ce.
5. V z�lo�ce File klikn�te na mo�nost Import
6. Ve vznikl�m okn� vybereme General/Existing Projects into Workspace a stiskneme next
7. V polo�ce Select root directory vybereme tla��tkem Browse ko�enov� adres�� pr�ce a stiskneme Ok
8. Nyn� by jsme m�li m�t v okn� projects v�zvy 7 projekt� k importu
9. Nejsou-li vybr�ny, vybereme tyto projekty tla��tkem Select All
10. Klikneme na tla��tko Finish
11. Klikneme na tla��tko Run. Budeme vyzv�ni ke zvolen� zp�sobu spu�t�n�. Zvol�me Eclipse Application
12. Po spu�t�n� je vytvo�eno nov� okno prost�ed� Eclipse.
13. Chceme-li vytvo�it novou s�, tak mus�me nejprve pomoc� tla��tka new zalo��t nov� projekt (General/Project). Pot� na tento projekt klikneme prav�m tla��tkem my�i a zvol�me New > Other. Zde zvol�me Examples/BPmodel Diagram.