package bpsimstudio2.simulace.logika;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JOptionPane;

import bpsimstudio2.simulace.ChangeAction;
import bpsimstudio2.simulace.zaznam.PesLogger;
import modelBP.Edge;
import modelBP.Element;
import modelBP.Group;
import modelBP.GroupName;
import modelBP.GroupPointer;
import modelBP.ModelBPFactory;
import modelBP.ModelBPPackage;
import modelBP.Object;
import modelBP.Place;
import modelBP.Process;
import modelBP.Scenario;
import modelBP.Transition;

/**
 * @author Jakub Cech trida resici simulaci
 */
public class PesSimulace {
	List<Place> places;
	List<Transition> transitions;
	ChangeAction action;
	PesLogger logger;
	Process proc;

	public PesSimulace() {
		places = new ArrayList<Place>();
		transitions = new ArrayList<Transition>();
	}

	public PesSimulace(Process proc, PesLogger logger) {
		places = new ArrayList<Place>();
		transitions = new ArrayList<Transition>();
		pridejObjekty(proc);
		this.proc = proc;
		this.logger = logger;
	}

	private void pridejObjekty(Process proc) {
		for (Element elem : proc.getHasElement()) {
			if (elem instanceof Transition) {
				transitions.add((Transition) elem);
			}
			if (elem instanceof Place) {
				places.add((Place) elem);
			}

			if (elem instanceof Process) {
				pridejObjekty((Process) elem);
			}

		}
	}

	/**
	 * zjisteni proveditelnych prechodu
	 */
	private List<Transition> getProveditelne() {
		List<Transition> out = new ArrayList<Transition>();
		for (Transition transition : transitions) {

			if (isProveditelny(transition)) {
				out.add(transition);
			}
		}
		return out;

	}

	/**
	 * zjisteni ktere z proveditelnych prechodu jsou proveditelne v aktualnim
	 * case
	 */
	private List<Transition> getAktualneProveditelne(
			List<Transition> proveditelne) {
		List<Transition> out = new ArrayList<Transition>();
		for (Transition transition : proveditelne) {

			if (isAktualneProveditelny(transition)) {
				out.add(transition);
			}
		}
		return out;

	}

	/**
	 * provedeni nahodneho prechodu
	 */
	public boolean provedRandom(ChangeAction action) {
		this.action = action;
		List<Transition> proveditelne = getProveditelne();
		List<Transition> aktProveditelne = getAktualneProveditelne(proveditelne);
		if (proveditelne.size() > 0) {
			while (aktProveditelne.size() == 0) {
				int cas = proc.getGlobalTime();
				adjustTime();
				if (proc.getGlobalTime() == cas)
					return true;
				aktProveditelne = getAktualneProveditelne(proveditelne);
			}

			while (aktProveditelne.size() > 0) {

				int index = new Random().nextInt(aktProveditelne.size());

				if (proved(aktProveditelne.get(index))) {
					return true;
				} else {
					aktProveditelne.remove(index);
				}
			}

		}
		return false;
	}

	/**
	 * provedeni cele mapy s oznamenim konce
	 */
	public void provedAll(ChangeAction action) {
		provedAllNoMsg(action);
		JOptionPane.showMessageDialog(null, "Žádný přechod není možné provést",
				"", JOptionPane.INFORMATION_MESSAGE);

	}

	/**
	 * provedeni cele mapy
	 */
	public boolean provedAllNoMsg(ChangeAction mojeAkce) {
		this.action = mojeAkce;
		int stop = 50;
		int p = 0;
		while (provedRandom(action)) {
			stop--;
			p++;
			if (stop <= 0) {
				String[] possibilities = { "10", "50", "100", "500", "1000" };
				String s = (String) JOptionPane
						.showInputDialog(
								null,
								"Bylo provedeno "
										+ p
										+ " přechodů a síť stále obsahuje proveditelné přechody.\nJe však možné, že se někde vyskytla chyba a síť je nekonečná.\n\nKolik dalších kroků chcete provést? ",
								"", JOptionPane.WARNING_MESSAGE, null,
								possibilities, "100");

				if ((s != null) && (s.length() > 0)) {
					switch (s) {
					case "10":
						stop += 10;
						break;

					case "50":
						stop += 50;
						break;

					case "100":
						stop += 100;
						break;

					case "500":
						stop += 500;
						break;
					case "1000":
						stop += 1000;
						break;

					default:
						break;
					}
				} else {
					return false;

				}

			}
		}
		return true;
	}

	/**
	 * zjisteni proveditelnosti prechodu
	 */
	private boolean isProveditelny(Transition trans) {
		boolean proveditelne = true;
		for (Edge edge : trans.getLinkedIn()) {
			Map<GroupName, Integer> objMista = new HashMap<GroupName, Integer>();
			Place pl = (Place) edge.getSource();
			for (Group obj : pl.getHasObject()) {
				objMista.put(obj.getNamed(), obj.getCount());
			}
			for (GroupPointer obj : edge.getMultiplicity()) {
				if (objMista.get(obj.getNamed()) == null) {
					return false;
				} else {
					proveditelne &= (objMista.get(obj.getNamed()) >= obj
							.getCount());
				}
			}
		}
		for (Edge edge : trans.getLinkedOut()) {
			Map<GroupName, Integer> objMista = new HashMap<GroupName, Integer>();
			Map<GroupName, Integer> capa = new HashMap<GroupName, Integer>();
			Place pl = (Place) edge.getTarget();
			for (Group obj : pl.getHasObject()) {
				objMista.put(obj.getNamed(), obj.getCount());
			}
			for (GroupPointer obj : pl.getCapacity()) {
				capa.put(obj.getNamed(), obj.getCount());
			}
			for (GroupPointer obj : edge.getMultiplicity()) {
				if (capa.get(obj.getNamed()) == null) {
					return false;
				}
				if (objMista.get(obj.getNamed()) == null) {
					if (capa.get(obj.getNamed()) <= 0) {
						continue;
					} else {
						proveditelne &= (capa.get(obj.getNamed()) >= obj
								.getCount());
					}
				} else {
					if (capa.get(obj.getNamed()) <= 0) {
						continue;
					} else {
						proveditelne &= (capa.get(obj.getNamed()) >= (objMista
								.get(obj.getNamed()) + obj.getCount()));
					}
				}
			}
		}
		return proveditelne;
	}

	/**
	 * zjisteni proveditelnosti prechodu v aktualnim case
	 */
	private boolean isAktualneProveditelny(Transition trans) {
		for (Edge edge : trans.getLinkedIn()) {
			Map<GroupName, Group> objMista = new HashMap<GroupName, Group>();
			Place pl = (Place) edge.getSource();
			for (Group obj : pl.getHasObject()) {
				objMista.put(obj.getNamed(), obj);
			}
			for (GroupPointer obj : edge.getMultiplicity()) {

				Group target = objMista.get(obj.getNamed());
				if (obj.getCount() > target.getHasEnabledObject().size()) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * provedeni prechodu
	 */
	private boolean proved(Transition trans) {

		ScriptEngineManager factory = new ScriptEngineManager();
		ScriptEngine engine = factory.getEngineByName("groovy");
		Scenario scenar = getScenarioToDo(trans);
		Map<Group, List<Object>> editToDo = new HashMap<Group, List<Object>>();
		Map<Place, Group> groupsToAdd = new HashMap<Place, Group>();

		int cas = 0;
		// zjisteni scenare
		if (scenar != null) {
			engine.put("scen", scenar);
			cas = scenar.getTime();
		} else {
			engine.put("scen", ModelBPFactory.eINSTANCE.createScenario());
			cas = trans.getTime();
		}
		engine.put("time", cas);

		engine.put("guard", true);
		Map<String, Object> pracovniEle = new HashMap<String, Object>();
		// naplneni vstupnich mist pro skript
		for (Edge edge : trans.getLinkedIn()) {
			Map<GroupName, Group> objMista = new HashMap<GroupName, Group>();
			Place pl = (Place) edge.getSource();
			for (Group obj : pl.getHasObject()) {
				objMista.put(obj.getNamed(), obj);
			}
			for (GroupPointer obj : edge.getMultiplicity()) {

				int mapI = 0;
				Group target = objMista.get(obj.getNamed());
				List<Object> zbytek = new ArrayList<Object>();
				List<Object> enabled = target.getHasEnabledObject();
				for (Object o : target.getHasObject()) {
					zbytek.add(o);
				}
				for (int i = 0; i < obj.getCount(); i++) {
					mapI++;
					int index = new Random().nextInt(enabled.size());
					pracovniEle.put("I" + pl.getName() + "G"
							+ target.getNamed().getName() + "O" + mapI,
							enabled.get(index));
					zbytek.remove(enabled.get(index));
					enabled.remove(index);

				}
				editToDo.put(target, zbytek);
				// updateGroup(target, zbytek);

			}
		}
		// naplneni vystupnich mist pro skript
		for (Edge edge : trans.getLinkedOut()) {
			Map<GroupName, Group> objMista = new HashMap<GroupName, Group>();
			Place pl = (Place) edge.getTarget();
			for (Group obj : pl.getHasObject()) {
				objMista.put(obj.getNamed(), obj);
			}
			for (GroupPointer obj : edge.getMultiplicity()) {

				int mapI = 0;
				if (objMista.get(obj.getNamed()) == null) {
					Group nova;
					if (obj.getNamed().isActive()) {
						nova = ModelBPFactory.eINSTANCE.createActive();
					} else {
						nova = ModelBPFactory.eINSTANCE.createPassive();
					}
					nova.setNamed(obj.getNamed());
					objMista.put(nova.getNamed(), nova);
					groupsToAdd.put(pl, nova);
					// updatePlaceAddGroup(pl, nova);
				}
				Group target = objMista.get(obj.getNamed());
				for (int i = 0; i < obj.getCount(); i++) {
					mapI++;
					Object o = ModelBPFactory.eINSTANCE.createObject();
					o.setDescription("gen");
					o.setTime(proc.getGlobalTime() + cas);
					pracovniEle.put("O" + pl.getName() + "G"
							+ target.getNamed().getName() + "O" + mapI, o);
				}

			}
		}

		// naplneni skriptu daty
		for (Entry<String, Object> set : pracovniEle.entrySet()) {
			engine.put(set.getKey(), set.getValue());
		}

		engine.put("trans", trans);
		// odstraneni komentaru a provedeni skriptu
		if (!trans.getScript().replaceAll("//.*\n", "")
				.replaceAll("/\\*.*\\*/", "").trim().isEmpty())
			try {
				engine.eval(trans.getScript());
			} catch (ScriptException e) {
				JOptionPane.showMessageDialog(null, "Provádění přechodu "
						+ trans.getName()
						+ " bylo ukončeno z důvodu chyby skriptu.", "",
						JOptionPane.INFORMATION_MESSAGE);
				return true;
			}

		// kontrola guardu
		if (!(boolean) engine.get("guard")) {
			return false;
		}

		// prvedeni odlozenych akci
		for (Entry<Group, List<Object>> set : editToDo.entrySet()) {
			updateGroup(set.getKey(), set.getValue());
		}

		for (Entry<Place, Group> set : groupsToAdd.entrySet()) {
			updatePlaceAddGroup(set.getKey(), set.getValue());
		}
		// ziskani vystupnich objektu
		for (Entry<String, Object> set : pracovniEle.entrySet()) {
			pracovniEle.put(set.getKey(), ((Object) engine.get(set.getKey())));
		}
		// vlozeni vystupnich objektu
		for (Edge edge : trans.getLinkedOut()) {
			boolean provest = false;
			// zjisteni, zda se hrana provede
			if (scenar != null) {
				for (Scenario scen : edge.getScenarios()) {
					provest |= scen.equals(scenar);
				}
			}
			// provedeni hrany
			if ((scenar == null) || provest) {
				Map<GroupName, Group> objMista = new HashMap<GroupName, Group>();
				Place pl = (Place) edge.getTarget();
				for (Group obj : pl.getHasObject()) {
					objMista.put(obj.getNamed(), obj);
				}
				for (GroupPointer obj : edge.getMultiplicity()) {
					int mapI = 0;
					if (objMista.get(obj.getNamed()) == null) {
						Group nova;
						if (obj.getNamed().isActive()) {
							nova = ModelBPFactory.eINSTANCE.createActive();
						} else {
							nova = ModelBPFactory.eINSTANCE.createPassive();
						}
						nova.setNamed(obj.getNamed());
						objMista.put(nova.getNamed(), nova);
						updatePlaceAddGroup(pl, nova);
					}

					Group target = objMista.get(obj.getNamed());
					List<Object> objekty = new ArrayList<Object>();
					List<Object> objToUpdate = new ArrayList<Object>();
					for (Object o : target.getHasObject()) {
						objekty.add(o);
					}
					for (int i = 0; i < obj.getCount(); i++) {
						mapI++;
						Object ob = pracovniEle.get("O" + pl.getName() + "G"
								+ target.getNamed().getName() + "O" + mapI);
						if (ob != null) {
							// if (ob.getContained() != null) {
							objToUpdate.add(ob);
							// }
							objekty.add(ob);
							logger.updateObject(ob, proc.getGlobalTime(), cas,
									trans, scenar);
						}
					}
					updateGroup(target, objekty);
					for (Object object : objToUpdate) {
						updateObjectTime(object, proc.getGlobalTime() + cas);

					}
				}

			}

		}
		logger.addStep(trans, scenar, cas);
		return true;
	}

	/**
	 * ziskani nahodneho scenare
	 */
	private Scenario getScenarioToDo(Transition trans) {
		if (trans.getHasScenarios().size() == 0) {
			return null;
		}
		float volba = new Random().nextFloat() * 100;
		float celkem = 0;
		for (Scenario scen : trans.getHasScenarios()) {
			celkem += scen.getPercent();
			if (celkem > volba)
				return scen;
		}
		return null;

	}

	/**
	 * zmena objektu ve skupine
	 */
	private void updateGroup(Group obj, List<Object> objekty) {
		if (obj == null) {

		} else {
			action.run(obj, ModelBPPackage.eINSTANCE.getGroup_HasObject(),
					objekty);
			action.run(obj, ModelBPPackage.eINSTANCE.getGroup_Count(),
					objekty.size());
		}
	}

	/**
	 * pridani skupiny do mista
	 */
	private void updatePlaceAddGroup(Place plac, Group group) {
		List<Group> listNew = new ArrayList<Group>();
		for (Group g : plac.getHasObject()) {
			listNew.add(g);
		}
		listNew.add(group);
		action.run(plac, ModelBPPackage.eINSTANCE.getPlace_HasObject(), listNew);
	}

	/**
	 * zmena casu objektu
	 */
	private void updateObjectTime(Object obj, int time) {
		if (obj == null) {

		} else {
			action.run(obj, ModelBPPackage.eINSTANCE.getObject_Time(), time);

		}
	}

	/**
	 * navyseni globalnich hodin
	 */
	private void adjustTime() {
		action.run(proc, ModelBPPackage.eINSTANCE.getProcess_GlobalTime(),
				proc.getGlobalTime() + 1);

		System.out.println("zvedam cas na " + proc.getGlobalTime());
	}

}
