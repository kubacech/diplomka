package bpsimstudio2.simulace.logika;

import bpsimstudio2.simulace.ChangeAction;
import modelBP.Edge;
import modelBP.Element;
import modelBP.GroupPointer;
import modelBP.ModelBPPackage;
import modelBP.Process;
import modelBP.Transition;

/**
 * @author Jakub Cech CEC0035
 * trida resici pomocne metody
 */
public class PesHelper {

	/**
	 * metoda pro pridani navodu do procesu pri prvnim spusteni operace
	 */
	public void opravProc(Process proc, ChangeAction mojeAkce) {
		for (Element elem : proc.getHasElement()) {
			if (elem instanceof Transition) {
				opravProcScript((Transition) elem, mojeAkce);
			}
		}

	}

	/**
	 * metoda pro pridani napoved jmen objektu ve scriptu 
	 */
	private void opravProcScript(Transition elem, ChangeAction mojeAkce) {
		String out = "//gen scen  - aktivni scenar\n";
		String rest = ((Transition) elem).getScript().replaceAll("//gen.*\n",
				"");
		for (Edge edge : elem.getLinkedIn()) {
			String plac;
			plac = edge.getSource().getName();
			for (GroupPointer obj : edge.getMultiplicity()) {
				for (int j = 0; j < obj.getCount(); j++) {
					out += ("//gen I" + plac + "G" + obj.getNamed().getName()
							+ "O" + (j + 1) + "   -  objekt typu "
							+ obj.getNamed().getName() + " z mista " + plac + ". \n");
				}
			}

		}

		for (Edge edge : elem.getLinkedOut()) {
			String plac;
			plac = edge.getTarget().getName();
			for (GroupPointer obj : edge.getMultiplicity()) {
				for (int j = 0; j < obj.getCount(); j++) {
					out += ("//gen O" + plac + "G" + obj.getNamed().getName()
							+ "O" + (j + 1) + "   -  objekt typu "
							+ obj.getNamed().getName() + " do mista " + plac + ". \n");
				}
			}

		}

		out = out + rest;
		mojeAkce.run(elem, ModelBPPackage.eINSTANCE.getTransition_Script(), out);

	}



}
