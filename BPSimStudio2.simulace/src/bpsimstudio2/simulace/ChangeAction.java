package bpsimstudio2.simulace;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.diagram.ui.actions.DiagramAction;
import org.eclipse.gmf.runtime.diagram.ui.requests.EditCommandRequestWrapper;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;

/**
 * @author Jakub Cech CEC0035
 * trida requestu pro zmeny v modelu
 */
public class ChangeAction extends DiagramAction {

	EObject target = null;
	EStructuralFeature targetValue = null;
	Object newValue = null;

	protected ChangeAction(IWorkbenchPart part) {
		super(part);
		super.setup();
	}

	public void run(EObject target, EStructuralFeature targetValue,
			Object newValue) {
		if (target != null && targetValue != null && newValue != null) {
			this.target = target;
			this.targetValue = targetValue;
			this.newValue = newValue;
			super.setTargetRequest(createTargetRequest());
			super.run();
		}
	}

	@Override
	protected Request createTargetRequest() {
		if (target == null || targetValue == null || newValue == null) {
			return new Request();
		}
		return new EditCommandRequestWrapper(new SetRequest(target,
				targetValue, newValue));

	}

	@Override
	protected boolean isSelectionListener() {
		return true;
	}

}
