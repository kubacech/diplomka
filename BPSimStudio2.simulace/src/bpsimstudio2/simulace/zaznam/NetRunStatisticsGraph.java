package bpsimstudio2.simulace.zaznam;

import java.awt.Font;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFrame;

import modelBP.Scenario;
import modelBP.Transition;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.ApplicationFrame;

/**
 * @author Jakub Cech CEC0035
 * trida pro kolacovy graf
 */
@SuppressWarnings("serial")
public class NetRunStatisticsGraph extends ApplicationFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7360514466469967720L;
	float celkem = 0;
	int behu = 0;

	public NetRunStatisticsGraph(Map<Transition, Map<Scenario, Integer>> data,
			float celkem, int behu) {
		super("Statistika provádění přechodů");
		this.celkem = celkem;
		this.behu = behu;
		JFrame hlavn = new JFrame();
		hlavn.setTitle("Graf provádění");
		hlavn.add(new ChartPanel(createChart(createDataset(data))));
		hlavn.pack();
		hlavn.setVisible(true);
	}

	/**
	 * vytvoreni datove zakladny ze zadane mapy
	 */
	private PieDataset createDataset(
			Map<Transition, Map<Scenario, Integer>> data) {
		DefaultPieDataset dataset = new DefaultPieDataset();
		for (Entry<Transition, Map<Scenario, Integer>> ste : data.entrySet()) {
			for (Entry<Scenario, Integer> scen : ste.getValue().entrySet()) {
				dataset.setValue(
						"Přechod "
								+ ste.getKey().getName()
								+ ((scen.getKey() != null) ? " se scénářem "
										+ scen.getKey().getName()
										: " bez definovaného scénáře")
								+ " - "
								+ scen.getValue()
								+ "x ("
								+ (float) (Math.round(scen.getValue() / celkem
										* 10000.0) / 100.0) + "%)", new Double(
								scen.getValue()));

			}
		}

		return dataset;
	}

	/**
	 * vytvoreni grafu
	 */
	private JFreeChart createChart(PieDataset dataset) {

		JFreeChart chart = ChartFactory.createPieChart(
				"Pravděpodobnost provádění za " + behu + " simulací", // chart
				// title
				dataset, // data
				true, // include legend
				true, false);

		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
		plot.setNoDataMessage("No data available");
		plot.setCircular(false);
		plot.setLabelGap(0.02);
		return chart;

	}

}
