package bpsimstudio2.simulace.zaznam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import modelBP.Scenario;
import modelBP.Transition;

/**
 * @author Jakub Cech CEC0035
 * trida zaznamenavajici provadeni site
 */
public class StepRecord {

	List<Step> kroky;

	public StepRecord() {
		kroky = new ArrayList<Step>();
	}

	protected void addStep(Transition transition, Scenario scenario,
			int stepTime) {
		kroky.add(new Step(transition, scenario, stepTime));
	}

	public void print() {
		Map<Transition, Map<Scenario, Integer>> stat = new HashMap<Transition, Map<Scenario, Integer>>();
		float celkem = (float)kroky.size();
		for (Step step : kroky) {
			if (stat.get(step.getTransition()) == null) {
				stat.put(step.getTransition(), new HashMap<Scenario, Integer>());
			}
			if (stat.get(step.getTransition()).get(step.getScenario()) == null) {
				stat.get(step.getTransition()).put(step.getScenario(), 1);
			} else {
				stat.get(step.getTransition())
						.put(step.getScenario(),
								stat.get(step.getTransition()).get(
										step.getScenario()) + 1);
			}
		}
		for (Entry<Transition, Map<Scenario, Integer>> ste : stat.entrySet()) {
			for (Entry<Scenario, Integer> scen : ste.getValue().entrySet()) {
				System.out.println("Přechod "
						+ ste.getKey().getName()
						+ ((scen.getKey() != null) ? " se scénéřem "
								+ scen.getKey().getName() : " bez definovaného scénáře")

						+ " proběhl celkem "
						+ scen.getValue()
						+ "x, což znamená, že se provádí s "
						+ (float) (Math.round(scen.getValue() / celkem
								* 10000.0) / 100.0) + "% pravděpodobností.");
			}
		}
	}

	protected Map<Transition, Map<Scenario, Integer>> getStat() {
		Map<Transition, Map<Scenario, Integer>> stat = new HashMap<Transition, Map<Scenario, Integer>>();
		for (Step step : kroky) {
			if (stat.get(step.getTransition()) == null) {
				stat.put(step.getTransition(), new HashMap<Scenario, Integer>());
			}
			if (stat.get(step.getTransition()).get(step.getScenario()) == null) {
				stat.get(step.getTransition()).put(step.getScenario(), 1);
			} else {
				stat.get(step.getTransition())
						.put(step.getScenario(),
								stat.get(step.getTransition()).get(
										step.getScenario()) + 1);
			}
		}
		return stat;

	}

	/**
	 * @author Jakub Cech
	 * trida zaznamenavajici jeden kork behu
	 */
	private class Step {
		Transition transition;
		Scenario scenario;
		int stepTime;

		public Step(Transition transition, Scenario scenario, int stepTime) {
			super();
			this.transition = transition;
			this.scenario = scenario;
			this.stepTime = stepTime;
		}

		public Transition getTransition() {
			return transition;
		}

		public void setTransition(Transition transition) {
			this.transition = transition;
		}

		public Scenario getScenario() {
			return scenario;
		}

		public void setScenario(Scenario scenario) {
			this.scenario = scenario;
		}

		public int getStepTime() {
			return stepTime;
		}

		public void setStepTime(int stepTime) {
			this.stepTime = stepTime;
		}

	}
}
