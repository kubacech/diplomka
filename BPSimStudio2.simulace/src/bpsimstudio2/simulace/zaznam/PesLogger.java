package bpsimstudio2.simulace.zaznam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import bpsimstudio2.simulace.PesInstance;
import modelBP.Object;
import modelBP.Scenario;
import modelBP.Transition;

/**
 * @author Jakub Cech CEC0035
 * singleton trida zaznamu celeho behu
 */
public class PesLogger {

	private static PesLogger instance;
	List<StepRecord> zaznamBehu;
	List<ObjectsRecordsGraf> historieObjektu;
	StepRecord aktualniRecord;
	ObjectsRecordsGraf objectsRecords;

	private PesLogger() {
		zaznamBehu = new ArrayList<StepRecord>();
		historieObjektu = new ArrayList<ObjectsRecordsGraf>();
		aktualniRecord = new StepRecord();
		zaznamBehu.add(aktualniRecord);
		objectsRecords = new ObjectsRecordsGraf();
		historieObjektu.add(objectsRecords);
	}

	public static PesLogger getInstance() {
		if (instance == null) {
			instance = new PesLogger();
		}
		return instance;
	}

	/**
	 * vytvoreni noveho zaznamu
	 */
	public void newRun() {
		aktualniRecord = new StepRecord();
		zaznamBehu.add(aktualniRecord);
		objectsRecords = new ObjectsRecordsGraf();
		historieObjektu.add(objectsRecords);
	}

	/**
	 * zaznamenani behu jednoho obecneho kroku site
	 */
	public void addStep(Transition transition, Scenario scenario, int stepTime) {
		aktualniRecord.addStep(transition, scenario, stepTime);
	}

	/**
	 * zaznamenani jednoho kroku pro zadany objekt
	 */
	public void updateObject(Object obj, int globalTime, int stepTime,
			Transition transition, Scenario scenario) {
		objectsRecords.updateObject(obj, globalTime, stepTime, transition,
				scenario);

	}

	public void printObj() {
		objectsRecords.print();
	}

	public void printStat() {
		// objectsRecords.print();
		Map<Transition, Map<Scenario, Integer>> stat = new HashMap<Transition, Map<Scenario, Integer>>();
		float celkem = 0;
		int behu = 0;
		for (StepRecord step : zaznamBehu) {
			behu++;
			for (Entry<Transition, Map<Scenario, Integer>> ste : step.getStat()
					.entrySet()) {
				if (stat.get(ste.getKey()) == null) {
					stat.put(ste.getKey(), new HashMap<Scenario, Integer>());
				}
				for (Entry<Scenario, Integer> steOne : ste.getValue()
						.entrySet()) {

					celkem += steOne.getValue();
					if (stat.get(ste.getKey()).get(steOne.getKey()) == null) {
						stat.get(ste.getKey()).put(steOne.getKey(),
								steOne.getValue());
					} else {
						stat.get(ste.getKey()).put(
								steOne.getKey(),
								stat.get(ste.getKey()).get(steOne.getKey())
										+ steOne.getValue());
					}
				}

			}
		}
		new NetRunStatisticsGraph(stat, celkem, behu);
//		System.out.println("V celé similaci " + behu + " běhů :" + celkem);
//		for (Entry<Transition, Map<Scenario, Integer>> ste : stat.entrySet()) {
//			for (Entry<Scenario, Integer> scen : ste.getValue().entrySet()) {
//				System.out.println("Přechod "
//						+ ste.getKey().getName()
//						+ ((scen.getKey() != null) ? " se scénářem "
//								+ scen.getKey().getName()
//								: " bez definovaného scénáře")
//
//						+ " proběhl celkem "
//						+ scen.getValue()
//						+ "x, což znamená, že se provádí s "
//						+ (float) (Math.round(scen.getValue() / celkem
//								* 10000.0) / 100.0) + "% pravděpodobností.");
//			}
//		}
	}

	/**
	 * vymazani celeho zaznamu
	 */
	public void reset() {
		zaznamBehu = new ArrayList<StepRecord>();
		historieObjektu = new ArrayList<ObjectsRecordsGraf>();
		aktualniRecord = new StepRecord();
		zaznamBehu.add(aktualniRecord);
		objectsRecords = new ObjectsRecordsGraf();
		historieObjektu.add(objectsRecords);
	}

}
