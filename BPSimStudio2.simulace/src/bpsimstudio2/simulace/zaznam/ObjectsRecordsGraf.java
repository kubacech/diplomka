package bpsimstudio2.simulace.zaznam;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

import modelBP.Object;
import modelBP.Scenario;
import modelBP.Transition;

/**
 * @author Jakub Cech
 *trida uchovavajici behy jednotlivych objektu
 */
public class ObjectsRecordsGraf extends ApplicationFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4676763125084672713L;
	List<OneObjectRecord> zaznamObjektu;
	int globalTime = 0;
	String outString = new String();

	public ObjectsRecordsGraf() {
		super("Záznam soucasného běhu");
		this.setLayout(null);
		zaznamObjektu = new ArrayList<OneObjectRecord>();
	}

	/**
	 * metoda pro zaznamenani kroku
	 */
	public void updateObject(Object obj, int globalTime, int stepTime,
			Transition transition, Scenario scenario) {
		if (this.globalTime < globalTime + stepTime) {
			this.globalTime = globalTime + stepTime;
		}
		for (OneObjectRecord objRec : zaznamObjektu) {
			if (objRec.getObject().equals(obj)) {
				objRec.addStep(transition, scenario, globalTime, stepTime);

				return;
			}
		}
		OneObjectRecord novy = new OneObjectRecord(obj);
		novy.addStep(transition, scenario, globalTime, stepTime);
		zaznamObjektu.add(novy);

	}

	/**
	 * metoda pro vykresleni
	 */
	public void print() {
		final BarRenderer renderer = new BarRenderer();
		renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());
		final CombinedDomainXYPlot plot = new CombinedDomainXYPlot();

		for (OneObjectRecord objRec : zaznamObjektu) {
			objRec.print(plot);
		}
		toFrame(new JFreeChart("Graf objektů", new Font("SansSerif", Font.BOLD,
				12), plot, false));

	}

	/**
	 * metoda vkladajici graf do okna
	 */
	private void toFrame(JFreeChart graf) {
		final ChartPanel chartPanel = new ChartPanel(graf);
		chartPanel.setLayout(null);
		// add the chart to a panel...
		chartPanel.setLayout(new BorderLayout());
		chartPanel.setSize(new java.awt.Dimension(500,
				zaznamObjektu.size() * 50));
		chartPanel.setPreferredSize(new java.awt.Dimension(500, zaznamObjektu
				.size() * 50));

		JFrame hlavn = new JFrame();
		hlavn.setTitle("Graf objektů");
		hlavn.setSize(715, 338);
		hlavn.setBackground(Color.gray);
		hlavn.setLayout(null);

		JScrollPane p = new JScrollPane();
		p.setSize(new java.awt.Dimension(700, 300));
		p.setVisible(true);
		p.getViewport().add(chartPanel);
		hlavn.add(p);

		// hlavn.add(chartPanel);
		hlavn.setVisible(true);
	}

	/**
	 * metoda pro vytvoreni jednoho grafu z listu
	 */
	private XYPlot createSubChart(final XYDataset dataset, String object,
			 List<OneObjectRecord.Step> oper) {

		final JFreeChart chart = ChartFactory.createXYAreaChart("", "", "ZA",
				dataset, PlotOrientation.VERTICAL, false, // legend
				false, // tool tips
				false // URLs
				);

		chart.setBackgroundPaint(Color.white);

		final XYPlot plot = chart.getXYPlot();
		// plot.setOutlinePaint(Color.black);
		plot.setBackgroundPaint(Color.lightGray);
		plot.setForegroundAlpha(0.65f);
		plot.setDomainGridlinesVisible(false);

		plot.setRangeGridlinesVisible(false);

		final XYTextAnnotation annotation = new XYTextAnnotation("Objekt "
				+ object, globalTime / 2, 1.5);
		annotation.setFont(new Font("SansSerif", Font.PLAIN, 11));
		plot.addAnnotation(annotation);
		plot.getDomainAxis().setVisible(true);
		plot.getRangeAxis().setVisible(false);

		final ValueAxis yAxis = plot.getRangeAxis();
		yAxis.setTickMarkPaint(Color.black);
		yAxis.setRange(0, 2);

		XYItemRenderer renderer = plot.getRenderer();
		renderer.setSeriesPaint(0, Color.red);
		plot.setRenderer(renderer);

		for (OneObjectRecord.Step step : oper) {
			if (step.stepTime > 0) {
				final XYTextAnnotation annotation2 = new XYTextAnnotation(step
						.getTransition().getName()
						+ ((step.getScenario() != null) ? "("
								+ step.getScenario().getName() + ")" : ""),
						step.getGlobalTime() + (step.stepTime / 2.0), 0.5);
				annotation2.setFont(new Font("SansSerif", Font.PLAIN, 9));
				plot.addAnnotation(annotation2);
			}
		}

		return plot;

	}

	/**
	 * vnitrni trida zaznamenajici beh jednoho objektu
	 *
	 */
	private class OneObjectRecord {

		List<Step> kroky;
		Object object;

		public OneObjectRecord(Object object) {
			kroky = new ArrayList<Step>();
			this.object = object;
		}

		public List<Step> getKroky() {
			return kroky;
		}

		public Object getObject() {
			return object;
		}

		protected void addStep(Transition transition, Scenario scenario,
				int globalTime, int stepime) {
			kroky.add(new Step(transition, scenario, globalTime, stepime));
		}

		public void print(CombinedDomainXYPlot plot) {
			// outString += ("Object " + object + "\n");
			final XYSeries series = new XYSeries("object.getDesc()");
			int globalTimePrint = 0;
			if (kroky.size() > 0) {
				int last = (kroky.get(0).globalTime == 0) ? 1 : 0;
				for (int j = 0; j < kroky.size(); j++) {
					Step step = kroky.get(j);
					if(last==0){
						series.add(step.getGlobalTime(), 0);
					}
						series.add(step.getGlobalTime(), 1);
						series.add(step.getGlobalTime() + step.getStepTime(), 1);
						last=1;
					if(j<(kroky.size()-1)&&kroky.get(j+1).getGlobalTime()!=step.getGlobalTime() + step.getStepTime()){
						series.add(step.getGlobalTime() + step.getStepTime(), 0);
						last=0;
					}
				}
				plot.add(
						createSubChart(new XYSeriesCollection(series),
								object.getDesc(), kroky), 1);
			}
		}

		/**
		 * trida reprezentujici jeden krok behu objektu
		 */
		private class Step {
			Transition transition;
			Scenario scenario;
			int globalTime;
			int stepTime;

			public Step(Transition transition, Scenario scenario,
					int globalTime, int runTime) {
				super();
				this.transition = transition;
				this.scenario = scenario;
				this.globalTime = globalTime;
				this.stepTime = runTime;
			}

			public Transition getTransition() {
				return transition;
			}

			public void setTransition(Transition transition) {
				this.transition = transition;
			}

			public Scenario getScenario() {
				return scenario;
			}

			public void setScenario(Scenario scenario) {
				this.scenario = scenario;
			}

			public int getGlobalTime() {
				return globalTime;
			}

			public void setGlobalTime(int globalTime) {
				this.globalTime = globalTime;
			}

			public int getStepTime() {
				return stepTime;
			}

			public void setStepTime(int stepTime) {
				this.stepTime = stepTime;
			}

		}
	}

}
