package bpsimstudio2.simulace;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ui.IWorkbenchPart;

import bpsimstudio2.simulace.logika.PesHelper;
import bpsimstudio2.simulace.logika.PesSimulace;
import bpsimstudio2.simulace.zaznam.PesLogger;
import modelBP.Element;
import modelBP.Group;
import modelBP.GroupName;
import modelBP.ModelBPPackage;
import modelBP.Place;
import modelBP.Process;
import modelBP.Transition;

/**
 * @author Jakub Cech CEC0035 hlavni instance resici rozhrani mezi simulaci a
 *         modelem
 */

public class PesInstance {

	private static PesInstance instance;
	private PesSimulace simulace;
	private PesLogger logger;
	private Process proc; // aktivni proces
	private Process procOld; // zaloha procesu pro obnoveni
	private ChangeAction mojeAkce;
	private boolean opraveno = false;
	private boolean firstRun = true;
	private Map<Element, Element> copyMap = new HashMap<Element, Element>();

	/**
	 * vychozi konstruktor
	 */
	private PesInstance() {
		simulace = new PesSimulace();
		if (logger == null) {
			logger = PesLogger.getInstance();
		}

	}

	/**
	 * @param proc
	 *            - tetoda ulozi proces pro operace navratu
	 */
	private void copyProc(Process proc) {
		procOld = EcoreUtil.copy(proc);
		for (int i = 0; i < proc.getHasElement().size(); i++) {
			copyMap.put(proc.getHasElement().get(i), procOld.getHasElement()
					.get(i));
		}
	}

	/**
	 * konstruktor k vytvoreni jedine instance uchovavajici vlozeny proces
	 * 
	 * @param proc
	 *            hlavni proces
	 */
	private PesInstance(Process proc) {
		this.proc = proc;
		if (procOld == null) {
			copyProc(proc);
		}

		logger = PesLogger.getInstance();
		this.simulace = new PesSimulace(proc, logger);

	}

	/**
	 * @return singleton tridy
	 */
	public static PesInstance getInstance() {
		if (instance == null) {
			instance = new PesInstance();
		}
		return instance;
	}

	public void setMainProc(Process proc) {
		instance = new PesInstance(proc);
		opraveno = false;
		firstRun = true;
	}

	/**
	 * metoda spoustejici nahodny provedeni jednoho prechodu
	 */
	public void runRandom(IWorkbenchPart part) {
		if (proc == null)
			return;
		if (mojeAkce == null) {
			this.mojeAkce = new ChangeAction(part);
		}
		if (!firstRun) {
			if (!opraveno) {
				opraveno = true;
				PesHelper helper = new PesHelper();
				helper.opravProc(proc, mojeAkce);
				PesLogger.getInstance().reset();
			}
		} else {
			firstRun = false;
		}
		if (!simulace.provedRandom(mojeAkce))
			JOptionPane.showMessageDialog(null,
					"Žádný přechod není možné provést", "",
					JOptionPane.INFORMATION_MESSAGE);

	}

	/**
	 * metoda k provedeni cele mapy
	 */
	public void runAll(IWorkbenchPart part) {
		if (proc == null)
			return;
		if (mojeAkce == null)
			this.mojeAkce = new ChangeAction(part);
		if (!firstRun) {
			if (!opraveno) {
				opraveno = true;
				PesHelper helper = new PesHelper();
				helper.opravProc(proc, mojeAkce);
				PesLogger.getInstance().reset();
			}
		} else {
			firstRun = false;
		}
		simulace.provedAll(mojeAkce);
	}

	/**
	 * metoda ke spusteni simulace uzivatelem zadany pocet krat
	 */
	public void runAllMore(IWorkbenchPart part) {
		if (proc == null)
			return;
		if (mojeAkce == null)
			this.mojeAkce = new ChangeAction(part);
		if (!firstRun) {
			if (!opraveno) {
				opraveno = true;
				PesHelper helper = new PesHelper();
				helper.opravProc(proc, mojeAkce);
				PesLogger.getInstance().reset();
			}
		} else {
			firstRun = false;
		}
		String s = (String) JOptionPane.showInputDialog(null,
				"Kolik simulaci si přejete spustit?", "Nastavení simulace",
				JOptionPane.PLAIN_MESSAGE, null, null, "1");

		int run = 0;
		try {
			run = Integer.parseInt(s.trim());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Zadaný počet musí být číslo.",
					"", JOptionPane.INFORMATION_MESSAGE);
		}
		if (run > 0) {
			for (int i = 0; i < run; i++) {
				if (simulace.provedAllNoMsg(mojeAkce))
					return;
				if (run > i + 1)
					resetNetPlaces(part);
			}

		}
		JOptionPane.showMessageDialog(null, "Všech" + run
				+ " simulací bylo úspěšně dokončeno.", "",
				JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * metoda obnovujici jen mista a objekty v nich
	 */
	public void resetNetPlaces(IWorkbenchPart part) {
		if (proc == null)
			return;
		if (mojeAkce == null)
			this.mojeAkce = new ChangeAction(part);

		Process procPom = EcoreUtil.copy(procOld);

		for (Element ele : proc.getHasElement()) {
			if (ele instanceof Place) {
				mojeAkce.run(ele,
						ModelBPPackage.eINSTANCE.getPlace_HasObject(),
						((Place) copyMap.get(ele)).getHasObject());
				for (Group group : ((Place) ele).getHasObject()) {
					for (GroupName groupName : proc.getHasGroupName()) {
						if (groupName.getName().equals(
								group.getNamed().getName())
								&& groupName.isActive() == group.getNamed()
										.isActive()) {
							mojeAkce.run(group,
									ModelBPPackage.eINSTANCE.getGroup_Named(),
									groupName);
							break;
						}

					}
				}
			}
			if (ele instanceof Transition) {
				mojeAkce.run(ele,
						ModelBPPackage.eINSTANCE.getTransition_Script(),
						((Transition) copyMap.get(ele)).getScript());
				mojeAkce.run(ele,
						ModelBPPackage.eINSTANCE.getTransition_CountMax(),
						((Transition) copyMap.get(ele)).getCountMax());
				mojeAkce.run(ele,
						ModelBPPackage.eINSTANCE.getTransition_CountMin(),
						((Transition) copyMap.get(ele)).getCountMin());
				mojeAkce.run(ele,
						ModelBPPackage.eINSTANCE.getTransition_TimeMax(),
						((Transition) copyMap.get(ele)).getTimeMax());
				mojeAkce.run(ele,
						ModelBPPackage.eINSTANCE.getTransition_TimeMin(),
						((Transition) copyMap.get(ele)).getTimeMin());

				// for (Scenario group : ((Transition) ele).getHasScenarios()) {
				// for (GroupName groupName : proc.getHasGroupName()) {
				// if (groupName.getName().equals(
				// group.getNamed().getName())
				// && groupName.isActive() == group.getNamed()
				// .isActive()) {
				// mojeAkce.run(group,
				// ModelBPPackage.eINSTANCE.getGroup_Named(),
				// groupName);
				// break;
				// }
				//
				// }
				// }
			}
		}
		mojeAkce.run(proc, ModelBPPackage.eINSTANCE.getProcess_GlobalTime(),
				procOld.getGlobalTime());
		logger.newRun();
		procOld = procPom;
		for (int i = 0; i < proc.getHasElement().size(); i++) {
			copyMap.put(proc.getHasElement().get(i), procOld.getHasElement()
					.get(i));
		}
		this.simulace = new PesSimulace(proc, logger);
	}

	/**
	 * metoda obnovujici celou sit
	 */
	public void resetWholeNet(IWorkbenchPart part) {
		if (proc == null)
			return;
		if (mojeAkce == null)
			this.mojeAkce = new ChangeAction(part);

		Process procPom = EcoreUtil.copy(procOld);

		mojeAkce.run(proc, ModelBPPackage.eINSTANCE.getProcess_HasElement(),
				procOld.getHasElement());
		mojeAkce.run(proc, ModelBPPackage.eINSTANCE.getProcess_HasGroupName(),
				procOld.getHasGroupName());
		mojeAkce.run(proc, ModelBPPackage.eINSTANCE.getProcess_GlobalTime(),
				procOld.getGlobalTime());
		logger.newRun();
		procOld = procPom;
		this.simulace = new PesSimulace(proc, logger);
	}

	/**
	 * metoda pro ulozeni soucasneho stavu site
	 */
	public void saveState(IWorkbenchPart part) {
		copyProc(proc);
	}
	
	/**
	 * metoda pro zobrazeni grafu objektu
	 */
	public void showLogObj() {
		logger.printObj();
	}

	/**
	 * metoda pro zobrazeni grafu site
	 */
	public void showLogStat() {
		logger.printStat();
	}
}
